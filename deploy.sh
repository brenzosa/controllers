#!/bin/bash

TARGET_HOST=$1
TARGET_USER=$2

if [ -z "$TARGET_USER"]; then
    echo "USAGE: ./deploy.sh <target host> <target user>"
    echo " e.g.: ./deploy.sh controller1.local pi"
    exit 1
fi

VERSION=$(jq -r .version package.json)

scp ./dist/controllers-ui.tgz $TARGET_USER@$TARGET_HOST:~/
scp ./dist/controllers-$VERSION.tgz $TARGET_USER@$TARGET_HOST:~/

ssh -o StrictHostKeyChecking=no $TARGET_USER@$TARGET_HOST bash <<EOF
    sudo rm -rf /opt/controllers-ui/dist/ /opt/controllers/package/
    sudo mkdir -p /opt/controllers-ui/ /opt/controllers/
    sudo chown $TARGET_USER:$TARGET_USER /opt/controllers
    sudo tar -C /opt/controllers-ui -xvvzf ~/controllers-ui.tgz
    sudo tar -C /opt/controllers -xvvzf ~/controllers-$VERSION.tgz
    sudo ln -s /opt/controllers/package/rs485.service /etc/systemd/system/rs485.service
    cd /opt/controllers/
    cp package/package.json .
    npm i .
    sudo chmod +x /opt/controllers/package/main.js
    sudo systemctl daemon-reload
    sudo systemctl enable rs485
    sudo systemctl restart rs485
EOF
