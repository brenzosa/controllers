#!/bin/bash

set -o errexit

mkdir -p ./dist/
rm -rf ./dist/*

npm version patch
npm run build
tar -cvvzf ./dist/controllers-ui.tgz -C ../controllers-ui/ ./dist/
