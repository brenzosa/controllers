#!/usr/bin/env node

import { setDebugEnabled } from './impl/buffer-utils';
import { TatsunoControllerV2Tests } from './impl/tatsuno/protocol-tests';
import { TatsunoProductType } from './impl/tatsuno/tatsuno-codes';
import { TatsunoProfile } from './impl/tatsuno/tatsuno-controller';
import { TatsunoPumpSimulator } from './impl/tatsuno/tatsuno-pump-simulator';
import { WebSocketDaemon } from './server/websocket-daemon';
import { WebServerDaemon } from './server/webserver-daemon';
import { ControllerService } from './server/controller.service';
import { environment } from './server/environment';
import { ControllerProfile } from './impl/controllers';
import { EaglestarPumpSimulator } from './impl/eaglestar/eaglestar-pump-simulator';

const profile = {
    host: 'localhost',
    port: 8899,
    timeout: 750,
    logMessages: false,
    pollInterval: 250,
    serialOptions: { baudRate: 19200, dataBits: 8, stopBits: 1, parity: 'even' },

    logStateTransitions: false,
    offlineTryReconnectInterval: 10000,
    inHolsterPollInterval: 1000,
    maxConsecutiveFailures: 6,
} as ControllerProfile;

const unitPrices = new Map([
    [TatsunoProductType.HIGH_OCTANE_UNLEADED, 63.50],
    [TatsunoProductType.REGULAR_UNLEADED, 59.99],
    [TatsunoProductType.DIESEL, 45.32],
]);

(function main(): void {
    let client = false;
    let server = false;
    let startWeb = false;
    let loop = false;

    const args = process.argv.slice(2);
    args.forEach(arg => {
        arg.startsWith("--host=") && (profile.host = arg.substring("--host=".length));
        arg.startsWith("--port=") && (profile.port = parseInt(arg.substring("--port=".length)));
        arg.startsWith("--serialPath=") && (profile.serialPath = arg.substring("--serialPath=".length));
        arg === "--log-messages" && (profile.logMessages = true);
        arg === "--log-state-transitions" && (profile.logStateTransitions = true);
        arg === "--debug" && (setDebugEnabled(true));
        arg === "--client" && (client = true);
        arg === "--loop" && (loop = true);
        arg === "--server" && (server = true);
        arg === "--web" && (startWeb = true);
    });

    if (startWeb) {
        const controllerService = new ControllerService();
        new WebSocketDaemon(controllerService).start();
        new WebServerDaemon(controllerService).start();
    }

    if (args.some(arg => arg === 'run-scenarios')) runScenarios();
    else if (args.some(arg => arg === 'tatsuno-simulator')) tatsunoPumpSimulator(client, server, loop);
    else if (args.some(arg => arg === 'eaglestar-simulator')) eaglestarPumpSimulator(client, server, loop);
    else if (!startWeb) console.error('USAGE:\nnode main.js [run-scenarios|run-simulations|discover|tatsuno-simulator|eaglestar-simulator]');
})();

async function runScenarios(): Promise<void> {
    profile.timeout = 150;
    const tests = new TatsunoControllerV2Tests(new TatsunoProfile({
        stationIds: [0x40, 0x45, 0x64],
        ...profile,
    }));

    tests.controller.unitPrices = unitPrices;
    tests.runScenarios();
}

async function tatsunoPumpSimulator(client: boolean, server: boolean, loop: boolean) {
    environment.logName = 'tatsuno-simulator';
    const pump = new TatsunoPumpSimulator(false, [0x40, 0x45], loop);
    setDebugEnabled(true);
    profile.logMessages = true;

    if (profile.serialPath) await pump.connectSerial(profile.serialPath, profile.serialOptions || {});
    else if (client) await pump.connect(profile.host, profile.port);
    else if (server) await pump.listen(profile.port);
    else console.error("Please specify [--client, --host=<host>, and --port=<port>] or [--server and --port=<port>] or [--serialPath=/dev/ttySC0]");
}

async function eaglestarPumpSimulator(client: boolean, server: boolean, loop: boolean) {
    environment.logName = 'eaglestar-simulator';
    const pump = new EaglestarPumpSimulator(false, [1, 2, 3, 4, 5], loop);
    setDebugEnabled(true);
    profile.logMessages = true;

    if (profile.serialPath) await pump.connectSerial(profile.serialPath, profile.serialOptions || {});
    else if (client) await pump.connect(profile.host, profile.port);
    else if (server) await pump.listen(profile.port);
    else console.error("Please specify [--client, --host=<host>, and --port=<port>] or [--server and --port=<port>] or [--serialPath=/dev/ttySC0]");
}