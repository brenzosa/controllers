import { NozzleStatus, ProductType } from "impl/controllers";
import { Systeminformation } from "systeminformation";
import { IPrice } from "./controller.service";

export enum LogLevel {
    DEBUG = 'DEBUG',
    INFO = 'INFO',
    WARN = 'WARN',
    ERROR = 'ERROR',
}

export abstract class Message {
    constructor(public type: string) { }
}

export class LogMessage extends Message {
    constructor(public level: LogLevel, public message: string) {
        super("LogMessage");
    }
}

export class ControllerMessage extends Message {
    connected?: boolean;
    fuelDispensers?: FuelDispenserMessage[];
    unitPrices?: IPrice[];

    constructor(items?: Partial<ControllerMessage>) {
        super("ControllerMessage");
        Object.assign(this, items);
    }
}

export class ControllerStatusMessage extends Message {
    connected = false;
    connecting = false;
    disconnecting = false;
    scanning = false;
    debugEnabled = false;
    logMessages = false;
    logStateTransitions = false;
    cpuStats = false;

    constructor(items?: Partial<ControllerStatusMessage>) {
        super("ControllerStatusMessage");
        Object.assign(this, items);
    }
}

export class FuelDispenserMessage extends Message {
    id?: string;
    online?: boolean;
    selectedNozzle?: Nozzle;
    nozzles?: Nozzle[];

    constructor(init?: Partial<FuelDispenserMessage>) {
        super("FuelDispenserMessage");
        Object.assign(this, init);
    }
}

export class Nozzle {
    fuelingSequence?: string;
    status?: NozzleStatus;
    unitPrice?: string;
    amount?: string;
    volume?: string;
    totalAmount?: string;
    totalVolume?: string;
    productType?: ProductType;
    locked?: boolean;

    constructor(init?: Partial<Nozzle>) { Object.assign(this, init); }
}

export class CpuCurrentSpeedDataMessage extends Message {
    min?: number;
    max?: number;
    avg?: number;
    cores?: number[];

    constructor(init?: Partial<CpuCurrentSpeedDataMessage>) {
        super("CpuCurrentSpeedDataMessage");
        Object.assign(this, init);
    }
}

export class CpuTemperatureDataMessage extends Message {
    main?: number;
    cores?: number[];
    max?: number;
    socket?: number[];
    chipset?: number;

    constructor(init?: Partial<CpuTemperatureDataMessage>) {
        super("CpuTemperatureDataMessage");
        Object.assign(this, init);
    }
}

export class CpuCurrentLoadDataMessage extends Message {
    avgLoad?: number;
    currentLoad?: number;
    currentLoadUser?: number;
    currentLoadSystem?: number;
    currentLoadNice?: number;
    currentLoadIdle?: number;
    currentLoadIrq?: number;
    rawCurrentLoad?: number;
    rawCurrentLoadUser?: number;
    rawCurrentLoadSystem?: number;
    rawCurrentLoadNice?: number;
    rawCurrentLoadIdle?: number;
    rawCurrentLoadIrq?: number;
    cpus?: Systeminformation.CurrentLoadCpuData[];

    constructor(init?: Partial<CpuCurrentLoadDataMessage>) {
        super("CpuCurrentLoadDataMessage");
        Object.assign(this, init);
    }
}

export class MemDataMessage extends Message {
    total?: number;
    free?: number;
    used?: number;
    active?: number;
    available?: number;
    buffcache?: number;
    buffers?: number;
    cached?: number;
    slab?: number;
    swaptotal?: number;
    swapused?: number;
    swapfree?: number;

    constructor(init?: Partial<MemDataMessage>) {
        super("MemDataMessage");
        Object.assign(this, init);
    }
}
