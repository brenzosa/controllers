import express from "express";
import cors from "cors";
import { Server } from "http";
import { AuthorizeRequestBody, ConnectRequestBody, ControllerService, LockPumpBody, UpdatePasswordRequestBody, UpdatePricesBody } from "./controller.service";
import { environment } from "./environment";
import { logError, logInfo } from "../impl/buffer-utils";
import { tokenValid } from "./ipc-daemon";

const TOKEN_HEADER = 'x-token';

export class WebServerDaemon {
    server = express();
    httpServer?: Server;

    constructor(private controllerService: ControllerService) { }

    public start(): void {
        this.server.use(express.static('../controllers-ui/dist/controllers-ui'));
        this.server.use(cors());
        this.server.use(express.urlencoded({ extended: false }));
        this.server.use(express.json());

        this.server.post('/rest/api/v1/controllers', (req, res) => this.send(req, res, () => this.controllerService.connect(req.body as ConnectRequestBody), true));
        this.server.delete('/rest/api/v1/controllers', (req, res) => this.send(req, res, () => this.controllerService.disconnect(), true));
        this.server.post('/rest/api/v1/controllers/scan', (req, res) => this.send(req, res, () => this.controllerService.scan(), true));
        this.server.post('/rest/api/v1/controllers/prices', (req, res) => this.send(req, res, () => this.controllerService.updatePrices(req.body as UpdatePricesBody), true));
        this.server.post('/rest/api/v1/controllers/cpu-stats', (req, res) => this.send(req, res, () => this.controllerService.toggleCpuStats(true)));
        this.server.delete('/rest/api/v1/controllers/cpu-stats', (req, res) => this.send(req, res, () => this.controllerService.toggleCpuStats(false)));
        this.server.post('/rest/api/v1/controllers/debug-logs', (req, res) => this.send(req, res, () => this.controllerService.toggleDebugLogs(true)));
        this.server.delete('/rest/api/v1/controllers/debug-logs', (req, res) => this.send(req, res, () => this.controllerService.toggleDebugLogs(false)));
        this.server.post('/rest/api/v1/controllers/message-logs', (req, res) => this.send(req, res, () => this.controllerService.toggleMessageLogs(true)));
        this.server.delete('/rest/api/v1/controllers/message-logs', (req, res) => this.send(req, res, () => this.controllerService.toggleMessageLogs(false)));
        this.server.post('/rest/api/v1/controllers/state-transition-logs', (req, res) => this.send(req, res, () => this.controllerService.toggleStateTransitionLogs(true)));
        this.server.delete('/rest/api/v1/controllers/state-transition-logs', (req, res) => this.send(req, res, () => this.controllerService.toggleStateTransitionLogs(false)));

        this.server.post('/rest/api/v1/controllers/dispenser/:id/poke', (req, res) => this.send(req, res, () => this.controllerService.poke(req.params.id), true));
        this.server.post('/rest/api/v1/controllers/dispenser/:id/password', (req, res) => this.send(req, res, () => this.controllerService.updatePassword(req.params.id, req.body as UpdatePasswordRequestBody), true));
        this.server.post('/rest/api/v1/controllers/dispenser/:id/authorize', (req, res) => this.send(req, res, () => this.controllerService.authorize(req.params.id, req.body as AuthorizeRequestBody), true));
        this.server.delete('/rest/api/v1/controllers/dispenser/:id/authorize', (req, res) => this.send(req, res, () => this.controllerService.cancelAuthorization(req.params.id), true));
        this.server.post('/rest/api/v1/controllers/dispenser/:id/lock', (req, res) => this.send(req, res, () => this.controllerService.lockPump(req.params.id, req.body as LockPumpBody), true));
        this.server.delete('/rest/api/v1/controllers/dispenser/:id/lock', (req, res) => this.send(req, res, () => this.controllerService.releasePump(req.params.id), true));

        this.server.get('/rest/api/v1/controllers/transactions', (req, res) => this.send(req, res, () => environment.transactionDAO.loadTransactions(
            req.query.afterTransactionId ? parseInt(req.query.afterTransactionId as string) : undefined,
            req.query.maxResults ? parseInt(req.query.maxResults as string) : undefined,
        )));

        this.server.get('/rest/api/v1/controllers/totals', (req, res) => this.send(req, res, () => environment.transactionDAO.loadTotals(
            req.query.asOfTime ? parseInt(req.query.asOfTime as string) : undefined
        )));

        this.server.get("*", (req, res) => res.redirect("/index.html"));
        this.httpServer = this.server.listen(environment.httpPort, () => logInfo(`WebServer listening on port: ${environment.httpPort}`));
    }

    public stop(): void {
        this.httpServer?.close();
    }

    private async send(req: any, res: any, handler: () => Promise<any>, requiresToken = false): Promise<void> {
        try {
            if (requiresToken && !tokenValid(req?.header(TOKEN_HEADER) || '')) {
                res.status(401).send({ error: 'Invalid Request Token' });
            } else {
                res.send(await handler());
            }
        } catch (err) {
            logError(`API error: ${JSON.stringify(err)}`);
            res.status(500).send({ error: err });
        }
    }
}