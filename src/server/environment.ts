import { TransactionsDAO } from "../dao/TransactionsDAO";
import { BehaviorSubject, Subject } from "rxjs";
import { ControllerMessage, ControllerStatusMessage, CpuCurrentSpeedDataMessage, CpuTemperatureDataMessage, FuelDispenserMessage, LogMessage, MemDataMessage } from "./messages";

export const environment = {
    keepAliveInterval: 30000,

    httpPort: parseInt(`${process.env.CONTROLLER_HTTP_PORT}`) || 3000,
    wsPort: parseInt(`${process.env.CONTROLLER_WS_PORT}`) || 3001,
    logs$: new Subject<LogMessage>(),
    controllerStatus$: new BehaviorSubject<ControllerStatusMessage>(new ControllerStatusMessage()),
    controller$: new BehaviorSubject<ControllerMessage>(new ControllerMessage()),
    fuelDispenserUpdate$: new Subject<FuelDispenserMessage>(),
    stats$: new Subject<CpuCurrentSpeedDataMessage | CpuTemperatureDataMessage | MemDataMessage>(),

    transactionDAO: new TransactionsDAO(),
    logName: 'controller',
}