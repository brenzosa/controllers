import { filter, interval } from 'rxjs';
import { cpuCurrentSpeed, cpuTemperature, currentLoad, mem } from 'systeminformation';
import { isDebugEnabled, logError, logInfo, setDebugEnabled } from '../impl/buffer-utils';
import { Controllers, ControllerType, ControllerProfile, IController, IFuelDispenser, INozzle } from '../impl/controllers';
import { environment } from './environment';
import { ControllerMessage, ControllerStatusMessage, CpuCurrentLoadDataMessage, CpuCurrentSpeedDataMessage, CpuTemperatureDataMessage, FuelDispenserMessage, MemDataMessage, Nozzle } from "./messages";

export interface IPrice {
    productType: string,
    label: string,
    price?: string,
}

export type ConnectRequestBody = {
    unitPrices: IPrice[];
    type: ControllerType;
    profile: ControllerProfile;
    testMode: boolean;
}

export type UpdatePricesBody = IPrice[];

export type LockPumpBody = {
    unlockCode: string;
}

export type UpdatePasswordRequestBody = {
    password: string;
}
export type AuthorizeRequestBody = {
    cost?: number;
    volume?: number;
}

export class ControllerService {
    controller?: IController;
    enableCpuStats = false;

    constructor() {
        interval(1000)
            .pipe(filter(() => !!this.enableCpuStats))
            .subscribe(() => this.pollCpuStats());
    }

    public async connect(request: ConnectRequestBody): Promise<ControllerMessage> {
        await this.controller?.disconnect();
        await environment.transactionDAO.disconnect();
        await environment.transactionDAO.connect();

        logInfo(`Handling connect request: ${JSON.stringify(request)}`);
        environment.controllerStatus$.next(new ControllerStatusMessage({ ...this.status(), connecting: true }));

        try {
            const unitPrices = new Map<string, number>();
            request.unitPrices
                .filter(price => !!price.price && parseFloat(price.price) > 0)
                .forEach(price => unitPrices.set(price.productType, parseFloat(price.price || '0')));

            const controller = Controllers.createController(request.type, request.profile);
            controller.unitPrices = unitPrices;

            await controller.connect(request.testMode);
            logInfo(`API connected!`);

            this.controller = controller;
            controller.fuelDispensers.map(fd => fd.onUpdate = () => environment.fuelDispenserUpdate$.next(this.fuelDispenser(fd)));
            this.enableCpuStats = !!request.profile?.enableCpuStats;
            return this.nextController(controller);
        } catch (err) {
            environment.controllerStatus$.next(this.status());
            throw err;
        }
    }

    public async disconnect(): Promise<ControllerMessage> {
        if (this.controller) {
            await this.controller.disconnect();
            await environment.transactionDAO.disconnect();
            return this.nextController(this.controller);
        } else {
            throw `Not connected`;
        }
    }

    public async scan(): Promise<ControllerMessage> {
        if (this.controller && !this.controller.scanning) {
            this.controller.scan().catch(err => logError(`Scan error: ${err}`));
            return this.nextController(this.controller);
        } else {
            throw (!this.controller ? `Not connected` : `Scan already in progress`);
        }
    }

    public async updatePrices(request: UpdatePricesBody): Promise<ControllerMessage> {
        if (this.controller) {
            const unitPrices = new Map<string, number>();
            request
                .filter(price => !!price.price && parseFloat(price.price) > 0)
                .forEach(price => unitPrices.set(price.productType, parseFloat(price.price || '0')));

            await this.controller.updatePrices(unitPrices);
            return this.nextController(this.controller);
        } else {
            throw `Not connected`;
        }
    }

    public async toggleCpuStats(enable: boolean): Promise<ControllerMessage> {
        if (this.controller) {
            this.enableCpuStats = enable;
            return this.nextController(this.controller);
        } else {
            throw `Not connected`;
        }
    }

    public async toggleDebugLogs(enable: boolean): Promise<ControllerMessage> {
        if (this.controller) {
            setDebugEnabled(enable);
            return this.nextController(this.controller);
        } else {
            throw `Not connected`;
        }
    }

    public async toggleMessageLogs(enable: boolean): Promise<ControllerMessage> {
        if (this.controller) {
            this.controller.profile.logMessages = enable;
            return this.nextController(this.controller);
        } else {
            throw `Not connected`;
        }
    }

    public async toggleStateTransitionLogs(enable: boolean): Promise<ControllerMessage> {
        if (this.controller) {
            this.controller.profile.logStateTransitions = enable;
            return this.nextController(this.controller);
        } else {
            throw `Not connected`;
        }
    }

    public async poke(id: string): Promise<void> {
        if (this.controller) {
            const fd = this.controller.fuelDispensers.filter(fd => fd.id === id)[0]
            if (!fd) throw `Fuel Dispenser not found: ${id}`;
            else await fd.poke();
        } else {
            throw `Not connected`;
        }
    }

    public async updatePassword(id: string, request: UpdatePasswordRequestBody): Promise<void> {
        if (this.controller) {
            const fd = this.controller.fuelDispensers.filter(fd => fd.id === id)[0]
            if (!fd) throw `Fuel Dispenser not found: ${id}`;
            else fd.unlockPassword = request.password;
        } else {
            throw `Not connected`;
        }
    }

    public async authorize(id: string, request: AuthorizeRequestBody): Promise<void> {
        if (this.controller) {
            const fd = this.controller.fuelDispensers.filter(fd => fd.id === id)[0]
            if (!fd) throw `Fuel Dispenser not found: ${id}`;
            else if (request && request.cost) await fd.authorizeTotalCost(request.cost);
            else if (request && request.volume) await fd.authorizeTotalVolume(request.volume);
            else await fd.authorize();
        } else {
            throw `Not connected`;
        }
    }

    public async cancelAuthorization(id: string): Promise<void> {
        if (this.controller) {
            const fd = this.controller.fuelDispensers.filter(fd => fd.id === id)[0]
            if (!fd) throw `Fuel Dispenser not found: ${id}`;
            else await fd.cancelAuthorization();
        } else {
            throw `Not connected`;
        }
    }

    public async lockPump(id: string, request: LockPumpBody): Promise<void> {
        if (this.controller) {
            const fd = this.controller.fuelDispensers.filter(fd => fd.id === id)[0]
            if (!fd) throw `Fuel Dispenser not found: ${id}`;
            else await fd.lockPump(request.unlockCode);
        } else {
            throw `Not connected`;
        }
    }

    public async releasePump(id: string): Promise<void> {
        if (this.controller) {
            const fd = this.controller.fuelDispensers.filter(fd => fd.id === id)[0]
            if (!fd) throw `Fuel Dispenser not found: ${id}`;
            else await fd.releasePump();
        } else {
            throw `Not connected`;
        }
    }

    private status(): ControllerStatusMessage {
        return new ControllerStatusMessage({
            connected: !!this.controller?.connected,
            scanning: !!this.controller?.scanning,
            debugEnabled: !!isDebugEnabled(),
            logMessages: !!this.controller?.profile?.logMessages,
            logStateTransitions: !!this.controller?.profile?.logStateTransitions,
            cpuStats: !!this.enableCpuStats,
        });
    }

    private nextController(controller: IController): ControllerMessage {
        const prices = [...controller.unitPrices].map(([productType, price]) => {
            return {
                productType, price: price && price > 0 ? price.toString() : undefined
            } as IPrice
        });

        const controllerMessage = new ControllerMessage({
            connected: controller.connected,
            unitPrices: prices,
            fuelDispensers: controller.fuelDispensers.map(fd => this.fuelDispenser(fd)),
        });

        environment.controller$.next(controllerMessage);
        environment.controllerStatus$.next(this.status());
        this.controller?.fuelDispensers
            .forEach(fd => environment.fuelDispenserUpdate$.next(this.fuelDispenser(fd)));

        return controllerMessage;
    }

    public fuelDispenser(fuelDispenser: IFuelDispenser): FuelDispenserMessage {
        return new FuelDispenserMessage({
            id: fuelDispenser.id,
            online: fuelDispenser.online,
            selectedNozzle: fuelDispenser.selectedNozzle ? this.nozzle(fuelDispenser.selectedNozzle) : undefined,
            nozzles: fuelDispenser.nozzles.map(nozzle => this.nozzle(nozzle))
        });
    }

    private nozzle(nozzle: INozzle): Nozzle {
        return new Nozzle({
            fuelingSequence: nozzle.fuelingSequence,
            status: nozzle.status,
            unitPrice: nozzle.unitPrice,
            amount: nozzle.amount,
            volume: nozzle.volume,
            totalAmount: nozzle.totalAmount,
            totalVolume: nozzle.totalVolume,
            productType: nozzle.productType,
            locked: nozzle.locked,
        });
    }

    private async pollCpuStats(): Promise<void> {
        try {
            environment.stats$.next(new CpuCurrentSpeedDataMessage(await cpuCurrentSpeed()));
            environment.stats$.next(new CpuCurrentLoadDataMessage(await currentLoad()));
            environment.stats$.next(new CpuTemperatureDataMessage(await cpuTemperature()));
            environment.stats$.next(new MemDataMessage(await mem()));
        } catch (err) {
            logError(`Error polling stats: ${err}`);
        }
    }
}