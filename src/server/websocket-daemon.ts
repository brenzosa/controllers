import { logInfo } from '../impl/buffer-utils';
import { Subscription } from 'rxjs';
import { WebSocket, WebSocketServer } from 'ws';
import { ControllerService } from './controller.service';
import { environment } from './environment';

export class WebSocketDaemon {
    private ws?: WebSocketServer;
    private alive = new Set<WebSocket>();
    private subscriptions = new Map<WebSocket, Set<Subscription>>();
    private keepAliveInterval?: NodeJS.Timer;

    constructor(private controllerService: ControllerService) {
    }

    public start(): void {
        this.ws = new WebSocketServer({ port: environment.wsPort });
        this.ws.on('connection', client => {
            logInfo(`(${this.constructor.name}) Got connection...`);
            this.alive.add(client);
            this.register(client);
            client.on('pong', () => this.alive.add(client));
        });

        this.keepAliveInterval = setInterval(() => {
            this.ws?.clients.forEach(client => {
                const alive = this.alive.delete(client);
                if (!alive) this.terminate(client); else client.ping();
            });
        }, environment.keepAliveInterval);

        this.ws.on('close', () => clearInterval(this.keepAliveInterval!));
        logInfo(`WebSocketServer listening on port: ${environment.wsPort}`);
    }

    public stop(): void {
        this.ws?.clients.forEach(ws => this.terminate(ws));
        this.ws?.close();
        this.alive.clear();
    }

    private register(client: WebSocket): void {
        const subscriptions = this.subscriptions.get(client) || new Set<Subscription>();
        this.subscriptions.set(client, subscriptions);

        subscriptions.add(environment.logs$.subscribe(message => client.send(JSON.stringify(message))));
        subscriptions.add(environment.controller$.subscribe(message => client.send(JSON.stringify(message))));
        subscriptions.add(environment.controllerStatus$.subscribe(message => client.send(JSON.stringify(message))));
        subscriptions.add(environment.fuelDispenserUpdate$.subscribe(message => client.send(JSON.stringify(message))));
        subscriptions.add(environment.stats$.subscribe(message => client.send(JSON.stringify(message))));

        this.controllerService.controller?.fuelDispensers
            .forEach(fd => client.send(JSON.stringify(this.controllerService.fuelDispenser(fd))));
    }

    private terminate(client: WebSocket): void {
        const subscriptions = this.subscriptions.get(client);
        if (subscriptions) {
            subscriptions?.forEach(subs => subs?.unsubscribe());
            this.subscriptions.delete(client);
        }

        client?.terminate();
    }
}
