import NodeIPC from 'node-ipc';
import { logDebug } from '../impl/buffer-utils';
import { delay, Subject, tap } from 'rxjs';

const CONTROLLER_IPC = process.env.CONTROLLER_IPC;
const TOKEN_EXPIRY = 5000;
const tokens: Set<string> = new Set<string>();
const tokens$ = new Subject<string>();

if (CONTROLLER_IPC) {
    tokens$
        .pipe(tap(token => tokens.add(token)))
        .pipe(tap(token => logDebug(`new token: ${token}, all tokens: [${[...tokens]}]`)))
        .pipe(delay(TOKEN_EXPIRY))
        .pipe(tap(token => tokens.delete(token)))
        .subscribe(token => logDebug(`token expired: ${token}, remaining tokens: [${[...tokens]}]`));

    const ipc = new NodeIPC.IPC();
    ipc.config.id = `${CONTROLLER_IPC}`;
    ipc.config.retry = 1500;

    type TokenEvent = {
        token: string;
    }

    ipc.connectTo(ipc.config.id, () => {
        const client = ipc.of[ipc.config.id].on('connect', (): void => logDebug('Connected...'));
        client.on('token', (data: TokenEvent): void => tokens$.next(data.token));
    });
}


export const tokenValid = (token: string): boolean => {
    return !CONTROLLER_IPC || tokens.delete(token);
}