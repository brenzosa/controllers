declare module "crc" {
    export function crc16ccitt(buf: Uint8Array | number[], previous?: number): number;
}