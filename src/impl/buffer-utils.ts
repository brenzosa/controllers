import { environment } from "../server/environment";
import { LogLevel, LogMessage } from "../server/messages";
import { createLogger, format, Logger } from 'winston';
import DailyRotateFile from "winston-daily-rotate-file";

let _logger: Logger | null = null;
const logger = () => _logger || (_logger = createLogger({
    level: 'info',
    format: format.json(),
    defaultMeta: { service: `${environment.logName}-service` },
    transports: [
        new DailyRotateFile({
            filename: `${environment.logName}-%DATE%.log`,
            dirname: './logs/',
            auditFile: './logs/.audit.json',
            datePattern: 'YYYY-MM-DD-HH',
            zippedArchive: true,
            maxSize: '20m',
            maxFiles: '14d',
        })
    ],
}));

const charCode0 = '0'.charCodeAt(0);
const charCode9 = '9'.charCodeAt(0);

export function isDebugEnabled(): boolean {
    return logger().isDebugEnabled();
}

export function setDebugEnabled(enabled: boolean): void {
    logger().level = enabled ? 'debug' : 'info';
}

export function encode(text: string): Uint8Array {
    return Uint8Array.from(text.split('').map(letter => letter.charCodeAt(0)));
}

export function decode(buffer: Uint8Array): string {
    return buffer ? [...buffer].map(x => String.fromCharCode(x)).join('') : '';
}

export function toHexString(buffer: Uint8Array): string {
    return [...buffer].map(v => v.toString(16).padStart(2, "0")).join('').toUpperCase();
}

export function bcc(data?: Uint8Array): number {
    return data && data.length > 0 ? [...data].reduce((b1, b2) => b1 ^ b2) : 0
}

export function bcd(digits: string): number[] {
    if (digits.length % 2 !== 0) throw `digit string must be divisble by 2`;
    const bcdArray = new Array(digits.length / 2).fill(0);
    for (let p = 0; p < digits.length; p++) {
        const digit = digits.charCodeAt(p);
        if (digit < charCode0 || digit > charCode9) throw `Invalid digit at position ${p}: ${digits}`;
        if (p % 2 === 0) bcdArray[p >> 1] += (((digit - charCode0) << 4) & 0xf0); // hi nibble
        else bcdArray[p >> 1] += ((digit - charCode0) & 0x0f); // low nibble
    }

    return bcdArray;
}

function timestamp(): string {
    return `${new Date().toISOString()}`;
}

export function logDebug(message: string): void {
    if (logger().isDebugEnabled() && !!message) {
        const formattedMessage = `${timestamp()} [DEBUG]: ${message}`;
        environment.logs$.next(new LogMessage(LogLevel.DEBUG, formattedMessage));
        logger().debug(formattedMessage);
    }
}

export function logInfo(message: string): void {
    if (logger().isInfoEnabled() && !!message) {
        const formattedMessage = `${timestamp()} [ INFO]: ${message}`
        environment.logs$.next(new LogMessage(LogLevel.INFO, formattedMessage));
        logger().info(formattedMessage);
    }
}

export function logWarn(message: string): void {
    if (logger().isWarnEnabled() && !!message) {
        const formattedMessage = `${timestamp()} [ WARN]: ${message}`
        environment.logs$.next(new LogMessage(LogLevel.WARN, formattedMessage));
        logger().warn(formattedMessage);
    }
}

export function logError(message: string): void {
    if (logger().isErrorEnabled() && !!message) {
        const formattedMessage = `${timestamp()} [ERROR]: ${message}`
        environment.logs$.next(new LogMessage(LogLevel.ERROR, formattedMessage));
        logger().error(formattedMessage);
    }
}
