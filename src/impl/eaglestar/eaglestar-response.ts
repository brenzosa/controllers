import { toHexString } from "../buffer-utils";
import { RS485Response } from "../protocol-base";
import { commandName } from "./eaglestar-rs485-protocol";

export class EaglestarResponse extends RS485Response {
    private responseFlags: string[] = [];

    get flags(): string[] {
        const flags: string[] = [...this.responseFlags];
        flags.push(this.success() ? "SUCCESS" : "FAILED");
        if (this.invalid) flags.push("INVALID");
        if (this.timedOut) flags.push("TIMEOUT");
        return flags;
    }

    constructor(public nozzleNumber: number = -1, public command: number = -1) {
        super();
    }

    success(): boolean {
        return !this.invalid && !this.timedOut;
    }

    addFlag(flag: string): void {
        this.responseFlags.push(flag);
    }

    toString(): string {
        return `EaglestarRequest: Nozzle#0x${this.nozzleNumber.toString(16).padStart(2, '0').toUpperCase()}`
            + `, command: ${commandName(this.command)}`
            + `, flags=[${this.flags.join()}], data: 0x${toHexString(this.buffer)}`;
    }
}