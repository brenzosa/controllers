import { bcc, isDebugEnabled, logDebug } from "../buffer-utils";
import { RS485ProtocolBase, ProtocolState } from "../protocol-base";
import { Commands } from "./eaglestar-codes";
import { EaglestarRequest } from "./eaglestar-request";
import { EaglestarResponse } from "./eaglestar-response";

export const TIMEOUT = 1000;

enum EaglestarState {
    INITIAL, SYN1, SYN2, SYN3, COMMAND, NOZZLE_NUMBER, DATA, BCC, FINAL
}

export class EaglestarRS485Protocol extends RS485ProtocolBase<EaglestarRequest, EaglestarResponse> {
    expectedDataLen = 0;

    constructor(timeout: number = TIMEOUT) {
        super(EaglestarState.INITIAL, EaglestarState.FINAL, timeout);

        this.stateTransitions.set(EaglestarState.INITIAL, () => { throw `Data not expected` });
        this.stateTransitions.set(EaglestarState.SYN1, b => this.checkSyn(b, EaglestarState.SYN2));
        this.stateTransitions.set(EaglestarState.SYN2, b => this.checkSyn(b, EaglestarState.SYN3));
        this.stateTransitions.set(EaglestarState.SYN3, b => this.checkSyn(b, EaglestarState.COMMAND));
        this.stateTransitions.set(EaglestarState.COMMAND, b => this.readCommand(b));
        this.stateTransitions.set(EaglestarState.NOZZLE_NUMBER, b => this.readNozzleNumber(b));
        this.stateTransitions.set(EaglestarState.DATA, b => this.readData(b));
        this.stateTransitions.set(EaglestarState.BCC, b => this.checkBCC(b));
    }

    protected onRead(buffer: Uint8Array): void {
        if (isDebugEnabled()) logDebug(`${this.constructor.name}  IN: ${toProtocolString(buffer)}`);
        return super.onRead(buffer);
    }

    public send(buffer: Uint8Array): void {
        if (isDebugEnabled()) logDebug(`${this.constructor.name} OUT: ${toProtocolString(buffer)}`);
        return super.send(buffer);
    }

    protected newEmptyResponse(request?: EaglestarRequest): EaglestarResponse {
        const nozzleNumber = request?.nozzleNumber || 0;
        const command = request?.command || 0
        return new EaglestarResponse(nozzleNumber, command);
    }

    protected newTimedoutResponse(request: EaglestarRequest): EaglestarResponse {
        const response = new EaglestarResponse(request.nozzleNumber, request.command);
        response.timedOut = true;
        return response;
    }

    protected sendRequest(request: EaglestarRequest): ProtocolState {
        const bccValue = request.command ^ request.nozzleNumber ^ bcc(request.buffer);
        this.send(Uint8Array.of(Commands.SYN, Commands.SYN, Commands.SYN, request.command, request.nozzleNumber, ...request.buffer, bccValue));

        if (request.command === Commands.CLEAR_TRANSACTION) {
            // Pump does not respond to the CLEAR_TRANSACTION command!
            setTimeout(() => {
                this.publishResponse();
                this.resetState();
            }, 10);

            return EaglestarState.INITIAL;
        } else {
            return EaglestarState.SYN1;
        }
    }

    protected responseMatchesRequest(request: EaglestarRequest, response: EaglestarResponse): boolean {
        if (request.nozzleNumber !== response.nozzleNumber) return false;
        if (request.command === response.command) return true;
        if (request.command === Commands.GET_DATA && response.command === (Commands.GET_DATA + 1)) return true;
        if (request.command === Commands.GET_TRANSACTION && response.command === (Commands.GET_TRANSACTION + 2)) return true;
        return false;
    }

    private checkSyn(value: number, nextState: EaglestarState): ProtocolState {
        if (value !== Commands.SYN) {
            throw `Expected SYN, but got: 0x${value.toString(16).padStart(2, '0').toUpperCase()}`;
        }

        return nextState;
    }

    private readCommand(value: number): ProtocolState {
        if (!this.request) {
            throw `Not expecting a command`;
        }

        this.expectedDataLen = ((command: number) => {
            switch (command) {
                case Commands.PRESET: return 1;
                case Commands.SET_PRICE: return 1;
                case Commands.STOP_FUELING: return 0;
                case Commands.AUTHORIZE: return 0;
                case Commands.SET_MODE: return 0;
                case Commands.GET_TOTALS: return 12;
                case Commands.CANCEL_PRESET: return 1;
                case Commands.SET_DATETIME: return 0;
                case Commands.TEST_MODE_ON: return 0;
                case Commands.TEST_MODE_OFF: return 0;
                case Commands.GET_VERSION: return 3;
                case Commands.UNLOCK: return 0;
                case Commands.SET_FUELING_MODE: return 0;

                case Commands.GET_DATA:
                case Commands.GET_DATA + 1: return 5;

                case Commands.GET_TRANSACTION:
                case Commands.GET_TRANSACTION + 2: return 36;

                case Commands.CLEAR_TRANSACTION: return 0;
                default: throw `Unrecognized command: 0x${command.toString(16).padStart(2, '0').toUpperCase()}`;
            }
        })(value);

        this.response = new EaglestarResponse();
        this.response.command = value;
        return EaglestarState.NOZZLE_NUMBER;
    }

    private readNozzleNumber(value: number): ProtocolState {
        if (!this.response) {
            throw `No response has been prepared for reading nozzle number`;
        } else {
            this.response.nozzleNumber = value;
            return EaglestarState.DATA;
        }
    }

    private readData(b: number): ProtocolState {
        if (!this.response) {
            throw `No response has been prepared for reading data`;
        } else if (this.expectedDataLen === 0) {
            return this.checkBCC(b);
        } else {
            this.response.buffer = Uint8Array.of(...this.response.buffer, b);
            return (this.response.buffer.length < this.expectedDataLen)
                ? EaglestarState.DATA
                : EaglestarState.BCC;
        }
    }

    private checkBCC(bccValue: number): ProtocolState {
        if (!this.response) {
            throw `Invalid state: No response data object ready`;
        }

        const bccExpected = this.response.command ^ this.response.nozzleNumber ^ bcc(this.response.buffer);
        if (bccExpected !== bccValue) {
            throw `BCC mismatch (0x${bccValue.toString(16).toUpperCase()} <> 0x${bccExpected.toString(16).toUpperCase()})`;
        }

        // all done
        return EaglestarState.FINAL;
    }

    protected stateName(state: ProtocolState): string {
        switch (state) {
            case EaglestarState.INITIAL:
                return 'INITIAL';
            case EaglestarState.SYN1:
                return 'SYN1';
            case EaglestarState.SYN2:
                return 'SYN2';
            case EaglestarState.SYN3:
                return 'SYN3';
            case EaglestarState.COMMAND:
                return 'COMMAND';
            case EaglestarState.NOZZLE_NUMBER:
                return 'NOZZLE_NUMBER';
            case EaglestarState.DATA:
                return 'DATA';
            case EaglestarState.BCC:
                return 'BCC';
            case EaglestarState.FINAL:
                return 'FINAL';
            default:
                return `${state}`;
        }
    }
}

export function toProtocolString(buffer: Uint8Array): string {
    return [...buffer].map((b, index) => {
        if (index <= 3) {
            return commandName(b);
        } else if (index === 4) {
            // nozzle number
            return `<N#0x${b.toString(16).padStart(2, '0').toUpperCase()}>`;
        } else if (index === buffer.length - 1) {
            return `<BCC:0x${b.toString(16).padStart(2, '0').toUpperCase()}>`;
        }

        return `${b.toString(16).padStart(2, '0').toUpperCase()}`;
    }).join('');
}

export function commandName(command: number): string {
    switch (command) {
        case Commands.SYN:
            return '<SYN>';
        case Commands.PRESET:
            return '<PRESET>';
        case Commands.SET_PRICE:
            return '<SET_PRICE>';
        case Commands.STOP_FUELING:
            return '<STOP_FUELING>';
        case Commands.AUTHORIZE:
            return '<AUTHORIZE>';
        case Commands.SET_MODE:
            return '<SET_MODE>';
        case Commands.GET_TOTALS:
            return '<GET_TOTALS>';
        case Commands.CANCEL_PRESET:
            return '<CANCEL_PRESET>';
        case Commands.SET_DATETIME:
            return '<SET_DATETIME>';
        case Commands.TEST_MODE_ON:
            return '<TEST_MODE_ON>';
        case Commands.TEST_MODE_OFF:
            return '<TEST_MODE_OFF>';
        case Commands.GET_VERSION:
            return '<GET_VERSION>';
        case Commands.UNLOCK:
            return '<UNLOCK>';
        case Commands.SET_FUELING_MODE:
            return '<SET_FUELING_MODE>';
        case Commands.GET_DATA:
            return '<GET_DATA:VOLUME>';
        case Commands.GET_DATA + 1:
            return '<GET_DATA:COST>';
        case Commands.GET_TRANSACTION:
            return '<GET_TRANSACTION>';
        case Commands.GET_TRANSACTION + 2:
            return '<GET_TRANSACTION:NO_DATA>';
        case Commands.CLEAR_TRANSACTION:
            return '<CLEAR_TRANSACTION>';
        default:
            return `<0x${command.toString(16).padStart(2, '0').toUpperCase()}>`
    }
}