export enum Commands {
    SYN = 0xFA,
    PRESET = 0x01,
    SET_PRICE = 0x02,
    STOP_FUELING = 0x04,
    AUTHORIZE = 0x05,
    SET_MODE = 0x06,
    GET_TOTALS = 0x07,
    CANCEL_PRESET = 0x08,
    SET_DATETIME = 0x0C,
    TEST_MODE_ON = 0x0D,
    TEST_MODE_OFF = 0x0E,
    GET_VERSION = 0x0F,
    UNLOCK = 0x10,
    SET_FUELING_MODE = 0x11,
    GET_DATA = 0x14, // replies 0x14 (liter preset) or 0x15 (money preset)
    GET_TRANSACTION = 0x16, // replies 0x16 (valid data) or 0x18 (invalid/no data)
    CLEAR_TRANSACTION = 0x17,
}

export enum PresetType {
    VOL_LIMIT_NORM = 0b00000100,
    CSH_LIMIT_NORM = 0b00000101,
    VOL_LIMIT_TEST = 0b00000000,
    CSH_LIMIT_TEST = 0b00000001,
}

export enum CommunicationMode {
    OFF_MODE = 0xAA,
    ON_MODE = 0x55,
}

export enum AfterFuelingMode {
    LOCKED_CANNOT_UNOCK = 0xAA,
    LOCKED_CAN_UNLOCK = 0xA0,
    UNLOCKED = 0x55,
}