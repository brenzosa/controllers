import { Priority } from "../protocol-base";
import { OpenOptions } from "serialport";
import { logError, logInfo, logWarn, toHexString } from "../buffer-utils";
import { ControllerProfile, IFuelDispenser, ProductType } from "../controllers";
import { Commands } from "./eaglestar-codes";
import { EaglestarFuelDispenser } from "./eaglestar-fuel-dispenser";
import { EaglestarRequest } from "./eaglestar-request";
import { EaglestarResponse } from "./eaglestar-response";
import { commandName, EaglestarRS485Protocol } from "./eaglestar-rs485-protocol";
import { ControllerBase } from "../controller-base";

interface IEaglestarPump {
    nozzleNumber: number;
    productType: string;
}

export class EaglestarProfile implements ControllerProfile {
    host = '';
    port = 0;
    pumps: IEaglestarPump[] = [];
    timeout = 1000;
    pollInterval = 200;
    maxConsecutiveFailures = 6;
    logMessages = false;
    logStateTransitions = false;
    enableCpuStats = false;
    serialPath?: string;
    serialOptions?: OpenOptions;
    offlineTryReconnectInterval = 10000;
    inHolsterPollInterval = 1000;

    lockAfterFueling = false;
    enableTestMode = false;

    constructor(init?: Partial<EaglestarProfile>) {
        Object.assign(this, init);
    }
}

export class EaglestarController extends ControllerBase<EaglestarProfile, EaglestarRS485Protocol, EaglestarFuelDispenser, EaglestarRequest, EaglestarResponse> {
    get enableTestMode(): boolean { return this.profile.enableTestMode || false; }

    constructor(public profile: EaglestarProfile) {
        super(profile, new EaglestarRS485Protocol(profile.timeout));
        super.fuelDispensers = profile.pumps.map(pump => {
            if (!pump.nozzleNumber) throw `pump.nozzleNumber required.`;
            if (!pump.productType) throw `pump.productType required.`;
            return new EaglestarFuelDispenser(this, pump.nozzleNumber, pump.productType);
        });
    }

    async scan(): Promise<void> {
        this.scanning = true;
        this.notify();

        try {
            const found: number[] = [];
            for (let nozzleNumber = 0x01; nozzleNumber <= 0xff; nozzleNumber++) {
                logInfo(`Trying Nozzle #0x${nozzleNumber.toString(16)}...`);
                try {
                    const version = await this.getVersion(nozzleNumber, Priority.LOWEST);
                    logInfo(`Found pump #0x${nozzleNumber.toString(16)}, version: ${version}`);
                    found.push(nozzleNumber);
                } catch (err) {
                    // no reply... ignore...
                }
            }

            logInfo(`Nozzle numbers: ${found.map(sa => sa.toString(16)).join()}`);
            this.notify();
        } catch (err) {
            logError(`Error during scan: ${err}`);
        } finally {
            this.scanning = false;
            this.notify();
        }
    }

    async sendMessage(request: EaglestarRequest): Promise<EaglestarResponse> {
        if (this.profile.logMessages) {
            logInfo(`POS -> Nozzle #0x${request.nozzleNumber.toString(16).padStart(2, '0').toUpperCase()} (${commandName(request.command)}): ${toHexString(request.buffer)}`);
        }

        const response = await this.rs485Protocol.sendMessage(request);
        if (this.profile.logMessages) {
            logInfo(`POS <- Nozzle #0x${response.nozzleNumber.toString(16).padStart(2, '0').toUpperCase()} (${commandName(response.command)}, ${response.flags}): ${toHexString(response.buffer)}`);
        }

        return response;
    }

    async tryConnect(fd: EaglestarFuelDispenser, priority: Priority): Promise<void> {
        fd.reset();
        await fd.initialize(priority);
    }

    async transactionSaved(fuelDispenser: IFuelDispenser, fuelingSequence: string): Promise<void> {
        await super.transactionSaved(fuelDispenser, fuelingSequence);
        return (fuelDispenser as EaglestarFuelDispenser).clearLastTransaction().then(response => {
            if (!response.success()) throw `Clear Transaction error: ${response}`;
        });
    }

    async updatePrices(prices: Map<ProductType, number>): Promise<void> {
        this.unitPrices = prices;

        for (const fd of this.fuelDispensers.filter(fd => fd.online)) {
            try {
                await fd.setPrice(Priority.HIGH);
            } catch (err) {
                logWarn(`Error updating price for fd#${fd.id}`);
            }
        }
    }

    protected async pollFuelDispenser(fd: EaglestarFuelDispenser): Promise<void> {
        if (!(await fd.getData()).success()) throw `Get data failed`;
    }

    private async getVersion(nozzleNumber: number, priority: Priority = Priority.LOW): Promise<string> {
        const request = new EaglestarRequest(nozzleNumber, Commands.GET_VERSION, priority);
        const response = await this.sendMessage(request);
        if (response.buffer.length !== 3) throw `Get version request mode failed`;

        const version = response.buffer[1].toString(16).padStart(2, '0') + '.' + response.buffer[2].toString(16).padStart(2, '0');
        return version;
    }
}