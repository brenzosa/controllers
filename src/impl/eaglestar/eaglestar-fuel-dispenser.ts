import { bcd, decode, logDebug, logInfo, logWarn, toHexString } from "../buffer-utils";
import { IFuelDispenser, INozzle, NozzleStatus } from "../controllers";
import { EaglestarResponse } from "./eaglestar-response";
import { EaglestarController } from "./eaglestar-controller";
import { AfterFuelingMode, Commands, CommunicationMode, PresetType } from "./eaglestar-codes";
import { EaglestarRequest } from "./eaglestar-request";
import { Priority } from "../protocol-base";

const MAX_PRICE = 327.67;
const MIN_PRICE = 0.02;

enum PresetStatus {
    VOLUME = 'VOLUME',
    COST = 'COST',
    NONE = 'NONE'
}

class EaglestarNozzle implements INozzle {
    get fuelingSequence(): string { return this.linkNo.toFixed(0).padStart(4, '0'); }
    status = NozzleStatus.IN_HOLSTER;
    unitPrice = "";
    amount = "";
    volume = "";
    totalAmount = "";
    totalVolume = "";
    totalKilos = "";
    productType = '';

    transactionAvailable = false;
    locked = false;
    presetFromPOS = false;
    memberCard = '';
    staffId = '';
    linkNo = 1;

    constructor(public nozzleNumber: number) { }
}

export class EaglestarFuelDispenser implements IFuelDispenser {
    get id(): string { return `${this.nozzleNumber.toString(16).padStart(2, '0').toUpperCase()}`; }
    get online(): boolean { return this.pricesSent && this.dateTimeSent && this.commModeSent && this.testModeSent }
    selectedNozzle: EaglestarNozzle;
    nozzles: EaglestarNozzle[];
    onUpdate?: () => void;
    unlockPassword = '000';

    public failures = 0;
    private pricesSent = false;
    private dateTimeSent = false;
    private commModeSent = false;
    private testModeSent = false;
    private commMode?: number;
    private version?: string;

    private lastVolume = Uint8Array.of();
    private lastAmount = Uint8Array.of();
    private lastTransaction = Uint8Array.of();
    private lastTotals = Uint8Array.of();
    private lockAfterFueling = true;

    get afterFuelingMode(): AfterFuelingMode { return this.lockAfterFueling ? AfterFuelingMode.LOCKED_CAN_UNLOCK : AfterFuelingMode.UNLOCKED; }
    get price(): number {
        const unitPrice = this.controller.unitPrices.get(this.productType);
        if (!unitPrice) throw `Please set a unit price for product type ${this.productType}.`;
        if (unitPrice > MAX_PRICE || unitPrice < MIN_PRICE) throw `Price is out of range (${unitPrice}).`;
        return unitPrice;
    }

    constructor(private controller: EaglestarController, private nozzleNumber: number, private productType: string) {
        const nozzle = new EaglestarNozzle(nozzleNumber);
        this.selectedNozzle = nozzle;
        this.nozzles = [nozzle];
        this.lockAfterFueling = controller.profile.lockAfterFueling;
    }

    toString(): string {
        return `Eaglestar Nozzle#0x${this.nozzleNumber.toString(16).padStart(2, '0').toUpperCase()}`
            + `, pricesSent=${this.pricesSent}, dateTimeSent=${this.dateTimeSent}`;
    }

    public reset(): void {
        this.failures = 0;
        this.pricesSent = false;
        this.dateTimeSent = false;
        this.commModeSent = false;
        this.testModeSent = false;

        this.lastVolume = Uint8Array.of();
        this.lastAmount = Uint8Array.of();
        this.lastTransaction = Uint8Array.of();
        this.lastTotals = Uint8Array.of();

        this.notify();
    }

    async poke(): Promise<boolean> {
        return (await this.getVersion()).success();
    }

    public async initialize(priority: Priority): Promise<void> {
        // do criticial comms first
        const priceResponse = await this.setPrice(priority);
        if (!priceResponse.success()) throw `Init Price failed: ${priceResponse.toString()}`;

        const dateAndTimeResponse = await this.setDateAndTime(priority);
        if (!dateAndTimeResponse.success()) throw `Init Date and Time failed: ${dateAndTimeResponse.toString()}`;

        const modeResponse = await this.setMode(CommunicationMode.ON_MODE, priority);
        if (!modeResponse.success()) throw `Init Comm Mode failed: ${modeResponse.toString()}`;

        const testModeResponse = await this.toggleTestMode(this.controller.enableTestMode, priority);
        if (!testModeResponse.success()) throw `Init Test Mode failed: ${testModeResponse.toString()}`;

        const versionResponse = await this.getVersion(priority);
        if (!versionResponse.success()) throw `Get Version failed: ${versionResponse.toString()}`;

        await this.notify();
    }

    private async preAuthorize(): Promise<void> {
        const unlockResponse = await this.unlockNozzle();
        if (!unlockResponse.success()) throw `Unlock Nozzle failed: ${unlockResponse.toString()}`;

        const afterFuelingModeResponse = await this.setAfterFuelingMode(this.afterFuelingMode, Priority.HIGH);
        if (!afterFuelingModeResponse.success()) throw `Init After Fueling Mode failed: ${afterFuelingModeResponse.toString()}`;
    }

    async authorize(): Promise<void> {
        await this.preAuthorize();
        const authorizeResponse = await this.controller.sendMessage(new EaglestarRequest(this.nozzleNumber, Commands.AUTHORIZE));
        if (!authorizeResponse.success()) throw `Authorize failed: ${authorizeResponse.toString()}`;
    }

    async authorizeTotalCost(cost: number): Promise<void> {
        await this.preAuthorize();
        const presetResponse = await this.preset(PresetType.CSH_LIMIT_NORM, cost);
        if (!presetResponse.success()) throw `Authorize Cost Amount failed: ${presetResponse.toString()}`;
    }

    async authorizeTotalVolume(volume: number): Promise<void> {
        await this.preAuthorize();
        const presetResponse = await this.preset(PresetType.VOL_LIMIT_NORM, volume);
        if (!presetResponse.success()) throw `Authorize Liter Amount failed: ${presetResponse.toString()}`;
    }

    async cancelAuthorization(): Promise<void> {
        const stopFuelingResponse = await this.stopFueling();
        if (!stopFuelingResponse.success()) throw `Stop Fuleing failed: ${stopFuelingResponse.toString()}`;

        const cancelPresetResponse = await this.cancelPreset(this.selectedNozzle.linkNo);
        if (!cancelPresetResponse.success()) throw `Cancel Preset failed: ${cancelPresetResponse.toString()}`;
    }

    async lockPump(unlockCode?: string): Promise<void> {
        await this.stopFueling();
        await this.cancelPreset(this.selectedNozzle.linkNo);

        this.unlockPassword = unlockCode || '000';
        this.lockAfterFueling = true;
        const afterFuelingModeResponse = await this.setAfterFuelingMode(this.afterFuelingMode, Priority.HIGH);
        if (!afterFuelingModeResponse.success()) throw `Lock Pump failed: ${afterFuelingModeResponse.toString()}`;
    }

    async releasePump(): Promise<void> {
        this.lockAfterFueling = this.controller.profile.lockAfterFueling;
        return this.authorize();
    }

    private warn(message: string): void {
        logWarn(`Nozzle #0x${this.nozzleNumber.toString(16).toUpperCase()}: ${message}`);
    }

    private info(message: string): void {
        logInfo(`Nozzle #0x${this.nozzleNumber.toString(16).toUpperCase()}: ${message}`);
    }

    private async preset(presetType: PresetType, amount: number): Promise<EaglestarResponse> {
        const unitPrice = this.price;

        // re-initialize
        await this.initialize(Priority.HIGH);
        await this.setAfterFuelingMode(this.afterFuelingMode, Priority.HIGH);
        this.selectedNozzle.unitPrice = unitPrice.toFixed(2);

        const byVolume = presetType === PresetType.VOL_LIMIT_NORM || presetType === PresetType.VOL_LIMIT_TEST;
        const byCost = presetType === PresetType.CSH_LIMIT_NORM || presetType === PresetType.CSH_LIMIT_TEST;

        // Liter(2+1 BCD),
        const volume = Math.round((byVolume ? amount : amount / unitPrice) * 100);
        const liters = volume.toFixed(0).padStart(6, '0');
        if (liters.length !== 6) throw `Liter amount is out of range ${volume}.`;

        // Money(3+1 BCD),
        const cost = Math.round((byCost ? amount : amount * unitPrice) * 100);
        const money = cost.toFixed(0).padStart(8, '0');
        if (money.length !== 8) throw `Cost is out of range (${cost}).`;

        // Kg(2+1 BCD),
        const kg = '000000';

        // Price(1+2 BCD),
        const price = Math.round(unitPrice * 100).toFixed(0).padStart(6, '0');
        if (price.length !== 6) throw `Price is out of range (${unitPrice}).`;

        // Price reciprocal (1+3 BCD),FLAG(1),
        const priceReciprocal = Math.round(1000000 / unitPrice).toFixed(0).padStart(8, '0');

        if (++this.selectedNozzle.linkNo > 9999) this.selectedNozzle.linkNo = 1;
        const buffer = Uint8Array.of(
            ...bcd(liters),
            ...bcd(money),
            ...bcd(kg),
            ...bcd(price),
            ...bcd(priceReciprocal),
            presetType,
            ...bcd(this.selectedNozzle.linkNo.toString().padStart(4, '0')),
        );

        if (buffer.length !== 20) {
            if (--this.selectedNozzle.linkNo < 1) this.selectedNozzle.linkNo = 9999;
            throw `Invalid fuel request: ${toHexString(buffer)}`;
        }

        const presetRequest = new EaglestarRequest(this.nozzleNumber, Commands.PRESET);
        presetRequest.buffer = buffer;

        const response = await this.controller.sendMessage(presetRequest);
        if (!response.success()) throw `Preset failed: ${response.toString()}`;
        if (response.buffer[0] === 0xCC) throw `Preset failed (Busy): ${response.toString()}`;
        if (response.buffer[0] === 0xDD) throw `Preset failed (Invalid Price): ${response.toString()}`;
        if (response.buffer[0] === 0xEE) throw `Preset failed (Value too low): ${response.toString()}`;
        if (response.buffer[0] !== 0xBB) {
            response.invalid = true;
            response.addFlag("UNKNOWN_STATUS");
        }

        return response;
    }

    public async setPrice(priority: Priority): Promise<EaglestarResponse> {
        this.pricesSent = false;

        // Price(1+2 BCD),
        const unitPrice = this.price;
        const price = Math.round(unitPrice * 100).toFixed(0).padStart(6, '0');
        if (price.length !== 6) throw `Price is out of range (${unitPrice}).`;

        // Price reciprocal (1+3 BCD)
        const priceReciprocal = Math.round(1000000 / unitPrice).toFixed(0).padStart(8, '0');

        const buffer = Uint8Array.of(
            ...bcd(price),
            ...bcd(priceReciprocal),
        );

        if (buffer.length !== 7) throw `Invalid Set Price request: ${toHexString(buffer)}`;

        const setPriceRequest = new EaglestarRequest(this.nozzleNumber, Commands.SET_PRICE, priority);
        setPriceRequest.buffer = buffer;

        const response = await this.controller.sendMessage(setPriceRequest);
        if (!response.success()) throw `Set Price failed: ${response.toString()}`;
        if (response.buffer[0] !== 0x01) {
            response.invalid = true;
            response.addFlag("PRICE_CHANGE_FAILED");
        }

        if (response.buffer[0] === 0x02) throw `Set Price failed: ${response.toString()}`;
        this.pricesSent = response.success();
        if (this.pricesSent) this.selectedNozzle.unitPrice = unitPrice.toFixed(2);
        return response;
    }

    private async stopFueling(): Promise<EaglestarResponse> {
        const stopFuelingRequest = new EaglestarRequest(this.nozzleNumber, Commands.STOP_FUELING);
        const response = await this.controller.sendMessage(stopFuelingRequest);
        if (!response.success()) throw `Stop Fueling failed: ${response.toString()}`;
        return response;
    }

    private async setMode(mode: CommunicationMode, priority: Priority): Promise<EaglestarResponse> {
        this.commModeSent = false;
        const setModeRequest = new EaglestarRequest(this.nozzleNumber, Commands.SET_MODE, priority);
        setModeRequest.buffer = Uint8Array.of(mode);

        const response = await this.controller.sendMessage(setModeRequest);
        if (!response.success()) throw `Set Communication Mode failed: ${response.toString()}`;
        else this.commModeSent = true;
        return response;
    }

    private async getTotals(priority: Priority = Priority.LOW): Promise<EaglestarResponse> {
        const getTotalsRequest = new EaglestarRequest(this.nozzleNumber, Commands.GET_TOTALS, priority);
        const response = await this.controller.sendMessage(getTotalsRequest);
        if (!response.success()) throw `Get Totals failed: ${response.toString()}`;

        if (response.success()) {
            const buffer = [...response.buffer];
            const money = ((buffer[0] << 24) & 0xff000000) + ((buffer[1] << 16) & 0x00ff0000) + ((buffer[2] << 8) & 0x0000ff00) + ((buffer[3]) & 0x000000ff);
            const litrs = ((buffer[4] << 24) & 0xff000000) + ((buffer[5] << 16) & 0x00ff0000) + ((buffer[6] << 8) & 0x0000ff00) + ((buffer[7]) & 0x000000ff);
            const kilos = ((buffer[8] << 24) & 0xff000000) + ((buffer[9] << 16) & 0x00ff0000) + ((buffer[10] << 8) & 0x0000ff00) + ((buffer[11]) & 0x000000ff);

            this.selectedNozzle.totalAmount = (parseFloat(money.toString()) / 100).toFixed(2);
            this.selectedNozzle.totalVolume = (parseFloat(litrs.toString()) / 100).toFixed(2);
            this.selectedNozzle.totalKilos = (parseFloat(kilos.toString()) / 100).toFixed(2);
            this.lastTotals = response.buffer;
        }

        return response;
    }

    private async cancelPreset(linkNo: number): Promise<EaglestarResponse> {
        const request = new EaglestarRequest(this.nozzleNumber, Commands.CANCEL_PRESET, Priority.HIGH);
        request.buffer = Uint8Array.of(...bcd(linkNo.toString().padStart(4, '0')));
        const response = await this.controller.sendMessage(request);
        if (!response.success()) throw `Cancel Preset failed: ${response.toString()}`;

        if (response.buffer[0] === 0xCC) {
            response.invalid = true;
            response.addFlag("NOT_ALLOWED");
        } else if (response.buffer[0] !== 0xBB) {
            response.invalid = true;
            response.addFlag("UNKNOWN_STATUS");
        }

        return response;
    }

    private async setDateAndTime(priority: Priority): Promise<EaglestarResponse> {
        this.dateTimeSent = false;
        const now = new Date();
        const dateAndTimeString = `${(now.getFullYear() % 100).toString().padStart(2, '0')}${(now.getMonth() + 1).toString().padStart(2, '0')}${(now.getDate()).toString().padStart(2, '0')}`
            + `${now.getHours().toString().padStart(2, '0')}${(now.getMinutes()).toString().padStart(2, '0')}${(now.getSeconds()).toString().padStart(2, '0')}`;

        const dateAndTimeBcd = bcd(dateAndTimeString);
        if (dateAndTimeBcd.length !== 6) throw `Invalid date/time string: ${dateAndTimeString}`;
        const request = new EaglestarRequest(this.nozzleNumber, Commands.SET_DATETIME, priority);
        request.buffer = Uint8Array.of(...dateAndTimeBcd);

        const response = await this.controller.sendMessage(request);
        if (!response.success()) throw `Set Date/Time failed: ${response.toString()}`;
        else this.dateTimeSent = true;

        return response;
    }

    private async toggleTestMode(testMode: boolean, priority: Priority): Promise<EaglestarResponse> {
        this.testModeSent = false;
        const request = new EaglestarRequest(this.nozzleNumber, testMode ? Commands.TEST_MODE_ON : Commands.TEST_MODE_OFF, priority);

        const response = await this.controller.sendMessage(request);
        if (!response.success()) throw `${testMode ? 'Enable' : 'Disable'} Test Mode failed: ${response.toString()}`;
        else this.testModeSent = true;

        return response;
    }

    private async getVersion(priority: Priority = Priority.LOW): Promise<EaglestarResponse> {
        const request = new EaglestarRequest(this.nozzleNumber, Commands.GET_VERSION, priority);
        const response = await this.controller.sendMessage(request);
        if (!response.success()) throw `Get Version failed: ${response.toString()}`;

        this.commMode = response.buffer[0];
        this.version = response.buffer[1].toString(16).padStart(2, '0') + '.' + response.buffer[2].toString(16).padStart(2, '0');

        switch (this.commMode) {
            case 0xAA: response.addFlag('RS485_MODE'); break;
            case 0x55: response.addFlag('CURRENT_LOOP_MODE'); break;
            default: response.addFlag('UNKNOWN_MODE'); break;
        }

        this.info(`Version: ${this.version}, comm. mode: ${this.commMode.toString(16).padStart(2, '0')}: ${response.toString()}`);
        return response;
    }

    private async unlockNozzle(): Promise<EaglestarResponse> {
        const request = new EaglestarRequest(this.nozzleNumber, Commands.UNLOCK, Priority.HIGH);
        const response = await this.controller.sendMessage(request);
        if (!response.success()) throw `Unlock Nozzle failed: ${response.toString()}`;
        return response;
    }

    private async setAfterFuelingMode(mode: AfterFuelingMode, priority: Priority): Promise<EaglestarResponse> {
        if (this.unlockPassword.length > 6) throw `Unlock password must be 6 or digits or less.`;
        const request = new EaglestarRequest(this.nozzleNumber, Commands.SET_FUELING_MODE, priority);
        request.buffer = Uint8Array.of(mode, ...bcd(`${this.unlockPassword.padStart(6, '0')}`));
        if (request.buffer.length !== 4) throw `Invalid fueling mode request: ${toHexString(request.buffer)}`

        const response = await this.controller.sendMessage(request);
        if (!response.success()) throw `Set After Fueling Mode failed: ${response.toString()}`;
        return response;
    }

    public async getData(priority: Priority = Priority.LOW): Promise<EaglestarResponse> {
        const currentVolume = this.lastVolume;
        const currentAmount = this.lastAmount;
        const currentTotals = this.lastTotals;

        // Invoke GET_DATA consecutively
        const [getVolumeData, getCostData, getTotals] = await Promise.all([this.getDataImpl(priority), this.getDataImpl(priority), this.getTotals(priority)]);
        if (!getVolumeData.success() || !getCostData.success() || !getTotals.success()) {
            // reset last readings to ensure they get transmitted on the next successful query
            this.lastVolume = Uint8Array.of();
            this.lastAmount = Uint8Array.of();
            this.lastTotals = Uint8Array.of();
            throw `Get data error (${getVolumeData.toString()}), (${getCostData.toString()})`;
        }

        if (!this.selectedNozzle.transactionAvailable) {
            const volumeChanged = this.lastVolume.join() !== currentVolume.join()
            const amountChanged = this.lastAmount.join() !== currentAmount.join();
            const totalsChanged = this.lastTotals.join() !== currentTotals.join();

            if (volumeChanged && !amountChanged) {
                // update amount manually
                this.selectedNozzle.amount = (parseFloat(this.selectedNozzle.unitPrice) * parseFloat(this.selectedNozzle.volume)).toFixed(2);
            } else if (!volumeChanged && amountChanged) {
                // update volume manually
                this.selectedNozzle.volume = (parseFloat(this.selectedNozzle.amount) / parseFloat(this.selectedNozzle.unitPrice)).toFixed(2);
            }

            if (volumeChanged || amountChanged || totalsChanged) this.notify();
        } else {
            // use GET_LAST_TRANSACTION to update volume / amount
            const currentTransaction = this.lastTransaction;
            if (!(await this.getLastTransaction(priority)).success()) throw `Get last transaction failed`;

            const transactionChanged = this.lastTransaction.join() !== currentTransaction.join();
            if (transactionChanged) this.notify();
        }

        return getCostData;
    }

    private presetStatus(response: EaglestarResponse): PresetStatus {
        // Bit3～Bit2: preset status
        // 00: liter preset is doing
        // 01: money preset is doing
        // 11: no preset
        const status = response.buffer[4];
        switch (status & 0b00001100) {
            case 0b00000000: return PresetStatus.VOLUME;
            case 0b00000100: return PresetStatus.COST;
            case 0b00001100: return PresetStatus.NONE;
            default: return PresetStatus.NONE;
        }
    }

    private async getDataImpl(priority: Priority = Priority.LOW): Promise<EaglestarResponse> {
        const getDataRequest = new EaglestarRequest(this.nozzleNumber, Commands.GET_DATA, priority);
        const response = await this.controller.sendMessage(getDataRequest);
        if (!response.success()) throw `Get Data failed: ${response.toString()}`;

        if (response.success()) {
            const status = response.buffer[4];

            // Bit1～Bit0:
            // 00: stop status, unlock
            // 10: stop status, lock
            // 01: fueling status, preset not from pos
            // 11: fueling status, preset from pos
            const fueling = !!(status & 0b00000001);
            this.selectedNozzle.locked = !fueling && !!(status & 0b00000010)

            // Bit4:
            // 0: no preset from pos, 1: preset available from pos
            this.selectedNozzle.presetFromPOS = !!(status & 0b00010000);

            // Bit5:
            // no transaction for reading, 1: transaction available for reading
            this.selectedNozzle.transactionAvailable = !!(status & 0b00100000);

            // BIT7:
            // 1: lift nozzle 0：nozzle stand by
            const nozzleLifted = !!(status & 0b10000000);

            if (fueling) {
                this.selectedNozzle.status = NozzleStatus.FUELING;
            } else if (nozzleLifted) {
                this.selectedNozzle.status = this.selectedNozzle.transactionAvailable
                    ? NozzleStatus.FUEL_COMPLETE
                    : NozzleStatus.NOT_IN_HOLSTER;
            } else {
                this.selectedNozzle.status = this.selectedNozzle.transactionAvailable
                    ? NozzleStatus.FUEL_COMPLETE
                    : NozzleStatus.IN_HOLSTER;
            }

            // NOTE: Don't update volume / amount data if transaction data is available
            if (!this.selectedNozzle.transactionAvailable) {
                if (response.command === Commands.GET_DATA) {
                    // liter volume
                    this.lastVolume = response.buffer;
                    this.selectedNozzle.volume = (parseFloat(toHexString(response.buffer.slice(0, 4))) / 100).toFixed(2);
                } else if (response.command === Commands.GET_DATA + 1) {
                    // money amount
                    this.lastAmount = response.buffer;
                    this.selectedNozzle.amount = (parseFloat(toHexString(response.buffer.slice(0, 4))) / 100).toFixed(2);
                }
            }

            const presetStatus = this.presetStatus(response);
            logDebug(`FD#${this.nozzleNumber}: presetFromPOS: ${this.selectedNozzle.presetFromPOS}, `
                + `Status presets: ${presetStatus}, `
                + `transactionAvailable: ${this.selectedNozzle.transactionAvailable}, `
                + `nozzleLifted: ${nozzleLifted}, nozzleStatus: ${this.selectedNozzle.status}, `
                + `volume: ${this.selectedNozzle.volume}, `
                + `amount: ${this.selectedNozzle.amount}`);
        }

        return response;
    }

    private async getLastTransaction(priority: Priority = Priority.LOW): Promise<EaglestarResponse> {
        const getTransactionRequest = new EaglestarRequest(this.nozzleNumber, Commands.GET_TRANSACTION, priority);
        const response = await this.controller.sendMessage(getTransactionRequest);
        if (!response.success()) throw `Get Transaction failed: ${response.toString()}`;

        // NOTE: if pump sends 0x18 then the data received is not valid (i.e, no transaction data)
        if (response.command !== Commands.GET_TRANSACTION) throw `No Transaction Data available: ${response.toString()}`;

        const buffer = [...response.buffer];
        const money = ((buffer[0] << 16) & 0xff0000) + ((buffer[1] << 8) & 0x00ff00) + ((buffer[2]) & 0x0000ff);
        const litrs = ((buffer[3] << 16) & 0xff0000) + ((buffer[4] << 8) & 0x00ff00) + ((buffer[5]) & 0x0000ff);
        const yyyymmddhhmmss = toHexString(response.buffer.slice(6, 6 + 7));

        // Transaction reason explanation:
        // Bit1～Bit0: preset type
        // 00: liter preset
        // 01: money preset
        // Bit4～Bit2: payment mode
        // 000 testing
        const reason = buffer[13];
        logDebug(`Transaction reason: 0x${reason.toString(16).padStart(2, '0').toUpperCase()}, timestamp: ${yyyymmddhhmmss}`);

        this.selectedNozzle.unitPrice = (parseFloat(toHexString(response.buffer.slice(14, 14 + 3))) / 100).toFixed(2);
        this.selectedNozzle.memberCard = decode(response.buffer.slice(17, 17 + 10));
        this.selectedNozzle.staffId = toHexString(response.buffer.slice(27, 27 + 2));

        const nozzleNumber = buffer[29];
        if (nozzleNumber !== response.nozzleNumber) logWarn(`Transaction nozzle #(0x${nozzleNumber.toString(16).padStart(2, '0').toUpperCase()}) doesn't match response: ${response}`)

        this.selectedNozzle.linkNo = parseInt(toHexString(response.buffer.slice(30, 30 + 2)));
        const totalVolume = ((buffer[32] << 24) & 0xff000000) + ((buffer[33] << 16) & 0x00ff0000) + ((buffer[34] << 8) & 0x0000ff00) + ((buffer[35]) & 0x000000ff);

        this.selectedNozzle.amount = (money / 100).toFixed(2);
        this.selectedNozzle.volume = (litrs / 100).toFixed(2);
        this.selectedNozzle.totalVolume = (totalVolume / 100).toFixed(2);
        this.lastTransaction = response.buffer;

        return response;
    }

    public async clearLastTransaction(): Promise<EaglestarResponse> {
        const request = new EaglestarRequest(this.nozzleNumber, Commands.CLEAR_TRANSACTION);
        const response = await this.controller.sendMessage(request);
        if (!response.success()) throw `Clear Transaction failed: ${response.toString()}`;
        return response;
    }

    private notify(): void {
        try {
            if (this.onUpdate) this.onUpdate();
        } catch (err) {
            this.warn(`notify() error: ${err}`);
        }
    }
}