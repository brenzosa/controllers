import { bcc, isDebugEnabled, encode, logDebug, logInfo, toHexString, decode, bcd, logError, logWarn } from "../buffer-utils";
import { RS485ProtocolBase, ProtocolState } from "../protocol-base";
import { TIMEOUT, toProtocolString } from "./eaglestar-rs485-protocol";
import { EaglestarRequest } from "./eaglestar-request";
import { EaglestarResponse } from "./eaglestar-response";
import { NozzleStatus } from "../controllers";
import { AfterFuelingMode, Commands, PresetType } from "./eaglestar-codes";

const defaultVolume = 40.0;

enum EaglestarState {
    SYN1, SYN2, SYN3, COMMAND, NOZZLE_NUMBER, DATA, BCC, FINAL
}

async function wait(millis: number): Promise<void> {
    return new Promise(resolve => setTimeout(() => resolve(), millis));
}

function unpackBCD(byte: number): string {
    return byte.toString(16).padStart(2, '0');
}

class UnsolicitedEaglestarRequest extends EaglestarRequest {
    unsolicited = true;
}

export class EaglestarPumpSimulator extends RS485ProtocolBase<EaglestarRequest, EaglestarResponse> {
    fuelDispensers: FuelDispenserSimulator[];

    expectedDataLen = 0;
    nozzleNumber = 0;
    buffer = Uint8Array.of();

    constructor(private testMode: boolean = false, nozzleNumbers: number[] = [], loop = false) {
        super(EaglestarState.SYN1, EaglestarState.FINAL, TIMEOUT);

        this.stateTransitions.set(EaglestarState.SYN1, b => this.checkSyn(b, EaglestarState.SYN2));
        this.stateTransitions.set(EaglestarState.SYN2, b => this.checkSyn(b, EaglestarState.SYN3));
        this.stateTransitions.set(EaglestarState.SYN3, b => this.checkSyn(b, EaglestarState.COMMAND));
        this.stateTransitions.set(EaglestarState.COMMAND, b => this.readCommand(b));
        this.stateTransitions.set(EaglestarState.NOZZLE_NUMBER, b => this.readNozzleNumber(b));
        this.stateTransitions.set(EaglestarState.DATA, b => this.readData(b));
        this.stateTransitions.set(EaglestarState.BCC, b => this.checkBCC(b));

        this.fuelDispensers = nozzleNumbers.map(nozzleNumber => new FuelDispenserSimulator(nozzleNumber, this));
        if (loop) this.loopFuelSequence().catch(err => logError(`Simulation loop error: ${err}`)).finally(() => logInfo(`Simulation loop ended.`));
        else logInfo(`Use '--loop' to run fuel authorization simulation loop.`);
    }

    private async loopFuelSequence(): Promise<void> {
        let linkNo = 1;
        logInfo(`Running fuel authorization loop...`);
        for (; ;) {
            for (const fd of this.fuelDispensers) {
                try {
                    logInfo(`Waiting before next volume authorization round...`);
                    await wait(15000);
                    logInfo(`Volume authorization for 0x${fd.nozzleNumber.toString(16)}...`);

                    const unlockCode = '123';
                    const unlock = new UnsolicitedEaglestarRequest(this.nozzleNumber, Commands.UNLOCK)
                    unlock.buffer = Uint8Array.of(AfterFuelingMode.LOCKED_CAN_UNLOCK, ...encode(unlockCode));
                    fd.process(unlock);

                    if (++linkNo > 9999) linkNo = 1;

                    // cancel previous presets
                    const request = new UnsolicitedEaglestarRequest(this.nozzleNumber, Commands.CANCEL_PRESET);
                    request.buffer = Uint8Array.of(...bcd(linkNo.toString().padStart(4, '0')));
                    fd.process(request);

                    const volumeAuthorization = new UnsolicitedEaglestarRequest(this.nozzleNumber, Commands.PRESET);
                    volumeAuthorization.buffer = Uint8Array.of(
                        ...bcd('004000'), // liters (2+1 BCD)
                        ...bcd('00000000'), // money (3+1 BCD)
                        ...bcd('000000'), // kg (2+1 BCD)
                        ...bcd('006000'), // price (1+2 BCD)
                        ...bcd('00000000'), // price reciprocal (1+3 BCD)
                        PresetType.VOL_LIMIT_NORM,
                        ...bcd(linkNo.toString().padStart(4, '0')),
                    );
                    fd.process(volumeAuthorization);
                    fd.process(new UnsolicitedEaglestarRequest(this.nozzleNumber, Commands.AUTHORIZE));
                } catch (err) {
                    logWarn(`Fuel Volume Authorization error: ${err}`);
                }

                try {
                    logInfo(`Waiting before next cost authorization round...`);
                    await wait(15000);
                    logInfo(`Cost authorization for 0x${fd.nozzleNumber.toString(16)}...`);

                    const unlockCode = '123';
                    const unlock = new UnsolicitedEaglestarRequest(this.nozzleNumber, Commands.UNLOCK)
                    unlock.buffer = Uint8Array.of(AfterFuelingMode.LOCKED_CAN_UNLOCK, ...encode(unlockCode));
                    fd.process(unlock);

                    if (++linkNo > 9999) linkNo = 1;

                    // cancel previous presets
                    const request = new UnsolicitedEaglestarRequest(this.nozzleNumber, Commands.CANCEL_PRESET);
                    request.buffer = Uint8Array.of(...bcd(linkNo.toString().padStart(4, '0')));
                    fd.process(request);

                    const volumeAuthorization = new UnsolicitedEaglestarRequest(this.nozzleNumber, Commands.PRESET);
                    volumeAuthorization.buffer = Uint8Array.of(
                        ...bcd('000000'), // liters (2+1 BCD)
                        ...bcd('00100000'), // money (3+1 BCD)
                        ...bcd('000000'), // kg (2+1 BCD)
                        ...bcd('006000'), // price (1+2 BCD)
                        ...bcd('00000000'), // price reciprocal (1+3 BCD)
                        PresetType.CSH_LIMIT_NORM,
                        ...bcd(linkNo.toString().padStart(4, '0')),
                    );
                    fd.process(volumeAuthorization);
                    fd.process(new UnsolicitedEaglestarRequest(this.nozzleNumber, Commands.AUTHORIZE));
                } catch (err) {
                    logWarn(`Fuel Cost Authorization error: ${err}`);
                }
            }
        }
    }

    public resetSimulation(nozzleNumber: number): void {
        this.nozzleNumber = nozzleNumber;
        this.buffer = encode('Simulator default response.');
    }

    public async sendMessage(): Promise<EaglestarResponse> {
        throw `Illegal request: Pump cannot initiate messages`;
    }

    protected publishResponse(): void {
        // nothing to actually publish!
        this.request = undefined;
    }

    protected newEmptyResponse(): EaglestarResponse {
        throw `Illegal request: Pump cannot send empty responses`;
    }

    protected newTimedoutResponse(): EaglestarResponse {
        throw `Illegal request: Pump don't initiate requests, hence, can't time-out.`;
    }

    protected sendRequest(): ProtocolState {
        throw `Illegal request: Pump don't send requests.`;
    }

    protected responseMatchesRequest(): boolean {
        throw `Illegal request: Pump don't make requests.`;
    }

    protected onRead(buffer: Uint8Array): void {
        if (isDebugEnabled()) logDebug(`${this.constructor.name}  IN: ${toProtocolString(buffer)}`);
        return super.onRead(buffer);
    }

    public send(buffer: Uint8Array): void {
        if (isDebugEnabled()) logDebug(`${this.constructor.name} OUT: ${toProtocolString(buffer)}`);
        return super.send(buffer);
    }

    public sendResponse(request: EaglestarRequest, command: Commands, nozzleNumber: number, buffer: Uint8Array): void {
        if ((request as UnsolicitedEaglestarRequest).unsolicited) {
            // don't reply to unsolicited requests
            return;
        }

        const bccValue = command ^ nozzleNumber ^ bcc(buffer);
        this.send(Uint8Array.of(Commands.SYN, Commands.SYN, Commands.SYN, command, nozzleNumber, ...buffer, bccValue));
    }

    private checkSyn(value: number, nextState: EaglestarState): ProtocolState {
        if (value !== Commands.SYN) {
            throw `Expected SYN, but got: 0x${value.toString(16).padStart(2, '0').toUpperCase()}`;
        }

        return nextState;
    }

    private readCommand(value: number): ProtocolState {
        this.request = new EaglestarRequest(-1);

        this.expectedDataLen = ((command: number) => {
            switch (command) {
                case Commands.PRESET: return 20;
                case Commands.SET_PRICE: return 7;
                case Commands.STOP_FUELING: return 0;
                case Commands.AUTHORIZE: return 0;
                case Commands.SET_MODE: return 1;
                case Commands.GET_TOTALS: return 0;
                case Commands.CANCEL_PRESET: return 2;
                case Commands.SET_DATETIME: return 6;
                case Commands.TEST_MODE_ON: return 0;
                case Commands.TEST_MODE_OFF: return 0;
                case Commands.GET_VERSION: return 0;
                case Commands.UNLOCK: return 0;
                case Commands.SET_FUELING_MODE: return 4;
                case Commands.GET_DATA: return 0;
                case Commands.GET_TRANSACTION: return 0;
                case Commands.CLEAR_TRANSACTION: return 0;
                default: throw `Unrecognized command: 0x${command.toString(16).padStart(2, '0').toUpperCase()}`;
            }
        })(value);

        this.request = new EaglestarRequest(-1);
        this.request.command = value;
        return EaglestarState.NOZZLE_NUMBER;
    }

    private readNozzleNumber(value: number): ProtocolState {
        if (!this.request) {
            throw `No request has been prepared for reading nozzle number`;
        } else {
            this.request.nozzleNumber = value;
            return EaglestarState.DATA;
        }
    }

    private readData(b: number): ProtocolState {
        if (!this.request) {
            throw `No request has been prepared for reading data`;
        } else if (this.expectedDataLen === 0) {
            return this.checkBCC(b);
        } else {
            this.request.buffer = Uint8Array.of(...this.request.buffer, b);
            return (this.request.buffer.length < this.expectedDataLen)
                ? EaglestarState.DATA
                : EaglestarState.BCC;
        }
    }

    private checkBCC(bccValue: number): ProtocolState {
        if (!this.request) {
            throw `Invalid state: No request data object ready`;
        }

        const bccExpected = this.request.command ^ this.request.nozzleNumber ^ bcc(this.request.buffer);
        if (bccExpected !== bccValue) {
            throw `BCC mismatch (0x${bccValue.toString(16).toUpperCase()} <> 0x${bccExpected.toString(16).toUpperCase()})`;
        }

        this.buffer = this.request.buffer;
        const fuelDispenser = this.fuelDispensers.find(fd => fd.nozzleNumber === this.request?.nozzleNumber);
        if (this.testMode && this.request.nozzleNumber != this.nozzleNumber) {
            throw `Ignoring request for different Pump`
        } else if (!this.testMode && !fuelDispenser) {
            throw `Ignoring request for unknown nozzle number: ${this.request?.nozzleNumber.toString(16).padStart(2, '0').toUpperCase()}`;
        } else if (!this.testMode && fuelDispenser) {
            fuelDispenser.process(this.request);
        }

        // all done
        return EaglestarState.FINAL;
    }

    protected stateName(state: ProtocolState): string {
        switch (state) {
            case EaglestarState.SYN1:
                return 'SYN1';
            case EaglestarState.SYN2:
                return 'SYN2';
            case EaglestarState.SYN3:
                return 'SYN3';
            case EaglestarState.COMMAND:
                return 'COMMAND';
            case EaglestarState.NOZZLE_NUMBER:
                return 'NOZZLE_NUMBER';
            case EaglestarState.DATA:
                return 'DATA';
            case EaglestarState.BCC:
                return 'BCC';
            case EaglestarState.FINAL:
                return 'FINAL';
            default:
                return `${state}`;
        }
    }
}

class NozzleState {
    constructor(public status: NozzleStatus = NozzleStatus.IN_HOLSTER) { }

    unitPrice = 0;
    amount = 0;
    volume = 0;
    totalAmount = 0;
    totalVolume = 0;
    totalKg = 0;

    transactionAvailable = false;
    lockAfterFueling = false;
    locked = false;
}

class FuelAuthorization {
    fuelCost = 0;
    fuelVolume = 0;
    unitPrice = 0;
}

class FuelDispenserSimulator {
    nozzle = new NozzleState();
    cancelFueling: (() => any) | null = null;

    liters = '';
    money = '';
    kg = '';
    priceReciprocal = '';
    flag = 0;
    link = '';

    constructor(public nozzleNumber: number, private simulator: EaglestarPumpSimulator) {
    }

    process(request: EaglestarRequest): void {
        switch (request.command) {
            case Commands.PRESET:
                this.preset(request);
                break;
            case Commands.SET_PRICE:
                this.setPrice(request);
                break;
            case Commands.STOP_FUELING:
                this.stopFueling(request);
                break;
            case Commands.AUTHORIZE:
                this.authorize(request);
                break;
            case Commands.SET_MODE:
                this.setMode(request);
                break;
            case Commands.GET_TOTALS:
                this.getTotals(request);
                break;
            case Commands.CANCEL_PRESET:
                this.cancelPreset(request);
                break;
            case Commands.SET_DATETIME:
                this.setDateTime(request);
                break;
            case Commands.TEST_MODE_ON:
                this.setTestMode(request, true);
                break;
            case Commands.TEST_MODE_OFF:
                this.setTestMode(request, false);
                break;
            case Commands.GET_VERSION:
                this.getVersion(request);
                break;
            case Commands.UNLOCK:
                this.unlock(request);
                break;
            case Commands.SET_FUELING_MODE:
                this.setFuelingMode(request);
                break;
            case Commands.GET_DATA:
                this.getData(request);
                break;
            case Commands.GET_TRANSACTION:
                this.getTransaction(request);
                break;
            case Commands.CLEAR_TRANSACTION:
                this.clearTransaction(request);
                break;

            default:
                throw `Simulation not yet implemented: ${request}`;
        }
    }

    private preset(request: EaglestarRequest): void {
        // 0xBB = success, 0xCC = Busy, 0xDD = Invalid Price, 0xEE = preset value low to 0.6 liter
        if (this.nozzle.status === NozzleStatus.FUELING) {
            this.simulator.sendResponse(request, request.command, this.nozzleNumber, Buffer.of(0xCC)); // busy
        } else {
            const data = [...request.buffer];
            let index = 0;

            this.nozzle.transactionAvailable = false;
            this.liters = `${unpackBCD(data[index++])}${unpackBCD(data[index++])}.${unpackBCD(index++)}`; // Liter(2+1 BCD),
            this.money = `${unpackBCD(data[index++])}${unpackBCD(data[index++])}${unpackBCD(data[index++])}.${unpackBCD(index++)}`; // Money(3+1 BCD),
            this.kg = `${unpackBCD(data[index++])}${unpackBCD(data[index++])}.${unpackBCD(index++)}`; // Kg(2+1 BCD),
            this.nozzle.unitPrice = parseFloat(`${unpackBCD(data[index++])}${unpackBCD(data[index++])}.${unpackBCD(index++)}`) || 0; // Price(1+2 BCD),
            this.priceReciprocal = `${unpackBCD(data[index++])}${unpackBCD(data[index++])}${unpackBCD(data[index++])}.${unpackBCD(index++)}`; // Price reciprocal (1+3 BCD)
            this.flag = data[index++]; // FLAG(1),
            this.link = `${unpackBCD(data[index++])}${unpackBCD(data[index++])}`; // link no.(2 BCD,it must be added to last time ,and isn’t zero)

            const literPreset = (this.flag & 0b00000011) === 0b00;
            const moneyPreset = (this.flag & 0b00000011) === 0b01;

            const fuelAuthorization = new FuelAuthorization();
            fuelAuthorization.unitPrice = this.nozzle.unitPrice;
            if (!fuelAuthorization.unitPrice) {
                throw `Unit price not set for Nozzle #${this.nozzleNumber}`;
            }

            if (moneyPreset && !isNaN(parseFloat(this.money))) {
                fuelAuthorization.fuelCost = parseFloat(this.money);
                fuelAuthorization.fuelVolume = fuelAuthorization.fuelCost / fuelAuthorization.unitPrice;
                logInfo(`Authorizing by cost: ${JSON.stringify(fuelAuthorization)}`);
            } else if (literPreset && !isNaN(parseFloat(this.liters))) {
                fuelAuthorization.fuelVolume = parseFloat(this.liters);
                fuelAuthorization.fuelCost = fuelAuthorization.fuelVolume * fuelAuthorization.unitPrice;
                logInfo(`Authorizing by volume: ${JSON.stringify(fuelAuthorization)}`);
            } else {
                // default fuel: P1,000
                fuelAuthorization.fuelVolume = defaultVolume;
                fuelAuthorization.fuelCost = fuelAuthorization.fuelVolume * fuelAuthorization.unitPrice;
                logInfo(`Authorizing by default volume: ${JSON.stringify(fuelAuthorization)}`);
            }

            this.simulator.sendResponse(request, request.command, this.nozzleNumber, Buffer.of(0xBB)); // success

            // perform fueling sequence
            this.runFuelSequence(fuelAuthorization, this.nozzle).catch(() => logInfo(`Simulated fueling canceled.`));
        }
    }

    private cancelPreset(request: EaglestarRequest): void {
        const data = [...request.buffer];
        let index = 0;

        if (this.cancelFueling) {
            // 0xBB = success, 0xCC = not allowed
            this.simulator.sendResponse(request, request.command, this.nozzleNumber, Buffer.of(0xCC)); // not allowed / fueling in progress
        }

        this.link = `${unpackBCD(data[index++])}${unpackBCD(data[index++])}`; // link no.(2 BCD,it must be added to last time ,and isn’t zero)
        logInfo(`Canceling preset for link# ${this.link}`);
        this.liters = '';
        this.money = '';
        this.kg = '';
        this.priceReciprocal = '';
        this.flag = 0;

        // 0xBB = success, 0xCC = not allowed
        this.simulator.sendResponse(request, request.command, this.nozzleNumber, Buffer.of(0xBB)); // success
    }

    private setPrice(request: EaglestarRequest): void {
        const data = [...request.buffer];
        let index = 0;

        this.nozzle.unitPrice = parseFloat(`${unpackBCD(data[index++])}${unpackBCD(data[index++])}.${unpackBCD(index++)}`) || 0; // Price(1+2 BCD),
        this.priceReciprocal = `${unpackBCD(data[index++])}${unpackBCD(data[index++])}${unpackBCD(data[index++])}.${unpackBCD(index++)}`; // Price reciprocal (1+3 BCD)

        // 0x01 = success, 02 = failed
        this.simulator.sendResponse(request, request.command, this.nozzleNumber, Buffer.of(0x01)); // success
    }

    private stopFueling(request: EaglestarRequest): void {
        if (this.cancelFueling) this.cancelFueling();
        this.cancelFueling = null;
        this.simulator.sendResponse(request, request.command, this.nozzleNumber, Buffer.of()); // success
    }

    private authorize(request: EaglestarRequest): void {
        if (this.cancelFueling) this.cancelFueling();
        this.cancelFueling = null;

        const fuelAuthorization = new FuelAuthorization();
        fuelAuthorization.unitPrice = this.nozzle.unitPrice;
        if (!fuelAuthorization.unitPrice) {
            throw `Unit price not set for Nozzle #${this.nozzleNumber}`;
        }

        if (!isNaN(parseFloat(this.money))) {
            fuelAuthorization.fuelCost = parseFloat(parseFloat(this.money).toFixed(0));
            fuelAuthorization.fuelVolume = fuelAuthorization.fuelCost / fuelAuthorization.unitPrice;
        } else if (!isNaN(parseFloat(this.liters))) {
            fuelAuthorization.fuelVolume = parseFloat(parseFloat(this.liters).toFixed(0));
            fuelAuthorization.fuelCost = fuelAuthorization.fuelVolume * fuelAuthorization.unitPrice;
        } else {
            // default fuel: P1,000
            fuelAuthorization.fuelVolume = defaultVolume;
            fuelAuthorization.fuelCost = fuelAuthorization.fuelVolume * fuelAuthorization.unitPrice;
        }

        this.simulator.sendResponse(request, request.command, this.nozzleNumber, Buffer.of()); // success

        // perform fueling sequence
        this.runFuelSequence(fuelAuthorization, this.nozzle).catch(() => logInfo(`Simulated fueling canceled.`));
    }

    private setMode(request: EaglestarRequest): void {
        logInfo(`Received set mode: ${toHexString(request.buffer)}`);
        this.simulator.sendResponse(request, request.command, this.nozzleNumber, Buffer.of());
    }

    private getTotals(request: EaglestarRequest): void {
        const totalAmount = Math.trunc(this.nozzle.totalAmount * 100);
        const totalVolume = Math.trunc(this.nozzle.totalVolume * 100);
        const totalKg = Math.trunc(this.nozzle.totalKg * 100);
        const buffer = Buffer.of(
            (totalAmount >>> 24) & 0xff,
            (totalAmount >>> 16) & 0xff,
            (totalAmount >>> 8) & 0xff,
            (totalAmount >>> 0) & 0xff,

            (totalVolume >>> 24) & 0xff,
            (totalVolume >>> 16) & 0xff,
            (totalVolume >>> 8) & 0xff,
            (totalVolume >>> 0) & 0xff,

            (totalKg >>> 24) & 0xff,
            (totalKg >>> 16) & 0xff,
            (totalKg >>> 8) & 0xff,
            (totalKg >>> 0) & 0xff,
        );

        if (buffer.length !== 12) {
            throw `Unable to form proper GetTotals response, length: ${buffer.length} <> 12`;
        }

        this.simulator.sendResponse(request, request.command, this.nozzleNumber, buffer); // success
    }

    private setDateTime(request: EaglestarRequest): void {
        logInfo(`Received timestamp: ${toHexString(request.buffer)}`);
        this.simulator.sendResponse(request, request.command, this.nozzleNumber, Buffer.of());
    }

    private setTestMode(request: EaglestarRequest, enable: boolean): void {
        logInfo(`Received request to turn test mode ${enable ? 'ON' : 'OFF'}`);
        this.simulator.sendResponse(request, request.command, this.nozzleNumber, Buffer.of());
    }

    private getVersion(request: EaglestarRequest): void {
        // return RS485 (0xAA), v 01.00 (BCD)
        this.simulator.sendResponse(request, request.command, this.nozzleNumber, Buffer.of(0xAA, 0x01, 0x00));
    }

    private unlock(request: EaglestarRequest): void {
        logInfo(`Received request to unlock pump`);
        this.nozzle.locked = false;
        this.simulator.sendResponse(request, request.command, this.nozzleNumber, Buffer.of());
    }

    private setFuelingMode(request: EaglestarRequest): void {
        const fuelingMode = [...request.buffer];
        const unlockCode = decode(Buffer.of(...fuelingMode.slice(1, 4)));
        if (fuelingMode[0] === 0xAA || fuelingMode[0] === 0xA0) {
            this.nozzle.lockAfterFueling = true;
        } else if (fuelingMode[0] === 0x55) {
            this.nozzle.lockAfterFueling = false;
        }

        logInfo(`Received fueling mode request: 0x${fuelingMode[0].toString(16)}, unlock code = ${unlockCode}`);
        this.simulator.sendResponse(request, request.command, this.nozzleNumber, Buffer.of());
    }

    private getData(request: EaglestarRequest): void {
        let status = 0;
        if (this.money) status |= 0b00000100; // bits 3-2: 01 = money preset
        else if (this.liters) status |= 0b00000000; // bits 3-2: 00 = liter preset
        else status |= 0b00001100; // bits 3-2: 11 = no preset

        const preset = !!this.money || !!this.liters;
        if (preset) status |= 0b00010000; // Bit 4: Preset from POS
        if (this.nozzle.status !== NozzleStatus.IN_HOLSTER) status |= 0b10000000; // Bit 7: nozzle lifted

        if (this.nozzle.status === NozzleStatus.FUELING) {
            if (preset) status |= 0b00000001; // 01: fueling status, preset not from pos
            else status |= 0b00000011; // 11: fueling status, preset from pos
        } else {
            if (this.nozzle.locked) status |= 0b00000010; // 10: stop status, lock
            else status |= 0b00000000; // 00: stop status, unlock
        }

        if (this.nozzle.transactionAvailable) {
            status |= 0b00100000; // Bit 5: transaction available for reading
        }

        if (this.money) {
            const buffer = Uint8Array.of(
                ...bcd(`${(this.nozzle.amount * 100).toFixed(0).padStart(8, '0')}`),
                status
            );

            if (buffer.length !== 5) {
                throw `Unable to form proper GetData response, length: ${buffer.length} <> 5`;
            }

            this.simulator.sendResponse(request, Commands.GET_DATA + 1, this.nozzleNumber, buffer);
        } else {
            const buffer = Uint8Array.of(
                ...bcd(`${(this.nozzle.volume * 100).toFixed(0).padStart(8, '0')}`),
                status
            );

            if (buffer.length !== 5) {
                throw `Unable to form proper GetData response, length: ${buffer.length} <> 5`;
            }

            this.simulator.sendResponse(request, Commands.GET_DATA, this.nozzleNumber, buffer);
        }
    }

    private getTransaction(request: EaglestarRequest): void {
        if (!this.nozzle.transactionAvailable) {
            const buffer = new Uint8Array(36);
            this.simulator.sendResponse(request, Commands.GET_TRANSACTION + 2, this.nozzleNumber, buffer);
        } else {
            const now = new Date();
            const yyyy = `${now.getFullYear()}`.padStart(4, '0');
            const mm = `${now.getMonth() + 1}`.padStart(2, '0');
            const dd = `${now.getDate() + 1}`.padStart(2, '0');
            const hh = `${now.getHours()}`.padStart(2, '0');
            const mi = `${now.getMinutes()}`.padStart(2, '0');
            const ss = `${now.getSeconds()}`.padStart(2, '0');

            let reason = 0;
            if (this.money) reason |= 0b00000001; // bits 1-0: 01 = money preset
            else if (this.liters) reason |= 0b00000000; // bits 1-0: 00 = liter preset
            else reason |= 0b00000011; // bits 3-2: 11 = no preset

            // Bit4～Bit2: payment mode
            // 000 testing
            // Bit7～Bit5: not use

            const amount = Math.trunc(this.nozzle.amount * 100);
            const volume = Math.trunc(this.nozzle.volume * 100);
            const totalVolume = Math.trunc(this.nozzle.totalVolume * 100);
            const buffer = Uint8Array.of(
                (amount >>> 16) & 0xff,
                (amount >>> 8) & 0xff,
                (amount >>> 0) & 0xff,
                (volume >>> 16) & 0xff,
                (volume >>> 8) & 0xff,
                (volume >>> 0) & 0xff,
                ...bcd(`${yyyy}${mm}${dd}${hh}${mi}${ss}`),
                reason,
                ...bcd(`${(this.nozzle.unitPrice * 100).toFixed(0).padStart(6, '0')}`), // Price (1+2 BCD)
                ...encode('0000000000'), // 10-char member chard
                ...bcd(`0000`), // staff-id (2 BCD)
                this.nozzleNumber,
                ...bcd(`${this.link.toString().padStart(4, '0')}`), // Transaction link # (2 BCD)
                (totalVolume >>> 24) & 0xff,
                (totalVolume >>> 16) & 0xff,
                (totalVolume >>> 8) & 0xff,
                (totalVolume >>> 0) & 0xff,
            );

            if (buffer.length !== 36) {
                throw `Unable to form proper Transaction response, length: ${buffer.length} <> 36`;
            }

            this.simulator.sendResponse(request, Commands.GET_TRANSACTION, this.nozzleNumber, buffer);
        }
    }

    private clearTransaction(request: EaglestarRequest): void {
        logInfo(`Received request to clear transaction`);
        this.nozzle.transactionAvailable = false;
        this.simulator.sendResponse(request, request.command, this.nozzleNumber, Buffer.of());
    }

    private async runFuelSequence(fuelAuthorization: FuelAuthorization, nozzle: NozzleState) {
        logInfo(`Simulated fueling sequence initiated: ${JSON.stringify(fuelAuthorization)}`);

        // wait 1 second to pull out of holster
        await this.wait(1000);
        nozzle.status = NozzleStatus.FUELING;
        logInfo(`Simulated fueling commencing...`);

        // wait 1 seconds to start fueling
        await this.wait(1000);

        const startingTotalVolume = nozzle.totalVolume;
        const startingTotalAmount = nozzle.totalAmount;
        let progress = 0;

        for (; ;) {
            // increment by 25% every second
            progress = Math.min(1, progress + 25 / 100 / 100);

            // update fuel volume / amount
            nozzle.volume = parseFloat((fuelAuthorization.fuelVolume * progress).toFixed(2));
            nozzle.amount = parseFloat((fuelAuthorization.fuelCost * progress).toFixed(2));

            // update totals
            nozzle.totalVolume = parseFloat((startingTotalVolume + nozzle.volume).toFixed(2));
            nozzle.totalAmount = parseFloat((startingTotalAmount + nozzle.amount).toFixed(2));

            if (progress === 1) {
                // update fuel volume / amount
                nozzle.volume = parseFloat(fuelAuthorization.fuelVolume.toFixed(2));
                nozzle.amount = parseFloat(fuelAuthorization.fuelCost.toFixed(2));

                // update totals
                nozzle.totalVolume = parseFloat((startingTotalVolume + nozzle.volume).toFixed(2));
                nozzle.totalAmount = parseFloat((startingTotalAmount + nozzle.amount).toFixed(2));

                nozzle.status = NozzleStatus.FUEL_COMPLETE;
                nozzle.transactionAvailable = true;
                break;
            }

            // wait 10 millis before incrementing again
            await this.wait(10);
        }

        // put back in holster after 3 seconds...
        logInfo(`Simulated Fueling Complete.`);
        await this.wait(3000);

        // done!
        logInfo(`Simulated Nozzle Restored.`);
        nozzle.status = NozzleStatus.IN_HOLSTER;
        nozzle.locked = nozzle.lockAfterFueling;
    }

    private async wait(millis: number): Promise<void> {
        return new Promise((resolve, reject) => {
            this.cancelFueling = reject;
            setTimeout(() => {
                this.cancelFueling = null;
                resolve();
            }, millis);
        });
    }
}