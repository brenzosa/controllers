import { toHexString } from "../buffer-utils";
import { Priority, RS485Request } from "../protocol-base";
import { EaglestarResponse } from "./eaglestar-response";
import { commandName } from "./eaglestar-rs485-protocol";

export class EaglestarRequest extends RS485Request<EaglestarResponse> {
    constructor(public nozzleNumber: number, public command: number = 0, priority: Priority = Priority.LOW) {
        super(new Uint8Array(), priority);
    }

    toString(): string {
        return `EaglestarRequest: Nozzle#0x${this.nozzleNumber.toString(16).padStart(2, '0').toUpperCase()}`
            + `, command: ${commandName(this.command)}`
            + `, data: 0x${toHexString(this.buffer)}`;
    }
}