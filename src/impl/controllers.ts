import { OpenOptions } from "serialport";
import { setDebugEnabled } from "./buffer-utils";
import { EaglestarController, EaglestarProfile } from "./eaglestar/eaglestar-controller";
import { TatsunoController, TatsunoProfile } from "./tatsuno/tatsuno-controller";

export enum ControllerType {
    TATSUNO_V_2_0 = 'TATSUNO_V_2_0',
    EAGLESTAR_2018 = 'EAGLESTAR_2018',
}

export enum NozzleStatus {
    IN_HOLSTER = '0',
    NOT_IN_HOLSTER = '1',
    FUELING = '3',
    FUEL_COMPLETE = '4',
}

export type ProductType = string;

export class Controllers {
    static createController(controllerType: ControllerType, profile: ControllerProfile): IController {
        if (profile.debug) setDebugEnabled(true);
        else setDebugEnabled(false);

        switch (controllerType) {
            case ControllerType.TATSUNO_V_2_0: return new TatsunoController(profile as TatsunoProfile);
            case ControllerType.EAGLESTAR_2018: return new EaglestarController(profile as EaglestarProfile);
            default: break;
        }

        throw `Unknown controller type: ${controllerType}`;
    }
}

export interface ControllerProfile {
    host: string,
    port: number,
    serialPath?: string,
    serialOptions?: OpenOptions,
    debug?: boolean;

    logMessages: boolean,
    logStateTransitions: boolean,
    enableCpuStats: boolean,
    timeout: number,

    pollInterval: number;
    offlineTryReconnectInterval: number;
    inHolsterPollInterval: number;
    maxConsecutiveFailures: number;
}

export interface IController {
    profile: ControllerProfile,
    connected: boolean;
    scanning: boolean;
    fuelDispensers: IFuelDispenser[];
    unitPrices: Map<ProductType, number>;

    connect(testMode: boolean): Promise<void>;
    updatePrices(prices: Map<string, number>): Promise<void>;
    disconnect(): Promise<void>;
    scan(): Promise<void>;
    onUpdate?: () => void;
}

export interface IFuelDispenser {
    id: string;
    online: boolean;
    unlockPassword: string;
    nozzles: INozzle[];
    selectedNozzle?: INozzle;
    failures: number;

    reset(): void;
    poke(): Promise<boolean>;
    authorize(): Promise<void>;
    authorizeTotalCost(cost: number): Promise<void>;
    authorizeTotalVolume(volume: number): Promise<void>;
    cancelAuthorization(): Promise<void>;
    lockPump(unlockCode?: string): Promise<void>;
    releasePump(): Promise<void>;
    onUpdate?: () => void;
}

export interface INozzle {
    fuelingSequence: string;
    status: NozzleStatus;
    unitPrice: string;
    amount: string;
    volume: string;
    totalAmount: string;
    totalVolume: string;
    productType: ProductType;
    nozzleNumber: number;
    locked: boolean;
}