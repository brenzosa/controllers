import { TransactionsDAO } from "../dao/TransactionsDAO";
import { environment } from "../server/environment";
import { IController, INozzle } from "../";
import { logError, logWarn, logDebug } from "./buffer-utils";
import { ControllerProfile, IFuelDispenser, NozzleStatus, ProductType } from "./controllers";
import { Priority, RS485ProtocolBase, RS485Request, RS485Response } from "./protocol-base";

export abstract class ControllerBase<
    PROFILE extends ControllerProfile,
    RS485 extends RS485ProtocolBase<REQUEST, RESPONSE>,
    FD extends IFuelDispenser,
    REQUEST extends RS485Request<RESPONSE>,
    RESPONSE extends RS485Response
    > implements IController {

    set connected(connected: boolean) { this._connected = connected; this.notify(); }
    get connected(): boolean { return this._connected && this.rs485Protocol.connected; }
    _connected = false;

    public fuelDispensers: FD[] = []
    public scanning = false;
    public unitPrices = new Map<ProductType, number>();
    public onUpdate?: () => void;

    private nextPoll?: any;
    private nextOfflineReCheck?: any;
    private lastPollTime = new Map<string, number>();
    private lastFuelDispenserTransactions = new Map<string, string>();
    private lastFuelDispenserTotals = new Map<string, string>();

    constructor(
        public profile: PROFILE,
        public rs485Protocol: RS485,
    ) {
        this.rs485Protocol.logStateTransitions = () => !!profile?.logStateTransitions;
    }

    public abstract scan(): Promise<void>;
    public abstract updatePrices(prices: Map<string, number>): Promise<void>;
    protected abstract tryConnect(fd: FD, priority: Priority): Promise<void>;
    protected abstract pollFuelDispenser(fd: FD): Promise<void>;

    public async transactionSaved(fuelDispenser: IFuelDispenser, fuelingSequence: string): Promise<void> {
        logDebug(`Fueling sequence (FD#${fuelDispenser.id}) ${fuelingSequence} transaction saved.`);
    }

    async connect(testMode = false): Promise<void> {
        if ([...this.unitPrices].length <= 0) {
            throw `Set unit prices before connecting.`;
        }

        if (!!this.profile.serialPath && !!this.profile.serialOptions) {
            await this.rs485Protocol.connectSerial(this.profile.serialPath, this.profile.serialOptions);
        } else {
            await this.rs485Protocol.connect(this.profile.host, this.profile.port);
        }

        this.connected = true;

        if (!testMode) {
            // Initiate continuous polling of all fuel dispensers...
            this.nextPoll = setTimeout(() => this.pollFuelDispensers(), this.profile.pollInterval);
            this.nextOfflineReCheck = setTimeout(() => this.pollReCheckOfflineFuelDispensers(), 0);
        }
    }

    async disconnect(): Promise<void> {
        try {
            logWarn(`Disconnecting...`);
            this.connected = false;
            clearTimeout(this.nextPoll);
            clearTimeout(this.nextOfflineReCheck);
            this.rs485Protocol.disconnect();
        } finally {
            this.notify();
        }
    }

    protected notify(): void {
        try {
            if (this.onUpdate) this.onUpdate();
        } catch (err) {
            logWarn(`notify() error: ${err}`);
        }
    }

    protected inHolster(fd: FD): boolean {
        return !fd.selectedNozzle || fd.selectedNozzle.status === NozzleStatus.IN_HOLSTER;
    }

    protected lastPollElapsedTime(fd: FD): number {
        return new Date().getTime() - (this.lastPollTime.get(fd.id) || 0);
    }

    protected shouldPoll(fd: FD): boolean {
        return fd.online && (!this.inHolster(fd) || this.lastPollElapsedTime(fd) > this.profile.inHolsterPollInterval);
    }

    protected shouldTryConnect(fd: FD): boolean {
        return !fd.online && this.lastPollElapsedTime(fd) > this.profile.offlineTryReconnectInterval;
    }

    protected async pollFuelDispensers(): Promise<void> {
        if (!this.connected) {
            this.fuelDispensers.forEach(fd => fd.reset());
            this.disconnect();
            return;
        }

        const onlineFuelDispensers = this.fuelDispensers.filter(fd => this.shouldPoll(fd));
        try {
            for (const fd of onlineFuelDispensers) {
                if (!this.connected) return;

                try {
                    await this.pollFuelDispenser(fd);
                    fd.failures = 0;

                    const transactionsDAO = environment.transactionDAO;
                    if (transactionsDAO.connected) {
                        const nozzle = fd.selectedNozzle;
                        if (nozzle?.status === NozzleStatus.FUEL_COMPLETE) {
                            this.saveIfNewTransaction(transactionsDAO, fd, nozzle);
                        } else {
                            this.saveTotalsIfChanged(transactionsDAO, fd);
                        }
                    }
                } catch (err) {
                    fd.failures++;
                    if (fd.failures > this.profile.maxConsecutiveFailures) {
                        // mark this as offline
                        fd.reset();
                    }
                } finally {
                    this.lastPollTime.set(fd.id, new Date().getTime())
                }
            }
        } catch (err) {
            logWarn(`FD Polling error: ${err}`);
        }

        const nextPollDelay = onlineFuelDispensers.length > 0 ? this.profile.pollInterval : this.profile.inHolsterPollInterval;
        if (this.connected) this.nextPoll = setTimeout(() => this.pollFuelDispensers(), nextPollDelay);
    }

    protected async pollReCheckOfflineFuelDispensers(): Promise<void> {
        const reCheckFuelDispensers = this.fuelDispensers.filter(fd => this.shouldTryConnect(fd));
        logDebug(`Polling ${reCheckFuelDispensers.length} offline fuel dispensers...`);

        try {
            for (const fd of reCheckFuelDispensers) {
                if (!this.connected) return;

                try {
                    await this.tryConnect(fd, Priority.LOWEST);
                } catch (err) {
                    logWarn(`FD Polling error: ${err}`);
                    fd.failures++;
                    if (fd.failures > this.profile.maxConsecutiveFailures) {
                        // mark this as offline
                        fd.reset();
                    }
                } finally {
                    this.lastPollTime.set(fd.id, new Date().getTime())
                }
            }
        } catch (err) {
            logWarn(`Re-check error: ${err}`);
        }

        if (this.connected) this.nextOfflineReCheck = setTimeout(() => this.pollReCheckOfflineFuelDispensers(), this.profile.offlineTryReconnectInterval);
    }

    private async saveIfNewTransaction(transactionsDAO: TransactionsDAO, fd: IFuelDispenser, nozzle: INozzle): Promise<void> {
        const nozzleKey = `${fd.id}-${nozzle.nozzleNumber}`;
        const txnData = `${nozzle.fuelingSequence},${nozzle.status},${nozzle.unitPrice},${nozzle.amount},${nozzle.volume},${nozzle.totalAmount},${nozzle.totalVolume},${nozzle.productType}`;

        if (this.lastFuelDispenserTransactions.get(nozzleKey) !== txnData) {
            try {
                // save new transaction
                this.lastFuelDispenserTransactions.set(nozzleKey, txnData);
                await transactionsDAO.saveTransaction(fd, nozzle);
                await this.transactionSaved(fd, nozzle.fuelingSequence);
            } catch (err) {
                logError(`Error saving transaction: ${err}; txnData: ${txnData}`);
                this.lastFuelDispenserTransactions.delete(nozzleKey);
            }
        }
    }

    private async saveTotalsIfChanged(transactionsDAO: TransactionsDAO, fd: IFuelDispenser): Promise<void> {
        for (const nozzle of fd.nozzles) {
            if (nozzle.nozzleNumber === undefined || nozzle.nozzleNumber === null
                || nozzle.totalAmount === undefined || nozzle.totalAmount === null || nozzle.totalAmount === ''
                || nozzle.totalVolume === undefined || nozzle.totalVolume === null || nozzle.totalVolume === ''
                || nozzle.status === NozzleStatus.FUELING) continue;

            const nozzleKey = `${fd.id}-${nozzle.nozzleNumber}`;
            const totalsData = `${nozzle.totalAmount},${nozzle.totalVolume}`;

            if (this.lastFuelDispenserTotals.get(nozzleKey) !== totalsData) {
                try {
                    // save new totals
                    this.lastFuelDispenserTotals.set(nozzleKey, totalsData);
                    await transactionsDAO.saveLatestTotals(fd, nozzle);
                } catch (err) {
                    logError(`Error saving totals: ${err}; totals: ${totalsData}`);
                    this.lastFuelDispenserTotals.delete(nozzleKey);
                }
            }
        }
    }
}