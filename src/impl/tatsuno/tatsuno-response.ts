import { RS485Response } from "../protocol-base";
import { Codes } from "./tatsuno-rs485-protocol";

export class TatsunoResponse extends RS485Response {
    dataSent = false; // if data is sent successfully
    nak = false; // if NAK received

    get flags(): string[] {
        const flags: string[] = [];
        flags.push(this.success() ? "SUCCESS" : "FAILED");
        if (this.unitAddress === Codes.SELECTING) flags.push(this.dataSent ? "DATA_SENT" : "DATA_NOT_SENT");
        if (this.nak) flags.push("NAK");
        if (this.invalid) flags.push("INVALID");
        if (this.timedOut) flags.push("TIMEOUT");
        return flags;
    }

    constructor(public stationAddress: number, public unitAddress: number) {
        super();
    }

    success(): boolean {
        if (this.unitAddress === Codes.POLING) {
            return !this.nak && !this.invalid && !this.timedOut;
        } else if (this.unitAddress === Codes.SELECTING) {
            return !this.nak && !this.invalid && !this.timedOut && this.dataSent;
        } else {
            return false;
        }
    }

    toString(): string {
        if (this.unitAddress === Codes.POLING) {
            return `TatsunoResponse: SA 0x${this.stationAddress.toString(16)}, flags=[${this.flags.join()}], data: "${this.data}"`;
        } else {
            return `TatsunoResponse: SA 0x${this.stationAddress.toString(16)}, flags=[${this.flags.join()}]`;
        }
    }
}