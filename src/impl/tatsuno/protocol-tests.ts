import { logInfo } from "../buffer-utils";
import { TatsunoController, TatsunoProfile } from "./tatsuno-controller";
import { TatsunoPumpSimulator } from "./tatsuno-pump-simulator";
import { TatsunoRequest } from "./tatsuno-request";
import { Codes } from "./tatsuno-rs485-protocol";

function factorial(value: number): number {
    return value === 1 ? 1 : value * factorial(value - 1);
}

function getPermutation<T>(items: T[], sequence: number): T[] {
    if (items.length <= 1) {
        return items;
    } else {
        const nMinus1Fact = factorial(items.length - 1);
        const remaining = [...items];
        const item = remaining.splice(0, 1);
        const remainingPermutation = getPermutation(remaining, sequence % nMinus1Fact);

        const position = Math.trunc(sequence / nMinus1Fact);
        remainingPermutation.splice(position, 0, ...item);
        return remainingPermutation;
    }
}

export class TatsunoControllerV2Tests {
    controller: TatsunoController;
    pump: TatsunoPumpSimulator;

    constructor(private profile: TatsunoProfile) {
        this.controller = new TatsunoController(profile);
        this.pump = new TatsunoPumpSimulator(true);
    }

    async runScenarios(): Promise<void> {
        const controller = this.controller;
        const pump = this.pump;

        if (this.profile.serialPath === '/dev/ttySC0' && !!this.profile.serialOptions) {
            await pump.connectSerial('/dev/ttySC1', this.profile.serialOptions);
        } else if (this.profile.serialPath === '/dev/ttySC1' && !!this.profile.serialOptions) {
            await pump.connectSerial('/dev/ttySC0', this.profile.serialOptions);
        } else {
            await pump.listen(this.profile.port);
        }

        await controller.connect(true);

        const scenarios: ((scenarioNo: number) => void)[] = [
            async function (scenarioNo: number) {
                logInfo(`============== Scenario ${scenarioNo}: POLL receives no data (EOT) ==============`);
                const request = new TatsunoRequest(0x40, Codes.POLING);
                pump.resetSimulation(request.stationAddress);
                pump.eotOnPoll = true;

                const response = await controller.rs485Protocol.sendMessage(request);
                logInfo(`Diagnostics: Got response: ${response}`);
                if (`${response}` !== `TatsunoResponse: SA 0x40, flags=[SUCCESS], data: ""`) throw `Bad response: ${response}`;
            },
            async function (scenarioNo: number) {
                logInfo(`============== Scenario ${scenarioNo}: POLL times out ==============`);
                const request = new TatsunoRequest(0x40, Codes.POLING);
                pump.resetSimulation(request.stationAddress + 1);

                const response = await controller.rs485Protocol.sendMessage(request);
                logInfo(`Diagnostics: Got response: ${response}`);
                if (`${response}` !== `TatsunoResponse: SA 0x40, flags=[FAILED,TIMEOUT], data: ""`) throw `Bad response: ${response}`;
            },
            async function (scenarioNo: number) {
                logInfo(`============== Scenario ${scenarioNo}: POLL Bad STX ==============`);
                const request = new TatsunoRequest(0x40, Codes.POLING);
                pump.resetSimulation(request.stationAddress);
                pump.badStxOnPoll = true;

                const response = await controller.rs485Protocol.sendMessage(request);
                logInfo(`Diagnostics: Got response: ${response}`);
                if (`${response}` !== `TatsunoResponse: SA 0x40, flags=[FAILED,TIMEOUT], data: ""`) throw `Bad response: ${response}`;
            },
            async function (scenarioNo: number) {
                logInfo(`============== Scenario ${scenarioNo}: POLL receives data with invalid BCC  ==============`);
                const request = new TatsunoRequest(0x40, Codes.POLING);
                pump.resetSimulation(request.stationAddress);
                pump.data = "Bad BCC in reply";
                pump.badBCCOnPoll = true;

                const response = await controller.rs485Protocol.sendMessage(request);
                logInfo(`Diagnostics: Got response: ${response}`);
                if (`${response}` !== `TatsunoResponse: SA 0x40, flags=[FAILED,INVALID], data: "Bad BCC in reply"`) throw `Bad response: ${response}`;
            },
            async function (scenarioNo: number) {
                logInfo(`============== Scenario ${scenarioNo}: POLL receives data for correct station address ==============`);
                const request = new TatsunoRequest(0x40, Codes.POLING);
                pump.resetSimulation(request.stationAddress);
                pump.data = "Correct Station Address in reply";

                const response = await controller.rs485Protocol.sendMessage(request);
                logInfo(`Diagnostics: Got response: ${response}`);
                if (`${response}` !== `TatsunoResponse: SA 0x40, flags=[SUCCESS], data: "Correct Station Address in reply"`) throw `Bad response: ${response}`;
            },
            async function (scenarioNo: number) {
                logInfo(`============== Scenario ${scenarioNo}: SELECT receives no data (EOT) ==============`);
                const request = new TatsunoRequest(0x40, Codes.SELECTING, "Select Data");
                pump.resetSimulation(request.stationAddress);
                pump.eotOnSelect = true;

                const response = await controller.rs485Protocol.sendMessage(request);
                logInfo(`Diagnostics: Got response: ${response}`);
                if (`${response}` !== `TatsunoResponse: SA 0x40, flags=[FAILED,DATA_NOT_SENT]`) throw `Bad response: ${response}`;
            },
            async function (scenarioNo: number) {
                logInfo(`============== Scenario ${scenarioNo}: SELECT times out  ==============`);
                const request = new TatsunoRequest(0x40, Codes.SELECTING, "Select Data");
                pump.resetSimulation(request.stationAddress + 1);

                const response = await controller.rs485Protocol.sendMessage(request);
                logInfo(`Diagnostics: Got response: ${response}`);
                if (`${response}` !== `TatsunoResponse: SA 0x40, flags=[FAILED,DATA_NOT_SENT,TIMEOUT]`) throw `Bad response: ${response}`;
            },
            async function (scenarioNo: number) {
                logInfo(`============== Scenario ${scenarioNo}: SELECT NAK on ACK0 ==============`);
                const request = new TatsunoRequest(0x40, Codes.SELECTING, "Select Data");
                pump.resetSimulation(request.stationAddress);
                pump.ack0OnSelect = false;

                const response = await controller.rs485Protocol.sendMessage(request);
                logInfo(`Diagnostics: Got response: ${response}`);
                if (`${response}` !== `TatsunoResponse: SA 0x40, flags=[FAILED,DATA_NOT_SENT,NAK]`) throw `Bad response: ${response}`;
            },
            async function (scenarioNo: number) {
                logInfo(`============== Scenario ${scenarioNo}: SELECT NAK on ACK1 ==============`);
                const request = new TatsunoRequest(0x40, Codes.SELECTING, "Select Data");
                pump.resetSimulation(request.stationAddress);
                pump.ack1OnSelect = false;

                const response = await controller.rs485Protocol.sendMessage(request);
                logInfo(`Diagnostics: Got response: ${response}`);
                if (`${response}` !== `TatsunoResponse: SA 0x40, flags=[FAILED,DATA_NOT_SENT,NAK]`) throw `Bad response: ${response}`;
            },
            async function (scenarioNo: number) {
                logInfo(`============== Scenario ${scenarioNo}: SELECT receives ACK1 ==============`);
                const request = new TatsunoRequest(0x40, Codes.SELECTING, "Expected Data");
                pump.resetSimulation(request.stationAddress);

                const response = await controller.rs485Protocol.sendMessage(request);
                logInfo(`Diagnostics: Got response: ${response}`);
                if (`${response}` !== `TatsunoResponse: SA 0x40, flags=[SUCCESS,DATA_SENT]`) throw `Bad response: ${response}`;
                if (pump.selected !== "Expected Data") throw `Selected result mismatch: ${pump.selected}`;
            },
        ];

        const startTime = Date.now();
        const scenarioNos = scenarios.map((_, i) => i);
        const totalPermutations = /* 1; */ factorial(scenarios.length);
        for (let sequence = 0; sequence < totalPermutations; sequence++) {
            logInfo(`============== Running scenario sequence #${sequence}/${totalPermutations}... ==============`);
            const scenarioSequence = getPermutation(scenarioNos, sequence);
            for (const scenarioNo of scenarioSequence) {
                await scenarios[scenarioNo](scenarioNo + 1);
                logInfo("SUCCESS");
            }
        }

        pump.disconnect();

        const totalRunTime = Date.now() - startTime;
        logInfo(`${totalPermutations} total scenario permutations completed in ${totalRunTime}ms.`);
    }
}