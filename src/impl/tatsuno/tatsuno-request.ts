import { encode } from "../buffer-utils";
import { Priority, RS485Request } from "../protocol-base";
import { TatsunoResponse } from "./tatsuno-response";

export class TatsunoRequest extends RS485Request<TatsunoResponse> {
    constructor(public stationAddress: number, public unitAddress: number, data?: string, priority: Priority = Priority.LOW) {
        super(data ? encode(data) : new Uint8Array(), priority);
    }

    toString(): string {
        return `TatsunoRequest: SA#0x${this.stationAddress.toString(16)}, UA#0x${this.unitAddress?.toString(16)}, data: "${this.data}"`;
    }
}