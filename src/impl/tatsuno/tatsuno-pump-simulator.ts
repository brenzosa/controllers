import { bcc, isDebugEnabled, encode, logDebug, logInfo, logWarn, logError } from "../buffer-utils";
import { RS485ProtocolBase, ProtocolState } from "../protocol-base";
import { Codes, TIMEOUT, toProtocolString } from "./tatsuno-rs485-protocol";
import { TatsunoRequest } from "./tatsuno-request";
import { TatsunoResponse } from "./tatsuno-response";
import { AmountType, AuthTerms, ProductFlagCode, SelectCodes, TatsunoProductType, UnitPriceFlag } from "./tatsuno-codes";
import { NozzleStatus, ProductType } from "../controllers";
import { crc16ccitt } from "crc";

const defaultUnitPrice = 50.0;
const defaultVolume = 40.0;

enum TatsunoState {
    INITIAL, ENQ, STX, STX_SA, STX_UA, UA, DATA, ACK0_A, ACK0_B, ACK1_A, ACK1_B, BCC, EOT, ACK1_EOT, FINAL
}

async function wait(millis: number): Promise<void> {
    return new Promise(resolve => setTimeout(() => resolve(), millis));
}

export class TatsunoPumpSimulator extends RS485ProtocolBase<TatsunoRequest, TatsunoResponse> {
    fuelDispenser?: FuelDispenserSimulator;
    fuelDispensers: FuelDispenserSimulator[];

    stationAddress = 0;
    data = '';
    selected = '';

    eotOnPoll = false;
    respondToPoll = true;
    badStxOnPoll = false;
    badBCCOnPoll = false;

    eotOnSelect = false;
    ack0OnSelect = true;
    ack1OnSelect = true;

    constructor(private testMode: boolean = false, stationAddresses: number[] = [], loop = false) {
        super(TatsunoState.INITIAL, TatsunoState.FINAL, TIMEOUT);

        this.stateTransitions.set(TatsunoState.INITIAL, b => this.readStationAddress(b));
        this.stateTransitions.set(TatsunoState.UA, b => this.readUnitAddress(b));
        this.stateTransitions.set(TatsunoState.ENQ, b => this.checkENQ(b));
        this.stateTransitions.set(TatsunoState.ACK1_A, b => this.checkACK1_A(b));
        this.stateTransitions.set(TatsunoState.ACK1_B, b => this.checkACK1_B(b));
        this.stateTransitions.set(TatsunoState.STX, b => this.checkSTX(b));
        this.stateTransitions.set(TatsunoState.STX_SA, b => this.readStxStationAddress(b));
        this.stateTransitions.set(TatsunoState.STX_UA, b => this.readStxUnitAddress(b));
        this.stateTransitions.set(TatsunoState.DATA, b => this.readData(b));
        this.stateTransitions.set(TatsunoState.BCC, b => this.checkBCC(b));
        this.resetSimulation(0);

        this.fuelDispensers = stationAddresses.map(stationAddress => new FuelDispenserSimulator(stationAddress, this));
        if (loop) this.loopFuelSequence().catch(err => logError(`Simulation loop error: ${err}`)).finally(() => logInfo(`Simulation loop ended.`));
        else logInfo(`Use '--loop' to run fuel authorization simulation loop.`);
    }

    private async loopFuelSequence(): Promise<void> {
        const volumeAuthorization = "1131006000260002600026000260002600026000"; // Volume
        const costAuthorization = "1132100000260002600026000260002600026000"; // Price

        logInfo(`Running fuel authorization loop...`);
        for (; ;) {
            for (const fd of this.fuelDispensers) {
                try {
                    logInfo(`Waiting before next volume authorization round...`);
                    await wait(15000);
                    logInfo(`Volume authorization for 0x${fd.stationAddress.toString(16)}...`);
                    fd.processSelect(volumeAuthorization);
                } catch (err) {
                    logWarn(`Fuel Volume Authorization error: ${err}`);
                }

                try {
                    logInfo(`Waiting before next cost authorization round...`);
                    await wait(15000);
                    logInfo(`Cost authorization for 0x${fd.stationAddress.toString(16)}...`);
                    fd.processSelect(costAuthorization);
                } catch (err) {
                    logWarn(`Fuel Cost Authorization error: ${err}`);
                }
            }
        }
    }

    public resetSimulation(stationAddress: number): void {
        this.stationAddress = stationAddress;
        this.data = 'Simulator default response.';
        this.selected = '';

        this.eotOnPoll = false;
        this.respondToPoll = true;
        this.badStxOnPoll = false;
        this.badBCCOnPoll = false;

        this.eotOnSelect = false;
        this.ack0OnSelect = true;
        this.ack1OnSelect = true;
    }

    public async sendMessage(): Promise<TatsunoResponse> {
        throw `Illegal request: Pump cannot initiate messages`;
    }

    protected newEmptyResponse(): TatsunoResponse {
        throw `Illegal request: Pump cannot send empty responses`;
    }

    protected newTimedoutResponse(): TatsunoResponse {
        throw `Illegal request: Pump don't initiate requests, hence, can't time-out.`;
    }

    protected sendRequest(): ProtocolState {
        throw `Illegal request: Pump don't send requests.`;
    }

    protected responseMatchesRequest(): boolean {
        throw `Illegal request: Pump don't make requests.`;
    }

    protected onRead(buffer: Uint8Array): void {
        if (isDebugEnabled()) logDebug(`${this.constructor.name}  IN: ${toProtocolString(buffer)}`);
        return super.onRead(buffer);
    }

    public send(buffer: Uint8Array): void {
        if (isDebugEnabled()) logDebug(`${this.constructor.name} OUT: ${toProtocolString(buffer)}`);
        return super.send(buffer);
    }

    private readStationAddress(value: number): ProtocolState {
        if (value === Codes.EOT) {
            return TatsunoState.INITIAL;
        } else if (value < 0x40 || value > 0x7f) {
            throw `Received invalid Station Address: 0x${value.toString(16)}`;
        } else if (this.testMode && value != this.stationAddress) {
            throw `Ignoring request for different Station`
        } else if (!this.testMode && !this.fuelDispensers.some(fd => fd.stationAddress === value)) {
            throw `Ignoring request for unknown station address: 0x${value.toString(16)}`;
        } else {
            // load into station address
            this.request = new TatsunoRequest(value, -1);
            this.fuelDispenser = this.fuelDispensers.find(fd => fd.stationAddress === value);
            return TatsunoState.UA;
        }
    }

    private readUnitAddress(unitAddress: number): ProtocolState {
        if (unitAddress === Codes.EOT) {
            return TatsunoState.INITIAL;
        } else if (!this.request) {
            throw `Invalid state: No request data object ready`;
        }

        this.request.unitAddress = unitAddress;
        if (unitAddress !== Codes.POLING && unitAddress !== Codes.SELECTING) {
            throw `Expected POLLING or SELECTING but got 0x${unitAddress.toString(16)}`;
        }

        return TatsunoState.ENQ;
    }

    private checkENQ(value: number): ProtocolState {
        if (value === Codes.EOT) {
            return TatsunoState.INITIAL;
        } else if (value !== Codes.ENQ) {
            throw `Expecting ENQ but got 0x${value.toString(16)}`;
        } else if (!this.request) {
            throw `Got ENQ but there is no pending request`;
        } else if (this.request.unitAddress === Codes.POLING) {
            return this.sendPollResponse();
        } else if (this.request.unitAddress === Codes.SELECTING) {
            return this.sendSelectResponse();
        }

        throw `Invalid UA on request: ${this.request}`;
    }

    private sendPollResponse(): ProtocolState {
        if (this.eotOnPoll) {
            this.send(Uint8Array.of(Codes.EOT));
            return TatsunoState.INITIAL;
        } else if (!this.respondToPoll) {
            // do nothing...
            return TatsunoState.INITIAL;
        } else if (this.badStxOnPoll) {
            this.send(Uint8Array.of(Codes.NAK));
            return TatsunoState.INITIAL;
        } else {
            // send data...
            if (!this.testMode) this.fuelDispenser?.sendNextPollResponse();
            else this.sendText(this.data || '');
            return TatsunoState.ACK1_A;
        }
    }

    private sendSelectResponse(): ProtocolState {
        if (this.eotOnSelect) {
            this.send(Uint8Array.of(Codes.EOT));
            return TatsunoState.INITIAL;
        } else if (!this.ack0OnSelect) {
            this.send(Uint8Array.of(Codes.NAK));
            return TatsunoState.INITIAL;
        } else {
            this.send(Uint8Array.of(Codes.ACK, Codes.ACK0));
            return TatsunoState.STX;
        }
    }

    private checkSTX(value: number): ProtocolState {
        if (value === Codes.EOT) {
            return TatsunoState.INITIAL;
        } else if (value === Codes.STX) {
            if (!this.request) {
                throw `No request object; can't receive STX`;
            }

            return TatsunoState.STX_SA;
        } else {
            throw `Expected STX or EOT, but got: 0x${value.toString(16)}`;
        }
    }

    private readStxStationAddress(value: number): ProtocolState {
        if (value === Codes.EOT) {
            return TatsunoState.INITIAL;
        } else if (!this.request) {
            throw `Invalid state: No request data object ready`;
        } else if (this.request.stationAddress !== value) {
            throw `Station Address mismatch`;
        }

        return TatsunoState.STX_UA;
    }

    private readStxUnitAddress(value: number): ProtocolState {
        if (value === Codes.EOT) {
            return TatsunoState.INITIAL;
        } else if (!this.request) {
            throw `Invalid state: No request data object ready`;
        } else if (this.request.unitAddress !== value) {
            throw `Unit Address mismatch`;
        }

        return TatsunoState.DATA;
    }

    private checkACK1_A(value: number): ProtocolState {
        if (value === Codes.EOT) {
            return TatsunoState.INITIAL;
        } else if (value === Codes.ACK) {
            return TatsunoState.ACK1_B;
        } else {
            throw `Expected ACK/ACK1, but got: 0x${value.toString(16)}`;
        }
    }

    private checkACK1_B(value: number): ProtocolState {
        if (value === Codes.EOT) {
            return TatsunoState.INITIAL;
        } else if (value !== Codes.ACK1) {
            throw `Expected ACK1, but got: 0x${value.toString(16)}`;
        }

        // end of exchange...
        this.send(Uint8Array.of(Codes.EOT));

        // reset back to initial state
        return TatsunoState.INITIAL;
    }

    private readData(data: number): ProtocolState {
        if (data === Codes.EOT) {
            return TatsunoState.INITIAL;
        }

        if (!this.request) {
            throw `No pending request; dropping packet: 0x${data.toString(16)}`;
        }

        if (data === Codes.ETX) {
            return TatsunoState.BCC;
        } else {
            // max length (including STX, SA, UA, ETX, and BCC) = 256
            if (this.request.data && this.request.data.length > 251) {
                throw `Select data exceeded max length; dropping packet: 0x${data.toString(16)}`;
            }

            this.request.buffer = Uint8Array.of(...this.request.buffer, data);
            return TatsunoState.DATA;
        }
    }

    private checkBCC(bccValue: number): ProtocolState {
        if (!this.request) {
            throw `Invalid state: No request data object ready`;
        }

        const bccExpected = this.request.stationAddress ^ this.request.unitAddress ^ bcc(this.request?.buffer) ^ Codes.ETX;
        if (bccExpected !== bccValue) {
            throw `BCC mismatch (0x${bccValue.toString(16)} <> 0x${bccExpected.toString(16)}) `;
        }

        this.selected = this.request.data;
        if (!this.ack1OnSelect) {
            this.send(Uint8Array.of(Codes.NAK));
            return TatsunoState.INITIAL;
        } else {
            if (!this.testMode) this.fuelDispenser?.processSelect(this.request.data);
            this.send(Uint8Array.of(Codes.ACK, Codes.ACK1));
            return TatsunoState.INITIAL;
        }
    }

    private sendText(data: string): void {
        const buffer = encode(data);
        const bccValue = (this.stationAddress ^ Codes.POLING ^ bcc(buffer) ^ Codes.ETX) + (this.badBCCOnPoll ? 1 : 0);
        this.send(Uint8Array.of(Codes.STX, this.stationAddress, Codes.POLING, ...buffer, Codes.ETX, bccValue));
    }

    protected stateName(state: ProtocolState): string {
        switch (state) {
            case TatsunoState.INITIAL:
                return 'INITIAL';
            case TatsunoState.ENQ:
                return 'ENQ';
            case TatsunoState.STX:
                return 'STX';
            case TatsunoState.STX_SA:
                return 'STX_SA';
            case TatsunoState.STX_UA:
                return 'STX_UA';
            case TatsunoState.UA:
                return 'UA';
            case TatsunoState.DATA:
                return 'DATA';
            case TatsunoState.ACK0_A:
                return 'ACK0_A';
            case TatsunoState.ACK0_B:
                return 'ACK0_B';
            case TatsunoState.ACK1_A:
                return 'ACK1_A';
            case TatsunoState.ACK1_B:
                return 'ACK1_B';
            case TatsunoState.BCC:
                return 'BCC';
            case TatsunoState.EOT:
                return 'EOT';
            case TatsunoState.ACK1_EOT:
                return 'ACK1_EOT';
            case TatsunoState.FINAL:
                return 'FINAL';
            default:
                return `${state}`;
        }
    }
}

class NozzleState {
    constructor(public productType: ProductType = TatsunoProductType.REGULAR_UNLEADED, public status: NozzleStatus = NozzleStatus.IN_HOLSTER) { }

    unitPrice = 0;
    amount = 0;
    volume = 0;
    totalAmount = 0;
    totalVolume = 0;
}

class FuelAuthorization {
    amountType: AmountType = AmountType.VOLUME;
    fuelCost = 0;
    fuelVolume = 0;
    unitPrice = 0;
}

class FuelDispenserSimulator {
    powerOnSequenceComplete = false;
    challenge = '';
    selected = '';

    nozzles = [
        new NozzleState(TatsunoProductType.REGULAR_UNLEADED),
        // new NozzleState(TatsunoProductType.HIGH_OCTANE_UNLEADED),
        new NozzleState(TatsunoProductType.DIESEL)
    ];
    selectedNozzle: number | null = null;
    cancelFueling: (() => any) | null = null;

    constructor(public stationAddress: number, private simulator: TatsunoPumpSimulator) {
    }

    private sendText(data: string): void {
        const buffer = encode(data);
        const bccValue = (this.stationAddress ^ Codes.POLING ^ bcc(buffer) ^ Codes.ETX);
        this.simulator.send(Uint8Array.of(Codes.STX, this.stationAddress, Codes.POLING, ...buffer, Codes.ETX, bccValue));
    }

    public sendNextPollResponse(): void {
        if (!this.powerOnSequenceComplete) {
            this.challenge = Math.trunc(Math.random() * 10000).toString().padStart(6, '0');
            this.sendText(this.challenge);
            return;
        }

        const nozzle = this.selectedNozzle !== null ? this.nozzles[this.selectedNozzle] : null;
        switch (this.selected.substring(0, 2)) {
            case '00':
                this.sendText('6020'); // POWER ON
                break;

            case SelectCodes.CANCEL_AUTHORIZATION:
            case SelectCodes.LOCK_PUMP:
            case SelectCodes.RELEASE_PUMP:
            case SelectCodes.QUERY_STATUS:
                this.sendStatus(nozzle);
                break;

            case SelectCodes.QUERY_TOTALS:
                this.sendTotals();
                break;

            default:
                // no selection yet.. send EOT
                this.simulator.send(Uint8Array.of(Codes.EOT));
                break;
        }
    }

    private sendStatus(nozzle: NozzleState | null): void {
        const nozzleStatus = nozzle?.status || NozzleStatus.IN_HOLSTER;

        const status = `61${nozzleStatus}`
            + `${(nozzle?.volume || 0).toFixed(2).replace('.', '').padStart(6, '0')}`
            + `${nozzle ? UnitPriceFlag.CASH_UNIT_PRICE : UnitPriceFlag.INVALID}`
            + `${(nozzle?.unitPrice || 0).toFixed(2).replace('.', '').padStart(4, '0')}`
            + `${(nozzle?.amount || 0).toFixed(2).replace('.', '').padStart(6, '0')}`
            + `${this.selectedNozzle !== null ? this.selectedNozzle + 1 : 0}`
            + `${nozzle?.productType || TatsunoProductType.NONE}`
            + '2';
        this.sendText(status);
    }

    private sendTotals(): void {
        const nozzleText = this.nozzles.map(nozzle => {
            const volume = nozzle.totalVolume.toFixed(2).replace('.', '').padStart(10, '0');
            const amount = nozzle.totalAmount.toFixed(2).replace('.', '').padStart(10, '0');
            return `${volume}${amount}`;
        }).join('');

        const totals = `65`
            + `${this.nozzles[0]?.productType || TatsunoProductType.NONE}`
            + `${this.nozzles[1]?.productType || TatsunoProductType.NONE}`
            + `${this.nozzles[2]?.productType || TatsunoProductType.NONE}`
            + `   ` // 3 spare 
            + `${nozzleText}`;
        this.sendText(totals);
    }

    public processSelect(text: string): void {
        this.selected = text;

        if (!this.powerOnSequenceComplete) {
            const expectedResponse = `00${this.packedCrc(this.challenge)}`;
            this.powerOnSequenceComplete = (text === expectedResponse);
            return;
        }

        switch (text.substring(0, 2)) {
            case '10':
                this.fuelAuthorization1(text);
                break;
            case '11':
                this.fuelAuthorization2(text);
                break;
            case '21':
                this.setFuelPrices(text);
                break;
            case SelectCodes.CANCEL_AUTHORIZATION:
                if (this.cancelFueling) this.cancelFueling();
                this.cancelFueling = null;
                break;
            case SelectCodes.LOCK_PUMP:
                if (this.cancelFueling) this.cancelFueling();
                this.cancelFueling = null;
                this.nozzles.forEach(nozzle => nozzle.status = NozzleStatus.IN_HOLSTER);
                break;
            case SelectCodes.RELEASE_PUMP:
                this.nozzles.forEach(nozzle => nozzle.status = NozzleStatus.IN_HOLSTER);
                break;
            case SelectCodes.QUERY_TOTALS:
            case SelectCodes.QUERY_STATUS:
                // reponse on next poll will be sent accordingly
                break;
            default:
                throw `Uknown SELECT data: ${text}`;
        }
    }

    private fuelAuthorization1(text: string): void {
        const request = /^10(.)(.)(......)(.)(....)$/.exec(text);
        if (!request) {
            throw `Invalid request: ${text}`;
        } else if (this.cancelFueling) {
            throw `Cannot authorize while still fueling`;
        }

        const unitPriceFlag = request[4] as UnitPriceFlag;
        const fuelAuthorization = new FuelAuthorization();
        if (unitPriceFlag === UnitPriceFlag.INVALID || unitPriceFlag === UnitPriceFlag.PROHIBITED) {
            // not currently authorized, so do nothing?
            return;
        } else if (unitPriceFlag === UnitPriceFlag.USER_INPUT) {
            fuelAuthorization.unitPrice = defaultUnitPrice;
        } else {
            fuelAuthorization.unitPrice = parseFloat(request[5]) / 100 || defaultUnitPrice;
        }

        const terms = request[1] as AuthTerms;
        fuelAuthorization.amountType = request[2] as AmountType;

        switch (fuelAuthorization.amountType) {
            case AmountType.PRICE:
                fuelAuthorization.fuelCost = parseFloat(request[3]) / 100;
                fuelAuthorization.fuelVolume = fuelAuthorization.fuelCost / fuelAuthorization.unitPrice;
                break;
            case AmountType.VOLUME:
                fuelAuthorization.fuelVolume = parseFloat(request[3]) / 100;
                fuelAuthorization.fuelCost = fuelAuthorization.fuelVolume * fuelAuthorization.unitPrice;
                break;
        }

        if (terms === AuthTerms.NORMAL || !fuelAuthorization.fuelCost || !fuelAuthorization.fuelVolume) {
            fuelAuthorization.fuelVolume = defaultVolume;
            fuelAuthorization.fuelCost = fuelAuthorization.unitPrice * fuelAuthorization.fuelVolume;
        }

        // choose a random nozzle
        this.selectedNozzle = Math.trunc(this.nozzles.length * Math.random());
        const nozzle = this.nozzles[this.selectedNozzle];
        nozzle.amount = 0;
        nozzle.volume = 0;
        nozzle.unitPrice = fuelAuthorization.unitPrice;
        nozzle.status = NozzleStatus.IN_HOLSTER;

        // perform fueling sequence
        this.runFuelSequence(fuelAuthorization, nozzle).catch(() => logInfo(`Simulated fueling canceled.`));
    }

    private fuelAuthorization2(text: string): void {
        const request = /^11(.)(.)(......)(.)(....)(.)(....)(.)(....)(.)(....)(.)(....)(.)(....)$/.exec(text);
        if (!request) {
            throw `Invalid request: ${text}`;
        } else if (this.cancelFueling) {
            throw `Cannot authorize while still fueling`;
        }

        // choose a random nozzle
        this.selectedNozzle = Math.trunc(this.nozzles.length * Math.random());
        const nozzle = this.nozzles[this.selectedNozzle];
        nozzle.amount = 0;
        nozzle.volume = 0;
        nozzle.status = NozzleStatus.IN_HOLSTER;

        // find matching price based on Nozzle's product Id
        const fuelAuthorization = new FuelAuthorization();
        switch (nozzle.productType) {
            case '1':
                fuelAuthorization.unitPrice = parseFloat(request[5]) / 100 || defaultUnitPrice;
                break;
            case '2':
                fuelAuthorization.unitPrice = parseFloat(request[7]) / 100 || defaultUnitPrice;
                break;
            case '3':
                fuelAuthorization.unitPrice = parseFloat(request[9]) / 100 || defaultUnitPrice;
                break;
            case '4':
                fuelAuthorization.unitPrice = parseFloat(request[11]) / 100 || defaultUnitPrice;
                break;
            case '5':
                fuelAuthorization.unitPrice = parseFloat(request[13]) / 100 || defaultUnitPrice;
                break;
            case '6':
                fuelAuthorization.unitPrice = parseFloat(request[15]) / 100 || defaultUnitPrice;
                break;
            default:
                throw `No unit price for nozzle #${this.selectedNozzle} with product type ${nozzle.productType}`;
        }

        // Use default price in case the nozzle's unit price has not been set
        if (!fuelAuthorization.unitPrice) {
            throw `Unit price not set for Nozzle #${this.selectedNozzle}`;
        }

        const terms = request[1] as AuthTerms;
        fuelAuthorization.amountType = request[2] as AmountType;

        switch (fuelAuthorization.amountType) {
            case AmountType.PRICE:
                fuelAuthorization.fuelCost = parseFloat(request[3]) / 100;
                fuelAuthorization.fuelVolume = fuelAuthorization.fuelCost / fuelAuthorization.unitPrice;
                break;
            case AmountType.VOLUME:
                fuelAuthorization.fuelVolume = parseFloat(request[3]) / 100;
                fuelAuthorization.fuelCost = fuelAuthorization.fuelVolume * fuelAuthorization.unitPrice;
                break;
        }

        if (terms === AuthTerms.NORMAL || !fuelAuthorization.fuelCost || !fuelAuthorization.fuelVolume) {
            fuelAuthorization.fuelVolume = defaultVolume;
            fuelAuthorization.fuelCost = fuelAuthorization.unitPrice * fuelAuthorization.fuelVolume;
        }

        // perform fueling sequence
        this.runFuelSequence(fuelAuthorization, nozzle).catch(() => logInfo(`Simulated fueling canceled.`));
    }

    private setFuelPrices(text: string): void {
        const request = /^21(.)(.)(....)(.)(....)(.)(....)(.)(....)(.)(....)(.)(....)$/.exec(text);
        if (!request) {
            throw `Invalid request: ${text}`;
        }

        for (let p = 1; p <= 6; p++) {
            const productTypeFlag = request[(p - 1) * 2 + 2] as ProductFlagCode;
            if (productTypeFlag === ProductFlagCode.VALID) {
                const unitPrice = parseFloat(request[(p - 1) * 2 + 3]) / 100 || defaultUnitPrice;
                this.nozzles
                    .filter(nozzle => nozzle.productType === `${p}`)
                    .forEach(nozzle => nozzle.unitPrice = unitPrice);
            }
        }
    }

    private async runFuelSequence(fuelAuthorization: FuelAuthorization, nozzle: NozzleState) {
        logInfo(`Simulated fueling sequence initiated: ${JSON.stringify(fuelAuthorization)}`);

        // wait 1 second to pull out of holster
        await this.wait(1000, nozzle);
        nozzle.status = NozzleStatus.NOT_IN_HOLSTER;
        logInfo(`Simulated removal from holster...`);

        // wait 2 second to start fueling
        await this.wait(2000, nozzle);
        nozzle.status = NozzleStatus.FUELING;
        logInfo(`Simulated fueling commencing...`);

        // wait 1 seconds before pumping fuel
        await this.wait(1000, nozzle);

        const startingTotalVolume = nozzle.totalVolume;
        const startingTotalAmount = nozzle.totalAmount;
        let progress = 0;

        for (; ;) {
            // increment by 25% every second
            progress = Math.min(1, progress + 25 / 100 / 100);

            // update fuel volume / amount
            nozzle.volume = parseFloat((fuelAuthorization.fuelVolume * progress).toFixed(2));
            nozzle.amount = parseFloat((fuelAuthorization.fuelCost * progress).toFixed(2));

            // update totals
            nozzle.totalVolume = parseFloat((startingTotalVolume + nozzle.volume).toFixed(2));
            nozzle.totalAmount = parseFloat((startingTotalAmount + nozzle.amount).toFixed(2));

            if (progress === 1) {
                nozzle.status = NozzleStatus.FUEL_COMPLETE;
                break;
            }

            // wait 10 millis before incrementing again
            await this.wait(10, nozzle);
        }

        // put back in holster after 3 seconds...
        logInfo(`Simulated Fueling Complete.`);
        await this.wait(3000, nozzle);

        // done!
        logInfo(`Simulated Nozzle Restored.`);
        nozzle.status = NozzleStatus.IN_HOLSTER;
    }

    private async wait(millis: number, nozzle: NozzleState): Promise<void> {
        return new Promise((resolve, reject) => {
            this.cancelFueling = () => {
                nozzle.status = NozzleStatus.IN_HOLSTER;
                reject();
            }

            setTimeout(() => {
                this.cancelFueling = null;
                resolve();
            }, millis);
        });
    }

    private packedCrc(data: string): string {
        const crc = crc16ccitt(encode(data));
        const hiByte = (crc >> 8) & 0xff;
        const loByte = crc & 0xff;
        const unpacked = ((hiByte + loByte) & 0xff).toString(16).padStart(2, '0').toUpperCase();
        return unpacked;
    }
}