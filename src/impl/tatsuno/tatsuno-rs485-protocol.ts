import { bcc, isDebugEnabled, encode, logDebug } from "../buffer-utils";
import { RS485ProtocolBase, ProtocolState } from "../protocol-base";
import { TatsunoRequest } from "./tatsuno-request";
import { TatsunoResponse } from "./tatsuno-response";

export const TIMEOUT = 1000;

export enum Codes {
    EOT = 0x04, ENQ = 0x05, STX = 0x02, ETX = 0x03,
    ACK = 0x10, ACK0 = 0x30, ACK1 = 0x31, NAK = 0x15,
    POLING = 0x51, SELECTING = 0x41,
}

enum TatsunoState {
    INITIAL, STX, SA, UA, DATA, ACK0_A, ACK0_B, ACK1_A, ACK1_B, BCC, ACK1_EOT, FINAL
}

export class TatsunoRS485Protocol extends RS485ProtocolBase<TatsunoRequest, TatsunoResponse> {
    constructor(timeout: number = TIMEOUT) {
        super(TatsunoState.INITIAL, TatsunoState.FINAL, timeout);

        this.stateTransitions.set(TatsunoState.INITIAL, b => this.checkEOT(b));
        this.stateTransitions.set(TatsunoState.STX, b => this.checkSTX(b));
        this.stateTransitions.set(TatsunoState.ACK0_A, b => this.checkACK0_A(b));
        this.stateTransitions.set(TatsunoState.ACK0_B, b => this.checkACK0_B(b));
        this.stateTransitions.set(TatsunoState.ACK1_A, b => this.checkACK1_A(b));
        this.stateTransitions.set(TatsunoState.ACK1_B, b => this.checkACK1_B(b));
        this.stateTransitions.set(TatsunoState.SA, b => this.readStationAddress(b));
        this.stateTransitions.set(TatsunoState.UA, b => this.readUnitAddress(b));
        this.stateTransitions.set(TatsunoState.DATA, b => this.readData(b));
        this.stateTransitions.set(TatsunoState.BCC, b => this.checkBCC(b));
        this.stateTransitions.set(TatsunoState.ACK1_EOT, b => this.checkEOTAfterACK1(b));
    }

    protected onRead(buffer: Uint8Array): void {
        if (isDebugEnabled()) logDebug(`${this.constructor.name}  IN: ${toProtocolString(buffer)}`);
        return super.onRead(buffer);
    }

    public send(buffer: Uint8Array): void {
        if (isDebugEnabled()) logDebug(`${this.constructor.name} OUT: ${toProtocolString(buffer)}`);
        return super.send(buffer);
    }

    protected newEmptyResponse(request?: TatsunoRequest): TatsunoResponse {
        const stationAddress = request?.stationAddress || -1;
        const unitAddress = request?.unitAddress || -1;
        return new TatsunoResponse(stationAddress, unitAddress);
    }

    protected newTimedoutResponse(request: TatsunoRequest): TatsunoResponse {
        const response = new TatsunoResponse(request.stationAddress, request.unitAddress);
        response.timedOut = true;
        return response;
    }

    protected sendRequest(request: TatsunoRequest): ProtocolState {
        if (request.unitAddress === Codes.POLING) {
            this.sendEnq(request);
            return TatsunoState.STX;
        } else if (request.unitAddress === Codes.SELECTING) {
            this.sendEnq(request);
            return TatsunoState.ACK0_A;
        } else {
            throw `Ignoring request with UA: 0x${request.unitAddress.toString(16)}`;
        }
    }

    protected responseMatchesRequest(request: TatsunoRequest, response: TatsunoResponse): boolean {
        return request.stationAddress === response.stationAddress
            && request.unitAddress === response.unitAddress;
    }

    private checkEOT(value: number): ProtocolState {
        if (value !== Codes.EOT) {
            throw `Expected EOT (state = INITIAL), but got: 0x${value.toString(16)}`;
        }

        // end of exchange...
        return TatsunoState.INITIAL;
    }

    private checkSTX(value: number): ProtocolState {
        if (value === Codes.EOT) {
            // no data..
            this.response = this.newEmptyResponse(this.request);
            return TatsunoState.FINAL;
        } else if (value === Codes.STX) {
            this.response = new TatsunoResponse(-1, -1);
            return TatsunoState.SA;
        } else {
            throw `Expected STX or EOT, but got: 0x${value.toString(16)}`;
        }
    }

    private readStationAddress(stationAddress: number): ProtocolState {
        if (stationAddress < 0x40 || stationAddress > 0x7f) {
            throw `Invalid SA value: 0x${stationAddress.toString(16)}`;
        }

        if (!this.response) {
            throw `Invalid state: No response data object ready`;
        }

        this.response.stationAddress = stationAddress;
        return TatsunoState.UA;
    }

    private readUnitAddress(unitAddress: number): ProtocolState {
        if (unitAddress !== Codes.POLING && unitAddress !== Codes.SELECTING) {
            throw `Invalid UA value: 0x${unitAddress.toString(16)}`;
        }

        if (!this.response) {
            throw `Invalid state: No response data object ready`;
        }

        this.response.unitAddress = unitAddress;
        return TatsunoState.DATA;
    }

    private readData(data: number): ProtocolState {
        if (!this.response) {
            throw `No pending response; dropping packet: 0x${data.toString(16)}`;
        }

        if (data === Codes.ETX) {
            return TatsunoState.BCC;
        } else {
            // max length (including STX, SA, UA, ETX, and BCC) = 256
            if (this.response!.data.length > 251) {
                throw `Response data exceeded max length; dropping packet: 0x${data.toString(16)}`;
            }

            this.response.buffer = Uint8Array.of(...this.response.buffer, data);
            return TatsunoState.DATA;
        }
    }

    private checkBCC(bccValue: number): ProtocolState {
        if (!this.response) {
            throw `Invalid state: No response data object ready`;
        }

        const bccExpected = this.response.stationAddress ^ this.response.unitAddress ^ bcc(this.response?.buffer) ^ Codes.ETX;
        if (bccExpected !== bccValue) {
            throw `BCC mismatch (${bccValue.toString(16)} <> ${bccExpected.toString(16)})`;
        }

        // Send ACK1 only if BCC is good
        this.send(Uint8Array.of(Codes.ACK, Codes.ACK1));

        return TatsunoState.ACK1_EOT;
    }

    private checkEOTAfterACK1(value: number): ProtocolState {
        if (value !== Codes.EOT) {
            throw `Expected EOT after ACK1, but got: 0x${value.toString(16)}`;
        }

        // end of exchange after receiving data...
        return TatsunoState.FINAL;
    }

    private checkACK0_A(value: number): ProtocolState {
        if (value === Codes.EOT) {
            // no data..
            this.response = this.newEmptyResponse(this.request);
            return TatsunoState.FINAL;
        } else if (value === Codes.NAK) {
            this.response = this.newEmptyResponse(this.request);
            this.response.nak = true;
            return TatsunoState.FINAL;
        } else if (value === Codes.ACK) {
            this.response = this.newEmptyResponse(this.request);
            return TatsunoState.ACK0_B;
        } else {
            throw `Expected ACK or EOT, but got: 0x${value.toString(16)}`;
        }
    }

    private checkACK0_B(value: number): ProtocolState {
        if (value === Codes.NAK) {
            this.response!.nak = true;
            return TatsunoState.FINAL;
        } else if (value === Codes.ACK0) {
            this.sendText(this.request);
            return TatsunoState.ACK1_A;
        } else {
            throw `Expected ACK0 but got: 0x${value.toString(16)}`;
        }
    }

    private checkACK1_A(value: number): ProtocolState {
        if (value === Codes.NAK) {
            this.response!.nak = true;
            return TatsunoState.FINAL;
        } else if (value === Codes.ACK) {
            return TatsunoState.ACK1_B;
        } else {
            throw `Expected ACK/ACK1, but got: 0x${value.toString(16)}`;
        }
    }

    private checkACK1_B(value: number): ProtocolState {
        if (value === Codes.NAK) {
            this.response!.nak = true;
            return TatsunoState.FINAL;
        } if (value === Codes.ACK1 || value === Codes.ACK0) {
            // end of exchange (NOTE: Docs indicate ACK0 is also valid response)...
            this.send(Uint8Array.of(Codes.EOT));
            this.response!.dataSent = true;
            return TatsunoState.FINAL;
        } else {
            throw `Expected ACK1, but got: 0x${value.toString(16)}`;
        }
    }

    private sendEnq(request: TatsunoRequest): void {
        if (!request) {
            throw `Unable to send data (missing request object)`;
        }

        this.send(Uint8Array.of(Codes.EOT, request.stationAddress, request.unitAddress, Codes.ENQ));
    }

    private sendText(request?: TatsunoRequest): void {
        if (!request) {
            throw `Unable to send data (missing request object)`;
        }

        const bccValue = request.stationAddress ^ request.unitAddress ^ bcc(request.buffer) ^ Codes.ETX;
        this.send(Uint8Array.of(Codes.STX, request.stationAddress, request.unitAddress, ...encode(request.data), Codes.ETX, bccValue));
    }

    protected stateName(state: ProtocolState): string {
        switch (state) {
            case TatsunoState.INITIAL:
                return 'INITIAL';
            case TatsunoState.STX:
                return 'STX';
            case TatsunoState.SA:
                return 'SA';
            case TatsunoState.UA:
                return 'UA';
            case TatsunoState.DATA:
                return 'DATA';
            case TatsunoState.ACK0_A:
                return 'ACK0_A';
            case TatsunoState.ACK0_B:
                return 'ACK0_B';
            case TatsunoState.ACK1_A:
                return 'ACK1_A';
            case TatsunoState.ACK1_B:
                return 'ACK1_B';
            case TatsunoState.BCC:
                return 'BCC';
            case TatsunoState.ACK1_EOT:
                return 'ACK1_EOT';
            case TatsunoState.FINAL:
                return 'FINAL';
            default:
                return `${state}`;
        }
    }
}

export function toProtocolString(buffer: Uint8Array): string {
    let stx = buffer.length > 0 && buffer[0] === Codes.STX;
    return [...buffer].map((b, index) => {
        if (!stx || index <= 2) {
            switch (b) {
                case Codes.EOT:
                    return "<EOT>";
                case Codes.ENQ:
                    return "<ENQ>";
                case Codes.STX:
                    return "<STX>";
                case Codes.ETX:
                    return "<ETX>";
                case Codes.ACK:
                    return "<ACK>";
                case Codes.ACK0:
                    return "<ACK0>";
                case Codes.ACK1:
                    return "<ACK1>";
                case Codes.NAK:
                    return "<NAK>";
                case Codes.POLING:
                    return "<POLING>";
                case Codes.SELECTING:
                    return "<SELECTING>";
                default:
                    return `0x${b.toString(16).padStart(2, '0').toUpperCase()}`;
            }
        } else {
            switch (b) {
                case Codes.ETX:
                    stx = false;
                    return "<ETX>";
                default:
                    break;
            }
        }

        if (b >= 32 && b <= 127) return String.fromCharCode(b);
        else return `0x${b.toString(16).padStart(2, '0').toUpperCase()}`;
    }).join('');
}