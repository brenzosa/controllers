import { crc16ccitt } from "crc";
import { encode, logError, logInfo, logWarn } from "../buffer-utils";
import { IFuelDispenser, INozzle, NozzleStatus } from "../controllers";
import { TatsunoResponse } from "./tatsuno-response";
import { TatsunoController, TatsunoProfile } from "./tatsuno-controller";
import { AmountType, AuthTerms, ErrorCode, ProductFlagCode, SelectCodes, TatsunoProductType, UnitPriceFlag, UnitRankCode } from "./tatsuno-codes";
import { Priority } from "../protocol-base";

class UnitPrice<FLAG_TYPE extends UnitPriceFlag | ProductFlagCode> {
    unitPriceFlag: FLAG_TYPE = ('0' as any);
    unitPrice = 0;

    constructor(items?: Partial<UnitPrice<FLAG_TYPE>>) {
        Object.assign(this, items);
    }

    toString(profile: TatsunoProfile): string {
        const unitPriceText = this.unitPrice
            .toFixed(profile.unitPriceDecimals)
            .replace('.', '')
            .padStart(4, '0');

        return `${this.unitPriceFlag}${unitPriceText}`;
    }
}

class TatsunoNozzle implements INozzle {
    set fuelingSequence(fuelingSequence: string) { this._fuelingSequence = fuelingSequence; }
    get fuelingSequence(): string { return this._fuelingSequence; }
    status = NozzleStatus.IN_HOLSTER;
    unitPrice = "";
    amount = "";
    volume = "";
    totalAmount = "";
    totalVolume = "";
    productType = TatsunoProductType.NONE;
    locked = false;

    private _fuelingSequence = '0';
    constructor(public nozzleNumber: number) { }
}

export class TatsunoFuelDispenser implements IFuelDispenser {
    get id(): string { return `${this.stationAddress.toString(16)}`; }
    get online(): boolean { return this.controllable && this.pricesSent; }
    get selectedNozzle(): INozzle | undefined { return this.nozzles.find(nozzle => nozzle.status !== NozzleStatus.IN_HOLSTER); }
    nozzles: TatsunoNozzle[] = [new TatsunoNozzle(1), new TatsunoNozzle(2), new TatsunoNozzle(3)];
    onUpdate?: () => void;

    public unlockPassword = '';
    public failures = 0;
    private controllable = false;
    private pricesSent = false;
    private errors = new Map<string, ErrorCode>();
    private selectedProduct = TatsunoProductType.NONE;
    private paymentMode = UnitRankCode.NONE;

    private lastStatus = '';
    private lastTotals = '';

    constructor(private controller: TatsunoController, private stationAddress: number) {
    }

    toString(): string {
        return `Tatsuno SA#0x${this.stationAddress.toString(16)}`
            + `, controllable=${this.controllable}`
            + `, pricesSent=${this.pricesSent}`
            + `, selectedProduct=${this.selectedProduct}`
            + `, paymentMode=${this.paymentMode}`
            + `, errors=${JSON.stringify(Object.fromEntries(this.errors))}`;
    }

    public reset(): void {
        this.failures = 0;
        this.errors.clear();
        this.selectedProduct = TatsunoProductType.NONE;
        this.paymentMode = UnitRankCode.NONE;
        this.controllable = false;
        this.pricesSent = false;
        this.notify();
    }

    async poke(priority: Priority = Priority.LOW, notifyOnSuccess = false): Promise<boolean> {
        const success = (await this.poll(priority)).success();
        if (notifyOnSuccess && success) this.notify();
        return success;
    }

    async authorize(): Promise<void> {
        const data = this.fuelAuthorization2(AuthTerms.NORMAL, AmountType.VOLUME, 0);
        const response = await this.select(data, Priority.HIGH);
        if (!response.success()) throw response;
    }

    async authorizeTotalCost(cost: number): Promise<void> {
        const data = this.fuelAuthorization2(AuthTerms.PRESET_VARIABLE, AmountType.PRICE, cost);
        const response = await this.select(data, Priority.HIGH);
        if (!response.success()) throw response;
    }

    async authorizeTotalVolume(volume: number): Promise<void> {
        const data = this.fuelAuthorization2(AuthTerms.PRESET_VARIABLE, AmountType.VOLUME, volume);
        const response = await this.select(data, Priority.HIGH);
        if (!response.success()) throw response;
    }

    async cancelAuthorization(): Promise<void> {
        const response = await this.select(SelectCodes.CANCEL_AUTHORIZATION, Priority.HIGH);
        if (!response.success()) throw response;
    }

    async lockPump(): Promise<void> {
        const response = await this.select(SelectCodes.LOCK_PUMP, Priority.HIGH);
        if (!response.success()) throw response;
    }

    async releasePump(): Promise<void> {
        const response = await this.select(SelectCodes.RELEASE_PUMP, Priority.HIGH);
        if (!response.success()) throw response;
    }

    async sendPrices(priority: Priority): Promise<void> {
        this.pricesSent = false;
        const result = await this.select(this.subUnitPrice(), priority);
        this.pricesSent = result.success();
        if (this.pricesSent) this.info(`Prices sent.`);
        else this.error(`Send prices FAILED.`);
    }

    select(data: string, priority: Priority = Priority.LOW): Promise<TatsunoResponse> {
        if (this.controller.profile.logMessages) {
            this.info(`POS -> 0x${this.stationAddress.toString(16)}: ${data}`)
        }

        return this.controller.select(this.stationAddress, data, priority)
    }

    private async poll(priority: Priority = Priority.LOW): Promise<TatsunoResponse> {
        const response = await this.controller.poll(this.stationAddress, priority);
        if (response.success()) {
            await this.processPollResponse(response, priority);
        }

        return response;
    }

    private fuelAuthorization1(
        authTerms: AuthTerms = AuthTerms.NORMAL,
        amountType: AmountType = AmountType.PRICE,
        amount = 0,
        unitPrice: UnitPrice<UnitPriceFlag> = { unitPriceFlag: UnitPriceFlag.PROHIBITED, unitPrice: 0 }
    ): string {
        const amountText = amount
            .toFixed(amountType === AmountType.VOLUME ? 2 : this.controller.profile.unitPriceDecimals)
            .replace('.', '')
            .padStart(6, '0');

        const unitPriceText = unitPrice.toString(this.controller.profile);
        if (unitPriceText.length !== 4) {
            throw `Invalid Unit Price: ${unitPriceText}`;
        } else if (amountText.length !== 6) {
            throw `Invalid Amount: ${amountText}`;
        }

        const text = `10${authTerms}${amountType}${amountText}${unitPriceText}`;
        if (text.length !== 15) {
            throw `Malformed fuel authorization (1): ${text}`;
        }

        return text;
    }

    private fuelAuthorization2(
        authTerms: AuthTerms = AuthTerms.NORMAL,
        amountType: AmountType = AmountType.PRICE,
        amount = 0
    ): string {
        const amountText = amount
            .toFixed(amountType === AmountType.VOLUME ? 2 : this.controller.profile.unitPriceDecimals)
            .replace('.', '')
            .padStart(6, '0');

        const unitPricesText = [
            TatsunoProductType.HIGH_OCTANE_UNLEADED,
            TatsunoProductType.REGULAR_UNLEADED,
            TatsunoProductType.DIESEL,
            TatsunoProductType.KEROSENE,
            TatsunoProductType.LEADED_HIGH_OCTANE,
            TatsunoProductType.LEADED_REGULAR
        ].map(TatsunoProductType => {
            const price = this.controller.unitPrices.get(TatsunoProductType);
            if (!this.controller.unitPrices.has(TatsunoProductType)) {
                return new UnitPrice<UnitPriceFlag>({ unitPriceFlag: UnitPriceFlag.PROHIBITED, unitPrice: 0 });
            } else if (price === 0) {
                return new UnitPrice<UnitPriceFlag>({ unitPriceFlag: UnitPriceFlag.USER_INPUT, unitPrice: price });
            } else if (!!price && price > 0) {
                return new UnitPrice<UnitPriceFlag>({ unitPriceFlag: UnitPriceFlag.CASH_UNIT_PRICE, unitPrice: price });
            } else {
                return new UnitPrice<UnitPriceFlag>({ unitPriceFlag: UnitPriceFlag.INVALID, unitPrice: 0 });
            }
        }).map(unitPrice => {
            const unitPriceText = unitPrice.toString(this.controller.profile);
            if (unitPriceText.length !== 5) throw `Invalid Unit Price: ${unitPriceText}`;
            return unitPriceText;
        }).join('');

        if (amountText.length !== 6) {
            throw `Invalid Amount: ${amountText}`;
        }

        const text = `11${authTerms}${amountType}${amountText}${unitPricesText}`;
        if (text.length !== 10 + 5 * 6) {
            throw `Malformed fuel authorization (1): ${text}`;
        }

        return text;
    }

    private subUnitPrice(unitRankCode: UnitRankCode = UnitRankCode.CASH): string {
        const subUnitPricesText = [
            TatsunoProductType.HIGH_OCTANE_UNLEADED,
            TatsunoProductType.REGULAR_UNLEADED,
            TatsunoProductType.DIESEL,
            TatsunoProductType.KEROSENE,
            TatsunoProductType.LEADED_HIGH_OCTANE,
            TatsunoProductType.LEADED_REGULAR
        ].map(productType => {
            const price = this.controller.unitPrices.get(productType);
            if (!this.controller.unitPrices.has(productType)) {
                return new UnitPrice<ProductFlagCode>({ unitPriceFlag: ProductFlagCode.INVALID, unitPrice: 0 });
            } else if (!!price && price <= 0) {
                return new UnitPrice<ProductFlagCode>({ unitPriceFlag: ProductFlagCode.BLANK, unitPrice: price });
            } else if (!!price && price > 0) {
                return new UnitPrice<ProductFlagCode>({ unitPriceFlag: ProductFlagCode.VALID, unitPrice: price });
            } else {
                return new UnitPrice<ProductFlagCode>({ unitPriceFlag: ProductFlagCode.INVALID, unitPrice: 0 });
            }
        }).map(unitPrice => {
            const unitPriceText = unitPrice.toString(this.controller.profile);
            if (unitPriceText.length !== 5) {
                throw `Invalid Unit Price: ${unitPriceText}`;
            }
            return unitPriceText;
        }).join('');

        const text = `21${unitRankCode}${subUnitPricesText}`;
        if (text.length !== 3 + 5 * 6) {
            throw `Malformed Sub-unit price data: ${text}`;
        }

        return text;
    }

    private info(message: string): void {
        logInfo(`SA 0x${this.stationAddress.toString(16)}: ${message}`);
    }

    private error(message: string): void {
        logError(`SA 0x${this.stationAddress.toString(16)}: ${message}`);
    }

    private async processPollResponse(response: TatsunoResponse, priority: Priority = Priority.LOW): Promise<void> {
        try {
            if (this.controller.profile.logMessages) {
                this.info(`POS <- 0x${this.stationAddress.toString(16)}: ${response.data}`)
            }

            const code = response.data.substring(0, 2);
            if (code.length === 2 && code !== '00' && code !== '60' && !this.controllable) {
                // already controllable
                this.readPumpCondition('6020', priority);
            }

            switch (code) {
                case '': // blank response?
                    // assume pump is already controllable
                    logWarn(`Got empty response from 0x${this.stationAddress.toString(16)}; assuming pump is controllable...`);
                    this.readPumpCondition('6020', priority);
                    break;

                case '00': // CRC challenge
                    if (this.controllable) this.reset();
                    this.info(`Responding to challenge: ${response.data}`);
                    await this.select(`00${this.packedCrc(response.data)}`, priority);
                    await this.poll(priority);
                    break;

                case '60': // pump condition
                    this.readPumpCondition(response.data);
                    break;
                case '61': // pump status
                    this.readPumpStatus(response.data);
                    break;
                case '62': // errror message
                    this.readErrorMessage(response.data);
                    break;
                case '65': // totals
                    this.readTotals(response.data);
                    break;
                case '67': // switch status (cash or credit)
                    this.readSwitchStatus(response.data);
                    break;
                default:
                    this.error(`Ignoring unrecognized response: ${response}`);
            }
        } catch (err) {
            this.error(`Error reading response: ${err}`);
        }
    }

    private async readPumpCondition(data: string, priority: Priority = Priority.LOW): Promise<void> {
        switch (data) {
            case '6010': // Power ON / controllable
                this.info(`Power ON.`);
                this.controllable = true;
                break;
            case '6020':
                this.info(`Pump controllable.`);
                this.controllable = true;
                break;
            default:
                this.info(`Pump not controllable (${data})`);
                this.controllable = false;
                break;
        }

        this.notify();

        if (this.controllable && !this.pricesSent) {
            try {
                await this.sendPrices(priority);
                this.notify();
            } catch (err) {
                this.error(`Error sending prices: ${err}`);
            }
        }
    }

    private readPumpStatus(data: string): void {
        // condition (1), fueling volume (4+2), u.price flag (1)
        // amount (6), nozzle Number(1)
        // product type(1), type of indication (1)
        const parts = /^61(.)(....)(..)(.)(....)(......)(.)(.)(.)$/.exec(data);
        if (parts) {
            if (parts[9] !== '2') throw `Ignoring pump status with invalid Type of indication: ${data}`;

            const nozzleNumber = parseInt(parts[7]);
            if (nozzleNumber === 0) {
                // nozzle is restored/no function
                return;
            }

            if (nozzleNumber < 1 || nozzleNumber > 3) throw `Ignoring pump status with invalid nozzle #: ${data}`;

            const nozzle = this.nozzles[nozzleNumber - 1];
            const prevNozzleStatus = nozzle.status;
            nozzle.status = parts[1] as NozzleStatus;
            nozzle.volume = `${parts[2]}.${parts[3]}`;
            if (parts[4] === '2') {
                // unit price and amount is valid
                const dot = this.controller.profile.unitPriceDecimals ? '.' : '';
                const unitPriceDecPos = 4 - this.controller.profile.unitPriceDecimals;
                nozzle.unitPrice = parts[5].substring(0, unitPriceDecPos) + dot + parts[5].substring(unitPriceDecPos);
                const amountDecPos = 6 - this.controller.profile.unitPriceDecimals;
                nozzle.amount = parts[6].substring(0, amountDecPos) + dot + parts[6].substring(amountDecPos);
            }

            if (prevNozzleStatus === NozzleStatus.IN_HOLSTER && nozzle.status !== prevNozzleStatus) {
                // assign a new sequence number once the nozzle is taken out of the holster:
                // it's surely impossible to perform two fueling sequences within the same second
                nozzle.fuelingSequence = (Math.trunc(new Date().getTime() / 1000) % (24 * 60 * 60)).toString();
            }

            nozzle.productType = parts[8] as TatsunoProductType;

            if (this.lastStatus !== data) this.notify();
            this.lastStatus = data;
        } else {
            this.error(`Ignoring pump status: ${data}`);
        }
    }

    private readErrorMessage(data: string): void {
        const parts = /^62(.)(.)$/.exec(data);
        if (parts) {
            const discriminationCode = parts[1];
            const errorCode = parts[2] as ErrorCode;
            this.errors.set(discriminationCode, errorCode);
        } else {
            this.error(`Ignoring error text: ${data}`);
        }
    }

    private readTotals(data: string): void {
        const parts = /^65(.)(.)(.)(...)(..........)?(..........)?(..........)?(..........)?(..........)?(..........)?$/.exec(data);
        if (parts) {
            this.nozzles[0].productType = parts[1] as TatsunoProductType;
            this.nozzles[1].productType = parts[2] as TatsunoProductType;
            this.nozzles[2].productType = parts[3] as TatsunoProductType;

            const amountDecPos = 10 - this.controller.profile.unitPriceDecimals;
            const dot = this.controller.profile.unitPriceDecimals ? '.' : '';
            if (parts[5] && parts[6]) {
                this.nozzles[0].totalVolume = parts[5].substring(0, 8) + '.' + parts[5].substring(8);
                this.nozzles[0].totalAmount = parts[6].substring(0, amountDecPos) + dot + parts[6].substring(amountDecPos);
            }
            if (parts[7] && parts[8]) {
                this.nozzles[1].totalVolume = parts[7].substring(0, 8) + '.' + parts[7].substring(8);
                this.nozzles[1].totalAmount = parts[8].substring(0, amountDecPos) + dot + parts[8].substring(amountDecPos);
            }
            if (parts[9] && parts[10]) {
                this.nozzles[2].totalVolume = parts[9].substring(0, 8) + '.' + parts[9].substring(8);
                this.nozzles[2].totalAmount = parts[10].substring(0, amountDecPos) + dot + parts[10].substring(amountDecPos);
            }

            if (this.lastTotals !== data) this.notify();
            this.lastTotals = data;
        } else {
            this.error(`Ignoring totals: ${data}`);
        }
    }

    private readSwitchStatus(data: string): void {
        const parts = /^67(.)(.)(.)$/.exec(data);
        if (parts) {
            if (parts[1] === '1') { // 1 means "unit price selector switch"
                if (parts[2] !== '0') {
                    this.selectedProduct = parts[2] as TatsunoProductType;
                } else {
                    this.selectedProduct = TatsunoProductType.NONE;
                }

                this.paymentMode = parts[3] as UnitRankCode;
            }
        } else {
            this.error(`Ignoring switch status: ${data}`);
        }
    }

    private packedCrc(data: string): string {
        const crc = crc16ccitt(encode(data));
        const hiByte = (crc >> 8) & 0xff;
        const loByte = crc & 0xff;
        const unpacked = ((hiByte + loByte) & 0xff).toString(16).padStart(2, '0').toUpperCase();
        return unpacked;
    }

    private notify(): void {
        try {
            if (this.onUpdate) this.onUpdate();
        } catch (err) {
            logWarn(`notify() error: ${err}`);
        }
    }
}