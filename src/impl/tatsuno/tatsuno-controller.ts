import { Priority } from "../protocol-base";
import { OpenOptions } from "serialport";
import { logError, logInfo, logWarn } from "../buffer-utils";
import { ControllerProfile, ProductType } from "../controllers";
import { SelectCodes } from "./tatsuno-codes";
import { TatsunoFuelDispenser } from "./tatsuno-fuel-dispenser";
import { TatsunoRequest } from "./tatsuno-request";
import { TatsunoResponse } from "./tatsuno-response";
import { Codes, TatsunoRS485Protocol } from "./tatsuno-rs485-protocol";
import { ControllerBase } from "../controller-base";

export class TatsunoProfile implements ControllerProfile {
    host = '';
    port = 0;
    unitPriceDecimals = 2;
    stationIds: number[] = [];
    timeout = 1000;
    pollInterval = 200;
    maxConsecutiveFailures = 6;
    logMessages = false;
    logStateTransitions = false;
    enableCpuStats = false;
    serialPath?: string;
    serialOptions?: OpenOptions;
    offlineTryReconnectInterval = 10000;
    inHolsterPollInterval = 1000;

    constructor(init?: Partial<TatsunoProfile>) {
        Object.assign(this, init);
    }
}

export class TatsunoController extends ControllerBase<TatsunoProfile, TatsunoRS485Protocol, TatsunoFuelDispenser, TatsunoRequest, TatsunoResponse> {
    constructor(public profile: TatsunoProfile) {
        super(profile, new TatsunoRS485Protocol(profile.timeout));
        super.fuelDispensers = profile.stationIds.map(stationId => new TatsunoFuelDispenser(this, stationId));
    }

    async scan(): Promise<void> {
        this.scanning = true;
        this.notify();

        try {
            const found: number[] = [];
            for (let stationAddress = 0x40; stationAddress <= 0x7f; stationAddress++) {
                logInfo(`Trying Station 0x${stationAddress.toString(16)}...`);
                try {
                    if ((await this.poll(stationAddress, Priority.LOWEST)).success()) {
                        logInfo(`Found station 0x${stationAddress.toString(16)}`);
                        found.push(stationAddress);
                    }
                } catch (err) {
                    // no reply... ignore...
                }
            }

            logInfo(`Station addresses: ${found.map(sa => sa.toString(16)).join()}`);
            this.notify();
        } catch (err) {
            logError(`Error during scan: ${err}`);
        } finally {
            this.scanning = false;
            this.notify();
        }
    }

    async poll(stationAddress: number, priority: Priority = Priority.LOW): Promise<TatsunoResponse> {
        const request = new TatsunoRequest(stationAddress, Codes.POLING, '', priority);
        return await this.rs485Protocol.sendMessage(request);
    }

    async select(stationAddress: number, data: string, priority: Priority = Priority.LOW): Promise<TatsunoResponse> {
        const request = new TatsunoRequest(stationAddress, Codes.SELECTING, data, priority);
        return await this.rs485Protocol.sendMessage(request);
    }

    async tryConnect(fd: TatsunoFuelDispenser, priority: Priority): Promise<void> {
        fd.reset();
        await fd.poke(priority, true);
    }

    async updatePrices(prices: Map<ProductType, number>): Promise<void> {
        this.unitPrices = prices;

        for (const fd of this.fuelDispensers.filter(fd => fd.online)) {
            try {
                await fd.sendPrices(Priority.HIGH);
            } catch (err) {
                logWarn(`Error updating price for fd#${fd.id}`);
            }
        }
    }

    protected async pollFuelDispenser(fd: TatsunoFuelDispenser): Promise<void> {
        if (!(await fd.select(SelectCodes.QUERY_STATUS)).success()) throw `Query Status failed`;
        if (!(await fd.poke())) throw `Poke failed`;
        if (!(await fd.select(SelectCodes.QUERY_TOTALS)).success()) throw `Query Totals failed`;
        if (!(await fd.poke())) throw `Poke failed`;
    }
}