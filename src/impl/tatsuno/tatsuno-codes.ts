export enum SelectCodes {
    CANCEL_AUTHORIZATION = '12',
    LOCK_PUMP = '13',
    RELEASE_PUMP = '14',
    QUERY_STATUS = '15',
    QUERY_TOTALS = '20'
}

export enum AuthTerms {
    NORMAL = '0',
    VOLUME_LIMIT = '1',
    PRESET_VARIABLE = '2',
    PRESET_FIXED = '3',
}

export enum AmountType {
    VOLUME = '1',
    PRICE = '2',
}

export enum UnitPriceFlag {
    PROHIBITED = '0',
    INVALID = '1',
    CASH_UNIT_PRICE = '2',
    CREDIT_UNIT_PRICE = '3',
    USER_INPUT = '9',
}

export enum UnitRankCode {
    NONE = '1', // Cancel / not selected
    CASH = '2',
    CREDIT = '3',
}

export enum ProductFlagCode {
    BLANK = '0',
    INVALID = '1',
    VALID = '2',
}

export enum ErrorCode {
    DATA_ERROR = '0',
    TIMING_ERROR = '1',
}

export enum TatsunoProductType {
    NONE = '0',
    HIGH_OCTANE_UNLEADED = '1',
    REGULAR_UNLEADED = '2',
    DIESEL = '3',
    KEROSENE = '4',
    LEADED_HIGH_OCTANE = '5',
    LEADED_REGULAR = '6',
}
