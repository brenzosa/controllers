import { AddressInfo, Server, Socket } from "net";
import SerialPort, { OpenOptions } from "serialport";
import { decode, logDebug, logError, logInfo, logWarn, toHexString } from "./buffer-utils";

export abstract class RS485Request<RESPONSE extends RS485Response> {
    onTimeout?: any;
    resolve?: (value: RESPONSE | PromiseLike<RESPONSE>) => void;

    constructor(
        public buffer: Uint8Array = new Uint8Array(),
        public priority: Priority = Priority.LOW,
    ) { }

    get data(): string {
        return decode(this.buffer);
    }
}

export abstract class RS485Response {
    constructor(
        public buffer: Uint8Array = new Uint8Array(),
        public timedOut: boolean = false,
        public invalid = false,
    ) { }

    abstract success(): boolean;

    get data(): string {
        return decode(this.buffer);
    }
}

export enum Priority {
    LOWEST = 0, // assigned to re-connection attempts (i.e., will most likely time-out)
    LOW = 5, // assigned to polling messages (i.e., lower priority than user-invoked commands)
    HIGH = 10, // highest priority are for user-invoked commands
}

export type ProtocolState = number;

export type StateTransitioner = (nextByte: number) => ProtocolState;

export abstract class RS485ProtocolBase<REQUEST extends RS485Request<RESPONSE>, RESPONSE extends RS485Response> {
    get connected(): boolean { return !!this.client || !!this.serialPort?.isOpen }
    stateTransitions: Map<ProtocolState, StateTransitioner> = new Map();
    response?: RESPONSE;
    request?: REQUEST;
    logStateTransitions: () => boolean = () => false;

    protected state: ProtocolState;
    private timeRequestSent = 0;

    private queuedRequests: REQUEST[] = [];
    private client?: Socket;
    private server?: Server;
    private serialPort?: SerialPort;

    constructor(public initialState: ProtocolState, public finalState: ProtocolState, public timeout: number) {
        this.state = initialState;
    }

    protected abstract sendRequest(request: REQUEST): ProtocolState;
    protected abstract newTimedoutResponse(request: REQUEST): RESPONSE;
    protected abstract newEmptyResponse(request?: REQUEST): RESPONSE;
    protected abstract responseMatchesRequest(request: REQUEST, response: RESPONSE): boolean;
    protected abstract stateName(state: ProtocolState): string;

    public connect(host: string, port: number): Promise<void> {
        logInfo(`(${this.constructor.name}) Connecting to ${host}:${port}...`);
        const client = new Socket();
        client.setKeepAlive(true);
        client.setNoDelay(true);
        client.setTimeout(5000);

        return new Promise((resolve, reject) => {
            client.connect({ port, host })
                .on('data', data => this.onRead(data))
                .on('timeout', () => {
                    client.end();
                    reject(`(${this.constructor.name}) Connection to ${host}:${port} timed out.`);
                })
                .once('error', err => reject(err))
                .once('close', () => this.client = undefined)
                .once('end', () => this.client = undefined)
                .once('connect', () => {
                    logInfo(`(${this.constructor.name}) Connection to ${(client.address() as AddressInfo).address}:${client.remotePort} ready.`);
                    this.client = client;
                    resolve();
                });
        });
    }

    public listen(port: number): Promise<void> {
        logInfo(`(${this.constructor.name}) Listening on ${port}...`);
        const server = this.server = new Server();

        return new Promise((resolve, reject) => {
            server.listen({ port: port }, () => {
                logInfo('listen success..');
                resolve();
            });
            server.on('connection', socket => {
                // close current connection (if any)...
                this.client?.end();
                this.client = socket;

                socket.setKeepAlive(true);
                socket.setNoDelay(true);

                logInfo(`(${this.constructor.name}) Got connection from ${(socket.address() as AddressInfo).address}:${socket.remotePort}...`);
                socket
                    .on('data', data => this.onRead(data))
                    .once('error', () => this.client = undefined)
                    .once('close', () => this.client = undefined)
                    .once('end', () => this.client = undefined);
            });
            server.once('error', err => reject(err));
        });
    }

    public connectSerial(path: string, options: OpenOptions): Promise<void> {
        logInfo(`(${this.constructor.name}) Connecting to ${path}[${JSON.stringify(options)}]...`);

        return new Promise((resolve, reject) => {
            const serialPort = this.serialPort = new SerialPort(path, options, err => err && reject(err));

            serialPort
                .on('data', data => this.onRead(data))
                .once('close', () => this.serialPort = undefined)
                .once('end', () => this.serialPort = undefined)
                .once('open', () => {
                    logInfo(`(${this.constructor.name}) Connection to ${path} ready.`);
                    resolve();
                });
        });
    }

    public disconnect(): void {
        this.serialPort?.close(err => err && logError(`Error closing serial port: ${err}`));
        this.server?.close();
        this.client?.end();

        this.serialPort = undefined;
        this.server = undefined;
        this.client = undefined;
    }

    public async sendMessage(request: REQUEST): Promise<RESPONSE> {
        const responsePromise = new Promise<RESPONSE>(resolve => request.resolve = resolve)
            .then(response => { clearTimeout(request.onTimeout); this.request = undefined; return response; })
            .finally(() => this.scheduleNextRequest());

        const insertPos = this.queuedRequests.findIndex(r => r.priority < request.priority);
        if (insertPos === -1) this.queuedRequests.push(request);
        else this.queuedRequests.splice(insertPos, 0, request);
        // logDebug(`Queued priority ${request.priority} at position ${insertPos} of ${this.queuedRequests.length}`);

        this.scheduleNextRequest();
        return responsePromise;
    }

    private scheduleNextRequest() {
        // NOTE: Delay sending if next message is of lowest priority: This should give higher priority messages
        // enough time to get queued up before we send out the lowest priority messages (i.e., we expect lowest
        // priority messages to likely time out since they are reserved for re-connection attempts)
        const nextRequestDelay = this.queuedRequests[0]?.priority === Priority.LOWEST ? 10 : 0;
        setTimeout(() => {
            try {
                this.sendNextRequest()
            } catch (err) {
                logError(`Error sending next request: ${err}`);
            }
        }, nextRequestDelay);
    }

    public send(buffer: Uint8Array): void {
        if (this.client) {
            if (this.logStateTransitions()) logDebug(`Sending (-> ${(this.client.address() as AddressInfo).address}): 0x${toHexString(buffer)}`);
            this.client?.write(buffer, err => err && logError(`I/O Send Error: ${err}`));

            // start timing how long it takes to receive a response
            this.timeRequestSent = Date.now();
        } else if (!!this.serialPort && this.serialPort?.isOpen) {
            if (this.logStateTransitions()) logDebug(`Sending (-> ${this.serialPort.path}: 0x${toHexString(buffer)}`);
            this.serialPort?.write(Buffer.of(...buffer), err => err && logError(`I/O Send Error: ${err}`));

            // start timing how long it takes to receive a response
            this.timeRequestSent = Date.now();
        } else {
            throw `Not connected.`;
        }
    }

    protected onRead(buffer: Uint8Array): void {
        let lastIndex = 0;
        try {
            if (this.logStateTransitions()) logDebug(`(${this.constructor.name}) Received (state=${this.stateName(this.state)}): 0x${toHexString(buffer)}`);
            else logDebug(`(${this.constructor.name}) Received: 0x${toHexString(buffer)}`);

            const responseLatency = this.timeRequestSent - Date.now();
            if (this.state !== this.initialState && responseLatency > this.timeout) {
                throw `WARNING: Timeout waiting for response: ${responseLatency}ms`;
            }

            buffer.forEach((b, index) => {
                lastIndex = index;
                const transitioner = this.stateTransitions.get(this.state);
                if (transitioner) {
                    const currentState = this.state;
                    this.state = transitioner(b);
                    if (this.logStateTransitions()) logDebug(`(${this.constructor.name}) ${this.stateName(currentState)} -> ${this.stateName(this.state)}`);
                    if (this.state === this.finalState) {
                        // publish response once the final state is reached
                        if (lastIndex < buffer.length - 1) {
                            throw `Extra data`;
                        }

                        this.publishResponse();
                        this.resetState();
                    } else if (this.state === this.initialState) {
                        this.resetState();
                    }
                } else {
                    throw `Invalid state`;
                }
            });
        } catch (err) {
            logWarn(`(${this.constructor.name}) Discarding remaining buffer [${err}]: (lastIndex=${lastIndex}) 0x${toHexString(buffer)}; err=${err}`);
            if (this.response) {
                this.response.invalid = true;
                this.publishResponse();
            }

            this.resetState();
        }
    }

    private sendNextRequest(): void {
        if (!this.connected) {
            this.queuedRequests.splice(0, this.queuedRequests.length);
        } else if (!this.request && this.queuedRequests.length > 0 && this.state === this.initialState) {
            const nextRequest = (this.request = this.queuedRequests.splice(0, 1)[0]);
            nextRequest.onTimeout = setTimeout(() => this.respondWithTimedOut(nextRequest), this.timeout);
            this.state = this.sendRequest(nextRequest);
        }
    }

    private respondWithTimedOut(request: REQUEST): void {
        request.resolve!(this.newTimedoutResponse(request));

        // reset state when a timeout event occurrs
        this.resetState();
    }

    protected resetState(): void {
        this.response = undefined;
        this.state = this.initialState;
    }

    protected publishResponse(): void {
        const request = this.request;
        const response = this.response || this.newEmptyResponse(request);

        if (request && this.responseMatchesRequest(request, response)) {
            request.resolve!(response);
        } else {
            // unsolicited message?
            logInfo(`(${this.constructor.name}) Discarding unsolicited response: ${response}`);
        }
    }
}
