import { logError } from "../impl/buffer-utils";
import { IFuelDispenser, INozzle } from "../impl/controllers";
import { Database, RunResult } from "sqlite3";

export interface ITransaction {
    timestamp: number;
    fuelDispenserId: string;
    fuelingSequence: string;

    volume: string;
    unitPrice: string;
    amount: string;

    productType: string;
    status: string;
    nozzleNumber: string;
    totalAmount: string;
    totalVolume: string;
}

export interface ITotals {
    timestamp: number;
    fuelDispenserId: string;
    nozzleNumber: string;
    totalAmount: string;
    totalVolume: string;
}

export class TransactionsDAO {
    connected = false;
    db?: Database;

    constructor() {
        process.on('exit', () => this.disconnect());
    }

    public async connect(): Promise<RunResult[]> {
        const connect = new Promise<void>((resolve, reject) => {
            this.db = new Database("./controllers.sqlite3", err => err ? reject(err) : resolve());
        });

        return connect
            .then(() => this.connected = true)
            .then(() => this.initTables());
    }

    public async disconnect(): Promise<void> {
        this.db?.close(err => { if (err) logError(`Error closing DB: ${err}`); });
        this.connected = false;
    }

    public async saveTransaction(fuelDispenser: IFuelDispenser, nozzle: INozzle): Promise<RunResult> {
        if (!this.db) throw `Not connected.`;
        const db = this.db;

        const $timestamp = new Date().getTime();
        const $fuelDispenserId = fuelDispenser.id;
        const $fuelingSequence = nozzle?.fuelingSequence || '';

        const $volume = nozzle?.volume || '';
        const $unitPrice = nozzle?.unitPrice || '';
        const $amount = nozzle?.amount || '';

        const $productType = nozzle?.productType || '';
        const $status = nozzle?.status || '';
        const $nozzleNumber = nozzle?.nozzleNumber || '';
        const $totalAmount = nozzle?.totalAmount || '';
        const $totalVolume = nozzle?.totalVolume || '';

        return new Promise((resolve, reject) => {
            const params = {
                $timestamp, $fuelDispenserId, $fuelingSequence, $volume, $unitPrice, $amount,
                $productType, $status, $nozzleNumber, $totalAmount, $totalVolume,
            };

            db.serialize(function () {
                db.run(
                    `INSERT INTO transactions
                            (timestamp, fuel_dispenser_id, fueling_sequence, volume, unit_price, amount,
                             product_type, nozzle_status, nozzle_number, total_volume, total_amount)
                     VALUES ($timestamp, $fuelDispenserId, $fuelingSequence, $volume, $unitPrice, $amount,
                             $productType, $status, $nozzleNumber, $totalVolume, $totalAmount)
                    `, params,
                    (runResult: RunResult, err: Error | null) => err ? reject(err) : resolve(runResult)
                );
            });
        });
    }

    public async loadTransactions($afterTransactionId = 0, $maxResults = 100): Promise<ITransaction[]> {
        if (!this.db) throw `Not connected.`;
        const db = this.db;

        const results: ITransaction[] = [];
        return new Promise((resolve, reject) => {
            const params = { $afterTransactionId, $maxResults };

            db.serialize(() => {
                db.each(
                    `SELECT *
                       FROM transactions
                      WHERE transaction_id > $afterTransactionId 
                      LIMIT $maxResults
                    `, params,
                    (err, row) => err ? reject(err) : results.push(row as ITransaction),
                    err => err ? reject(err) : resolve(results)
                );
            });
        });
    }

    public async deleteTransactions($olderThan = 0): Promise<void> {
        if (!this.db) throw `Not connected.`;
        const db = this.db;

        return new Promise((resolve, reject) => {
            const params = { $olderThan };

            db.serialize(() => {
                db.run(
                    `DELETE *
                       FROM transactions
                      WHERE timestamp < $olderThan
                    `, params, err => err ? reject(err) : resolve()
                );
            });
        });
    }

    public async loadTotals($asOfTime: number = new Date().getTime()): Promise<ITotals[]> {
        if (!this.db) throw `Not connected.`;
        const db = this.db;

        const results: ITransaction[] = [];
        const params = { $asOfTime };

        return new Promise((resolve, reject) => {
            db.serialize(() => {
                db.each(
                    `SELECT *
                       FROM totals
                      WHERE totals_id IN (
                             SELECT MAX(totals_id)
                               FROM totals
                              WHERE timestamp <= $asOfTime
                              GROUP BY fuel_dispenser_id, nozzle_number
                            )
                      ORDER BY fuel_dispenser_id, nozzle_number
                    `, params,
                    (err, row) => err ? reject(err) : results.push(row as ITransaction),
                    err => err ? reject(err) : resolve(results)
                );
            });
        });
    }

    public async saveLatestTotals(fuelDispenser: IFuelDispenser, nozzle: INozzle): Promise<RunResult> {
        if (!this.db) throw `Not connected.`;
        const db = this.db;

        const $timestamp = new Date().getTime();
        const $fuelDispenserId = fuelDispenser.id;
        const $nozzleNumber = `${nozzle?.nozzleNumber || ''}`;
        const $totalAmount = nozzle.totalAmount;
        const $totalVolume = nozzle.totalVolume;
        const params = {
            $timestamp, $fuelDispenserId, $nozzleNumber, $totalAmount, $totalVolume
        };

        return new Promise<RunResult>((resolve, reject) => {
            db.serialize(function () {
                db.run(`
                    INSERT INTO totals
                           (timestamp, fuel_dispenser_id, nozzle_number, total_volume, total_amount)
                    VALUES ($timestamp, $fuelDispenserId, $nozzleNumber, $totalVolume, $totalAmount)
                `, params, (runResult: RunResult, err: Error | null) => err ? reject(err) : resolve(runResult));
            });
        });
    }

    private async initTables(): Promise<RunResult[]> {
        if (!this.db) throw `Not connected.`;
        const db = this.db;

        const createTransactions = new Promise<RunResult>((resolve, reject) => {
            db.serialize(() => {
                db.run(`
                    CREATE TABLE IF NOT EXISTS transactions (
                        transaction_id    INTEGER PRIMARY KEY,
                        timestamp         INTEGER(8),
                        fuel_dispenser_id TEXT,
                        fueling_sequence  TEXT,
                        volume            TEXT,
                        unit_price        TEXT,
                        amount            TEXT,
                        product_type      TEXT,
                        nozzle_status     TEXT,
                        nozzle_number     INTEGER(1),
                        total_volume      TEXT,
                        total_amount      TEXT
                    )
                `, (runResult: RunResult, err: Error | null) => err ? reject(err) : resolve(runResult));
            });
        });

        const createTotals = new Promise<RunResult>((resolve, reject) => {
            db.serialize(() => {
                db.run(`
                    CREATE TABLE IF NOT EXISTS totals (
                        totals_id         INTEGER PRIMARY KEY,
                        timestamp         INTEGER(8),
                        fuel_dispenser_id TEXT,
                        nozzle_number     INTEGER(1),
                        total_volume      TEXT,
                        total_amount      TEXT
                    )
                `, (runResult: RunResult, err: Error | null) => err ? reject(err) : resolve(runResult));
            });
        });

        return Promise.all([createTransactions, createTotals]);
    }
}