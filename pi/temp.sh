#!/bin/bash

while true
do
  echo $(date '+%D %T') $(vcgencmd measure_temp)
  sleep 30
done
