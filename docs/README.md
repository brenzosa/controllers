# Install:

## Prerequisites:

- make and node-gyp

```
sudo npm install -g node-gyp
sudo apt-get install make
sudo apt-get update
sudo apt-get install --fix-missing g++
```

## Install

```
npm i .
```

## Raspberry Pi setup:

### Build microsd image using the [imager 1.6.2](https://downloads.raspberrypi.org/imager/imager_1.6.2.exe) or [download a newer version](https://www.raspberrypi.com/software/). 

- Follow these steps:

![./raspberry-pi-imager-00.png](./raspberry-pi-imager-00.png)
![./raspberry-pi-imager-00.png](./raspberry-pi-imager-01.png)
![./raspberry-pi-imager-00.png](./raspberry-pi-imager-02.png)
![./raspberry-pi-imager-00.png](./raspberry-pi-imager-03.png)
![./raspberry-pi-imager-00.png](./raspberry-pi-imager-04.png)
![./raspberry-pi-imager-00.png](./raspberry-pi-imager-05.png)
  

- Power on the Raspberry Pi, connect a keyboard and monitor, and enable SSH:

NOTE: Log-in using default credentials (username: **pi**, password: **raspberry**)

```
sudo touch /boot/ssh
sudo shutdown -r now 
```

### Setup Avahi

- Edit /etc/avahi/avahi-daemon.conf:

```
[server]
host-name=controller1
domain-name=local
use-ipv4=yes
use-ipv6=yes
allow-interfaces=eth0
...
```

- Update /etc/hostname

```
controller1
```

- Update /etc/hosts

```
127.0.0.1       localhost
::1             localhost ip6-localhost ip6-loopback
ff02::1         ip6-allnodes
ff02::2         ip6-allrouters

127.0.1.1               controller1
```

- SSH into pi from another machine using the controller1.local mDNS name:

```
ubuntu@DESKTOP-VELA157:~/brenzosa/controllers$ ssh pi@controller1.local
The authenticity of host 'controller1.local (192.168.137.34)' can't be established.
ECDSA key fingerprint is SHA256:SWTENuFMcSGJrtdR34L2SzYoUUDmzBeErkPE7CJn4cE.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Failed to add the host to the list of known hosts (/home/ubuntu/.ssh/known_hosts).
pi@controller1.local's password:
Linux controller1 5.10.17-v7+ #1414 SMP Fri Apr 30 13:18:35 BST 2021 armv7l

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Thu Nov  4 23:22:27 2021 from 192.168.137.1

SSH is enabled and the default password for the 'pi' user has not been changed.
This is a security risk - please login as the 'pi' user and type 'passwd' to set a new password.


Wi-Fi is currently blocked by rfkill.
Use raspi-config to set the country before use.

pi@controller1:~ $
```

### Setup nodejs on Pi

- Steps below are from https://www.makersupplies.sg/blogs/tutorials/how-to-install-node-js-and-npm-on-the-raspberry-pi:

```
sudo apt-get update
sudo apt-get upgrade

# For Raspberry Pi 3 /3B+
wget https://nodejs.org/dist/v16.13.0/node-v16.13.0-linux-armv7l.tar.xz

# install alternatives
sudo tar --directory=/opt/ -xf node-*
sudo update-alternatives --install /usr/bin/node node /opt/node-*/bin/node 20
sudo update-alternatives --install /usr/bin/npm npm /opt/node-*/bin/npm 20
sudo update-alternatives --install /usr/bin/npx npx /opt/node-*/bin/npx 20

# ...or...

# curl -sL https://deb.nodesource.com/setup_16.x | bash -
# sudo apt-get install -y nodejs
```

- Verify node setup:

```
pi@controller1:~ $ node -v
v16.13.0
pi@controller1:~ $
```

### Setup nginx:

```
sudo apt-get install nginx
```

- Edit /etc/nginx/sites-available/default

```
  location / {
    proxy_pass 'http://localhost:3000';
    proxy_pass_request_headers on;
    proxy_set_header Host 'localhost';
    proxy_set_header Origin 'http://localhost:3000';
  }

  location /websocket {
    proxy_pass 'http://localhost:3001';
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "Upgrade";
    proxy_set_header Host 'localhost';
    proxy_set_header Origin 'http://localhost:3001';
  }
```

- Restart nginx:

```
sudo service nginx restart
```

### Serial port configuration

- Steps from https://www.waveshare.com/wiki/2-CH_RS485_HAT:

```
echo "dtoverlay=sc16is752-spi1,int_pin=24" | sudo tee -a /boot/config.txt
sudo reboot
```

- Verify the drivers were installed after reboot:

```
pi@controller1:~ $ ls -alt /dev/gpiochip3 /dev/ttySC?
crw-rw---- 1 root gpio    254, 3 Nov  5 11:28 /dev/gpiochip3
crw-rw---- 1 root dialout 240, 0 Nov  5 11:28 /dev/ttySC0
crw-rw---- 1 root dialout 240, 1 Nov  5 11:28 /dev/ttySC1
pi@controller1:~ $
```

- Verify the new serial ports (/dev/ttySC0 & /dev/ttySC1) are detected:

```
pi@controller1:~ $ npx @serialport/list
/dev/ttyAMA0
/dev/ttySC0
/dev/ttySC1
pi@controller1:~ $
```

### Static IP Address

- Add an 'eth0' entry to /etc/dhcpcd.conf

```
interface eth0
static ip_address=192.168.137.11/24
static routers=192.168.137.1
static domain_name_servers=8.8.8.8
```

### Eaglestar Notes:
- Set nozzle number: "CANCEL" + "LITRE" + "LITRE" + "Number" + "lock 1"
- Enable communications via Keypad: "CANCEL" + "RECALL" + "SALE" + "SALE" + "1" + "lock 1"
- Store and upload manual fueling: "CANCEL" + "PRICE" + "PRICE" + "LITRE" + "1" + "lock 1"